# This is trivial django app that holds the built static files for
# pagebuilder.
#
# The existence of __init__.py here allows `pagebuilder_static` to be
# treated as a python module, and included in the project's INSTALLED_APPS
# setting, and thus for the compiled static files to be referenced in the
# normal ways, e.g.  from templates:
#
# {% static 'pagebuilder_static/js/main.js' %}
