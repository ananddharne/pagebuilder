import React, { Component } from 'react';

import $ from 'jquery';
import DocumentMeta from 'react-document-meta';

import BlocksContainer from 'blocks/BlocksContainer';
import ControlsContainer from 'controls/ControlsContainer';
import Header from 'Header';
import Models from 'Models';
import JobModal from 'modals/JobModal';
import JobListModal from 'modals/JobListModal';
import AddJobModal from 'modals/AddJobModal';
import SignUpModal from 'modals/SignUpModal';
import SignUpListModal from 'modals/SignUpListModal';
import VideoUploadHelpModal from 'modals/VideoUploadHelpModal';
import Dialog from 'modals/Dialog';
import ApplicationModal from 'modals/ApplicationModal';
import happieLogo from 'happie-logo.svg';
import previewImg from 'header/preview.svg';
import Loader from 'Loader';
import api from 'api';
import mapsHelper from "mapsHelper";
import preventDrop from 'preventDrop';

import deepEqual from 'deep-equal';

let Pusher = window.Pusher;

class App extends Component {

  componentWillMount() {

    // Prevent default behavior of dropping images on page
    preventDrop.init();

    let props = this.getSharedFunctions();
    // Set up api configuration
    api.init(props);

    // Asynchronously load google maps API
    mapsHelper.load();

    // Setup message if we try to navigate away
    this.props.router.setRouteLeaveHook(
      this.props.route,
      this.routerWillLeave
    )

    // Kind of complicated page loading with routing, because we could be:
    // A: Loading page for first time
    // B: Selecting a new page from PageSelect
    // C: Loading a new route by changing the URL param while still on the same route for App component
    // To handle this we have an initial state that can be overwritten when new data comes in,
    // which is defined in `loadInitialState`
    if (!this.state) {
      // If there's no state set at all, we need to initialize
      if (this.props.params.jobId && window.happie_page_data) {
        this.loadJobPage(this.props.params.jobId)
      } else {
        this.loadPage(this.props.params.pageId)
      }
    }

    // If it is a published page, we need to initialize pusher for event tracking
    if (window.happie_page_data) {
      let pusher = new Pusher(window.happie_page_data._metadata.pusher_key, {encrypted: true});
      let channel = pusher.subscribe('private-page-events');
      channel.bind('pusher:subscription_succeeded', () => {
        // If pusher connects successfully, put it in the state to use
        this.setState({
          pusher: pusher,
          channel: channel
        });
      });

      // We need to set the session close function
      window.onbeforeunload = this.closeSession;

      // Add any additional header scripts
      if (window.happie_page_data._metadata.additional_header_script && !$('head').find('.additional-header-script').length) {
        let s = document.createElement("script");
        s.type = "text/javascript";
        s.innerHTML = window.happie_page_data._metadata.additional_header_script;
        s.className = "additional-header-script";
        $('head').append(s);
      }

    }

  }

  // These are the main functions that every component uses to communicate app state back up to parent
  getSharedFunctions() {

    return {
      getAppState: this.getAppState,
      setAppState: this.setAppState,
      getActiveBlock: this.getActiveBlock,
      setActiveBlock: this.setActiveBlock,
      setActiveModal: this.setActiveModal,
      getActiveModal: this.getActiveModal,
      setActiveDialog: this.setActiveDialog,
      getActiveDialog: this.getActiveDialog,
      getAppMode: this.getAppMode,
      setAppMode: this.setAppMode,
      getLoaderHidden: this.getLoaderHidden,
      setLoaderHidden: this.setLoaderHidden,
      setUnsavedChanges: this.setUnsavedChanges,
      isDebugMode: this.isDebugMode,
      getPageId: this.getPageId,
      undo: this.undo,
      redo: this.redo,
      getAppStateHistory: this.getAppStateHistory,
      goToRoute: this.goToRoute,
      getQueryParams: this.getQueryParams,
      reportEvent: this.reportEvent,
      setCandidate: this.setCandidate,
      getCandidate: this.getCandidate,
      setCurrentJob: this.setCurrentJob,
      getCurrentJob: this.getCurrentJob,
      setJobApplied: this.setJobApplied
    }
  }

  componentWillReceiveProps = (e) => {
    // If route is not the same as the one we have saved, load new page
    if (e.params.pageId !== this.getPageId()) {
      this.loadPage(e.params.pageId);
    }

  }

  loadInitialState = (pageId, callback) => {
    // Create new objects so models don't get overwritten
    let defaultModels = $.extend(true, {}, Models);
    let defaultActiveBlock= $.extend(true, {}, Models.header);

    // Set base state for pages. Note that pageId may be passed in as a prop, saved in state, or undefined,
    // in which case we save an empty string
    this.setState({
      pageState: defaultModels,
      activeBlock: defaultActiveBlock.id,
      activeModal: "",
      activeDialog: "",
      mode: "edit",
      loaderHidden: true,
      stateHistory: [],
      stateHistoryIndex: 0,
      shouldCreateHistory: true, // When we hit "undo", set this false so we keep track of undo/redo position
      undoInterval: null,
      setStateTimeouts: {},
      setStateTimeoutDuration: 200,
      pageId: pageId || this.getPageId() || "",
      hasUnsavedChanges: false,
      jobsLoaded: false,
      candidate: null,
      currentJob: null
    }, callback)
  }

  componentWillUnmount = () => {
    clearInterval(this.state.undoInterval);
  }

  loadJobPage = (jobId) => {
    let job = window.happie_page_data.reqs[0];
    let _metadata = window.happie_page_data._metadata;
    // Create new objects so models don't get overwritten
    let defaultModels = $.extend(true, {}, Models);
    defaultModels._metadata = _metadata;
    let defaultActiveBlock= $.extend(true, {}, Models.header);

    this.setState({
      pageState: defaultModels,
      activeBlock: defaultActiveBlock.id,
      activeModal: "jobs:0",
      activeDialog: "",
      mode: "published",
      loaderHidden: true,
      stateHistory: [],
      stateHistoryIndex: 0,
      shouldCreateHistory: true, // When we hit "undo", set this false so we keep track of undo/redo position
      undoInterval: null,
      setStateTimeouts: {},
      setStateTimeoutDuration: 200,
      pageId: "",
      hasUnsavedChanges: false,
      jobsLoaded: true,
      candidate: null,
      currentJob: job,
      jobPage: true
    }, () => {
      // load jobs
      this.loadJobs()
      .done(() => {
        $("#app-pre-loader").hide();
      })
    })
  }

  loadPage = (pageId) => {

    // Get default state for app, pass in pageId (if it was provided)

    this.loadInitialState(pageId, () => {

      // Either load page from API or load from production string (if it exists)
      let loadFunction;
      if (window.happie_page_data && window.happie_page_data.page_blocks) {
        loadFunction = this.loadPublishedPages;
      } else {
        loadFunction = api.loadPage.bind(api);
      }


      loadFunction()
      .done((returnedData) => {

        // Add other data to our page data as a special key that should not actually be saved back to db,
        // just to make it easier to reference in other parts of app
        let returnedState = returnedData["page_blocks"];
        // Check if the metadata is already set
        if (window.happie_page_data && window.happie_page_data._metadata) {
          returnedState._metadata = window.happie_page_data._metadata;
        } else {
          returnedState._metadata = {
            company_logo_url: returnedData.company_logo_url,
            organization_logo_url: returnedData.organization_logo_url,
            company: returnedData.company || "Unassigned Company",
            last_published_at: returnedData.last_published_at,
            page_route: returnedData.page_route
          }
        }

        let newState = $.extend(true, this.state.pageState, returnedState);


        this.setState({
          pageState: newState
        }, () => {
          // Set up interval for undos
          // TODO: Figure out undo, right now causing way too many issues
          // this.setUndoInterval();

          // Show initial req if the query param is defined
          if (this.props.location.query && this.props.location.query.reqId) {
            this.showInitialReq();
          } else {
            this.loadJobs();
          }


          // After loading page, set unsaved changes to false
          // Current debouncing in LongFormEditor is at 500ms, so setting to 501 should let us set this
          // after that onChange fires after initial load
          setTimeout(() => {
            this.setUnsavedChanges(false);
          }, 501)

        })
      })
      .fail((e) => {
        console.error("Error getting page data" , e);
      })
      .always(() => {
        // Hide initial page loader after page data is returned
        $("#app-pre-loader").hide();
      })
    })
  }

  loadPublishedPages = () => {
    let deferred = $.Deferred();
    deferred.resolve({
      page_blocks: window.happie_page_data.page_blocks
    });
    return deferred;
  }

  loadJobs = () => {

    if (window.happie_page_data && window.happie_page_data.reqs) {

      let deferred = new $.Deferred()
      .done(() => {
        let pageState = $.extend(true, {}, this.state.pageState);

        // Turn strings into `location` object
        let parsedJobs = window.happie_page_data.reqs.map((value, index) => {
          value.location = {
            lat: parseFloat(value.latitude),
            lng: parseFloat(value.longitude),
            place_id: value.place_id
          }

          if (value.hiring_manager) {
            value.hiringManagerEmail = value.hiring_manager.email;
            value.hiringManagerLinkedIn = value.hiring_manager.linkedin_url;
            value.hiringManagerFullName = value.hiring_manager.first_name + " " +  value.hiring_manager.last_name;
            value.hiringManagerTitle = value.hiring_manager.title;
          }

          if (value.lander_comp_ote) {
            value.compensation = value.lander_comp_ote;
            delete value.lander_comp_ote;
          }


          return value;
        })

        // TODO: Get local job sync working
        pageState.jobs.jobs = parsedJobs;
        this.setState({
          jobsLoaded: true,
          pageState: pageState
        })

      });

      setTimeout(() => {
        deferred.resolve();
      },100);

      return deferred;
    } else {
      let request = api.loadJobs()
      .done((e) => {

        let pageState = $.extend(true, {}, this.state.pageState);

        // Turn strings into `location` object
        let parsedJobs = e.map((value, index) => {
          value.location = {
            lat: parseFloat(value.latitude),
            lng: parseFloat(value.longitude),
            place_id: value.place_id
          }

          if (value.hiring_manager) {
            value.hiringManagerEmail = value.hiring_manager.email;
            value.hiringManagerLinkedIn = value.hiring_manager.linkedin_url;
            value.hiringManagerFullName = value.hiring_manager.first_name + " " +  value.hiring_manager.last_name;
            value.hiringManagerTitle = value.hiring_manager.title;
          }

          if (value.lander_comp_ote) {
            value.compensation = value.lander_comp_ote;
            delete value.lander_comp_ote;
          }


          return value;
        })

        // TODO: Get local job sync working
        pageState.jobs.jobs = parsedJobs;
        this.setState({
          jobsLoaded: true,
          pageState: pageState
        })
      })
      .fail((e) => {
          console.error("error getting jobs");
      })

      return request;
    }

  }

  showInitialReq = () => {
    // If reqId is defined, open that req

    // Load jobs
    this.loadJobs()
    .done(() => {
      let jobIndex;

      if (this.props.location.query && this.props.location.query.reqId) {
        let reqId = parseInt(this.props.location.query.reqId, 10);

        // Get index for selected job
        let jobs = this.getAppState("jobs", "jobs");

        $(jobs).each((index, value) => {
          if (value.pk === reqId) {
            // Set the current job
            this.setState({
              currentJob: value
            });
            jobIndex = index
          }
        })

        this.setActiveModal(`jobs:${jobIndex}`);
        this.setActiveBlock(`jobs`);

      }
    })
    .fail((e) => {
      console.error("failed to get jobs " , e);
    })
  }

  setUnsavedChanges = (flag) => {

  // If we're setting unsaved changes to true, and we're not in debug mode, show a message to user when exiting
  setTimeout(() => {

    if (flag && !this.isDebugMode() && this.getAppMode() === "edit") {
      // Note that most browsers do not actually support a custom string,
      // but it will at least show with default text

      window.onbeforeunload = function() {
        return "Whoa! You haven't saved your page yet. Are you sure you want to leave before saving all of your work?"
      }

    } else if (window.happie_page_data) {
      window.onbeforeunload = this.closeSession;
    } else {
      window.onbeforeunload = null;
    }

      this.setState({
        hasUnsavedChanges: flag
      })
    })

  }

  closeSession = () => {
    // This should only ever overwrite the unload function if it's a published page
    this.reportEvent('session_complete');
  }

  // Handle routing back ourselves because of how Django container needs hard reloads
  routerWillLeave = () => {

    // Pretty sloppy right now, could use some TLC to refactor
    // TODO: Fix this, not reloading correctly on prod
    if (this.state.hasUnsavedChanges && !this.isDebugMode()) {
      let confirmation = confirm("Whoa! You haven't saved your page yet. Are you sure you want to leave before saving all of your work?");

      if (confirmation && window.parent.updateUrlHash) {
        window.parent.updateUrlHash(null);
      }

      return confirmation;

    } else {
      if (window.parent.updateUrlHash) {
        window.parent.updateUrlHash(null);
      }

      return true;
    }

  }

  // Hovering `Exit Preview` button can be clicked to go back to editable mode
  exitPreviewClick = () => {
    this.setState({
      mode: "edit"
    }, () => {
      // Trigger a resize event to get blocks to adapt to correct sizes
      $(window).trigger('resize');
    })
  }

  // The two main functions to set/get state of the entire app are here
  // Right now they only act on one key, but there is a helper file called
  // 'appArrayStateHelpers' that is used to set array values
  // Normal params are id: identifier for the model being read and key: the key on that model.
  // Can use id="app" to get full app state
  getAppState = (id, key) => {

    // Special ID to return full app state
    // Exclude metadata that is already returned from server
    // TODO: Figure out id "id: " attributes should be excluded if we're saving to server
    if (id === "app") {
      return this.getFullAppState();
    }

    // Check to make sure if we're getting a specific key, that both key and id are defined
    if (!this.state || !this.state.pageState[id]) {
      console.error("Error getting key " , key)
      console.error("For id " , id)
      return null;
    }

    // Otherwise return specified key for the model id. If key is not defined, return whole object
    if (!key) {
      return this.state.pageState[id]
    } else {
      return this.state.pageState[id][key]
    }

  }

  getFullAppState = () => {
    let state = $.extend(true, {}, this.state.pageState);
    delete state._metadata;

    // For jobs, need to extract location data into just lat/lng
    let jobs = state.jobs.jobs.slice();

    jobs = jobs.map((value, index) => {
      if (value.location) {
        value.latitude = value.location.lat;
        value.longitude = value.location.lng;
        value.place_id = value.location.place_id;
        delete value.location;
        delete value.locations;
        return value;
      }
      return false;
    })

    state.jobs.jobs = jobs;

    return state;
  }

  // TODO: Extend this for setting multiple keys at once
  commitAppStateChange = (id, key, value) => {

    if (!this.state || !this.state.pageState[id]) {
      console.error("Error setting key " , key)
      return null;
    }

    // Find the right model for ID, then update the corresponding value
    let stateBlock = this.state.pageState[id];


    // Set new state
    stateBlock[key] = value;

    // Set our new state in the correct state object, then set entire state
    let updatedPageState = this.state.pageState;

    updatedPageState[id] = stateBlock;


    // Set unsaved changes to true
    this.setUnsavedChanges(true);
  }

  setAppState = (id, key, value, skipDebounce) => {
    // Hit really bad performance issues with rapidly changing state, such as typing
    // or using the color picker, so debounce all input based on which key we're setting
    // before actually committing changes
    // Skip debounce with optional flag

    if (!skipDebounce) {

      clearTimeout(this.state.setStateTimeouts[key]);

      let setStateTimeout = setTimeout(() => {
        this.commitAppStateChange(id, key, value)
      }, this.state.setStateTimeoutDuration)

      let timeouts = this.state.setStateTimeouts;

      timeouts[key] = setStateTimeout;

      this.setState({
        setStateTimeouts: timeouts
      })

    } else {
      this.commitAppStateChange(id, key, value);
    }

  }

  setCandidate = (candidate) => {
    this.setState({
      candidate: candidate
    });
  }

  getCandidate = () => {
    return this.state.candidate;
  }

  setCurrentJob = (job) => {
    this.setState({
      currentJob: job
    });
  }

  getCurrentJob = () => {
    return this.state.currentJob;
  }

  setJobApplied = (id, applied) => {
    // Get index for selected job
    let jobs = this.getAppState("jobs", "jobs");

    $(jobs).each((index, value) => {
      if (value.id === id) {
        jobs[index].applied = applied
      }
    })

    this.setAppState('jobs','jobs',jobs);
  }

  // Interval for detecting changes in app state for undo/redo functionality
  setUndoInterval = () => {

    // Set first history item to be our current history
    this.setState({
      stateHistory: [$.extend(true, {}, this.state.pageState)]
    })

    let undoInterval = setInterval(() => {

      // Save state in our history if it doesn't match our last history item
      let stateHistory = this.state.stateHistory.slice();

      let currentState = $.extend(true, {}, this.state.pageState);
      let lastState = $.extend(true, {}, stateHistory[stateHistory.length - 1]);

      let areEqual = deepEqual(lastState, currentState);

      if (!areEqual && this.state.shouldCreateHistory) {

        stateHistory.push(currentState)

        // Trim if outside limit
        let limit = 30;
        if (stateHistory.length > limit) {
          stateHistory = stateHistory.slice(stateHistory.length - limit - 1, stateHistory.length)
        }

        this.setState({
          stateHistory: stateHistory,
          stateHistoryIndex: stateHistory.length - 1
        })
      }

    }, 1000)


    this.setState({
      undoInterval: undoInterval
    })
  }

  getActiveBlock = () => {
    return this.state.activeBlock;
  }

  // id here is normally just a string, like "header"
  // But in cases when we have arrays or sub-selectors, we add a colon like "team:3" which indicates
  // the team block is selected, and the 3rd index (4th item) is selected
  setActiveBlock = (id) => {

    this.setState({
      activeBlock: id
    })
  }

  getActiveModal = (id) => {
    return this.state.activeModal;
  }
  // Similar in nature to setActiveBlock
  setActiveModal = (id) => {

    // TODO: Make sure we can't open jobs modal without making "goals" block active first, which loads jobs

    this.setState({
      activeModal: id
    })
  }

  // Like active modal, but for dialog actually set object with props instead of just a string
  getActiveDialog = (id) => {
    return this.state.activeDialog;
  }
  setActiveDialog = (dialogOptions) => {
    this.setState({
      activeDialog: dialogOptions
    })
  }

  // "Edit" mode is when actually using pagebuilder.
  // "Preview" and "Production" are (for now) basically the same, i.e hiding editable features
  getAppMode = () => {
    if (window.happie_page_data && window.happie_page_data.page_blocks) {
      return "published";
    } else {
      return this.state.mode;
    }

  }

  setAppMode = (mode) => {
    this.setState({
      mode: mode
    })
  }

  // Undo/Redo
  undo = () => {

    let stateHistory = this.state.stateHistory.slice();
    let lastState;

    let lastIndex = this.state.stateHistoryIndex - 1

    lastState = $.extend(true, {}, stateHistory[lastIndex]);

    this.setState({
      pageState: lastState,
      stateHistoryIndex: lastIndex,
      shouldCreateHistory: false
    })

  }

  redo = () => {
    let stateHistory = this.state.stateHistory.slice();
    let redoState = $.extend(true, {}, stateHistory[this.state.stateHistoryIndex + 1]);

    this.setState({
      pageState: redoState,
      stateHistoryIndex: this.state.stateHistoryIndex + 1,
      shouldCreateHistory: false
    })

  }

  // So header can render itself correctly
  getAppStateHistory = () => {
    return {
      stateHistory: this.state.stateHistory.slice(),
      index: this.state.stateHistoryIndex
    }
  }

  setLoaderHidden = (isHidden) => {
    this.setState({
      loaderHidden: isHidden
    })
  }

  getLoaderHidden = () => {
    return this.state.loaderHidden
  }

  goToRoute = () => {
    if (this.state.hasUnsavedChanges && !this.isDebugMode()) {
      let shouldNavigate = confirm("Whoa! You haven't saved your page yet. Are you sure you want to leave before saving all of your work?");
      if (shouldNavigate) {
        this.props.router.push("/");
      }
    } else {
      this.props.router.push("/");
    }
  }

  isDebugMode = () => {
    let isDebug = false;
    if (this.props.router.location.query && this.props.router.location.query.debug === "true") {
      isDebug = true;
    }
    return isDebug;
  }

  getPageId = () => {
    if (window.happie_page_data && window.happie_page_data.page_id) {
      return window.happie_page_data.page_id;
    } else {
      return this.state ? this.state.pageId : "";
    }
  }

  getQueryParams = () => {
    if (this.props.router.location) {
      return this.props.router.location.query
    }
  }

  reportEvent = (type,values=[]) => {
    // Only send events if it's a published page
    if (window.happie_page_data) {
      let page_id = window.happie_page_data.page_id;
      let session_id = window.happie_page_data._metadata.session_id;
      let sequence_id = window.happie_page_data._metadata.sequence_id;
      let candidate_id = window.happie_page_data._metadata.candidate_id;

      // If pusher fails to initialize or hasn't initialized yet, just use our api
      if (this.state.pusher) {
        this.state.channel.trigger('client-page-events', {
          event: type,
          page_id: page_id,
          session_id: session_id,
          sequence_id: sequence_id,
          candidate_id: candidate_id,
          values: values
        });
      } else {
        api.reportEvent({
          session_id: session_id,
          sequence_id: sequence_id,
          candidate_id: candidate_id,
          event: type,
          values: values
        });
      }
    }
  }

  render() {

    let props = this.getSharedFunctions();

    // TODO: Declared here because before they ommitted in edit mode, could now be moved down into return()
    let header = <Header {...props} router={this.props.router} />;
    let editContainer = <ControlsContainer {...props} />;

    // Meta tag information
    let keywords = this.getAppState("header", "keywords").replace(/[ ,]+/g, ",");

    let meta = {
      title: this.getAppState("header", "title") || "Happie Page Builder",
      description: this.getAppState("header", "description") || "",
      meta: {
        name: {
          "keywords": keywords || ""
        }
      }
    }

    // Controls with indices expect them to be passed in via props
    let modalIndex = parseInt(this.getActiveModal().split(":")[1], 10);

    // List of modals that can be triggered with "setActiveModal"
    // Note that key, index, and modelId are not necessarily used if the modal doesn't save any data
    let modals = [
      <JobModal {...props} index={modalIndex} key="jobs" modelId="jobs" />,
      <JobListModal {...props} index={modalIndex} key="jobList" modelId="jobs" />,
      <SignUpModal {...props} index={modalIndex} key="signUp" modelId="signUp" />,
      <SignUpListModal {...props} index={modalIndex} key="signUpList" modelId="signUpList" />,
      <Dialog {...props} index={modalIndex} key="dialog" modelId="dialog" />,
      <VideoUploadHelpModal {...props} index={modalIndex} key="videoHelp" modelId="videoHelp" />,
      <ApplicationModal {...props} index={modalIndex} key="application" modelId="application" />,
      <AddJobModal {...props} index={modalIndex} key="addJob" modelId="jobs" />
    ]

    let content = <div id="pagebuilder-app" className="app" data-mode={this.getAppMode()}>
      <DocumentMeta {...meta} />
      <Loader hidden={this.state.loaderHidden} />
      {header}
      <img className="exit-preview-button"
        alt="Exit Preview"
        onClick={this.exitPreviewClick}
        role="button"
        src={previewImg}
        style={this.state.mode !== "edit" ? {display: "block"} : {display: "none"}}>
      </img>
        <BlocksContainer {...props} />
        {modals}
        {editContainer}
    </div>;

    // If this is a job page, only render the job
    if (this.state.jobPage) {
      meta = {
        title: this.state.currentJob.title || "Happie Job Page",
        description: this.state.currentJob.description || "",
        meta: {
          name: {
            "keywords": keywords || ""
          }
        }
      }
      content = <div id="pagebuilder-app" className="app" data-mode={this.getAppMode()}>
        <DocumentMeta {...meta} />
        <div className="job-page-logo">
          <img src={happieLogo} alt="Happie" />
        </div>
        <JobModal {...props} index={modalIndex} key="jobs" modelId="jobs" jobPage={true} />
        <ApplicationModal {...props} index={modalIndex} key="application" modelId="application" jobPage={true} />
      </div>
    }

    return (
      content
    );
  }
}

export default App;
