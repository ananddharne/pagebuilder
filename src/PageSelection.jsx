import React, { Component } from 'react';
import { Link } from 'react-router'
import DocumentMeta from 'react-document-meta';

import placeholder from "image-placeholder-white.svg";
import editImage from "pageSelect/edit.svg";
import editActiveImage from "pageSelect/edit-active.svg";
import cloneImage from "pageSelect/duplicate.svg";
import cloneActiveImage from "pageSelect/duplicate-active.svg";
import deleteImage from "pageSelect/delete.svg";
import deleteActiveImage from "pageSelect/delete-active.svg";
import createImage from "pageSelect/magic.svg";

import $ from 'jquery';
import moment from 'moment';
import classnames from 'classnames';

// TODO: Refactor these images out of 'team' directory
import addImg from "team/add-team-member.svg";
import addImgActive from "team/add-team-member-active.svg";

import api from "api";
import Dialog from "modals/Dialog"

import alerts from 'alerts';
import Switch from "./components/controls/elements/Switch";

class PageSelection extends Component {

  componentWillMount() {

    // Set defaults
    this.setState({
      addImg: addImg,
      pages: [],
      editImage: editImage,
      cloneImage: cloneImage,
      deleteImage: deleteImage
    })


    // Set up api configuration
    let props = {
      isDebugMode: this.isDebugMode,
      setLoaderHidden: this.setLoaderHidden,
      pageId: "",
      getQueryParams: this.getQueryParams
    }

    // Redirect right away if a page ID was set in
    if (this.props.location.query && this.props.location.query.pk) {

      this.props.router.replace({
        pathname: `/builder/${this.props.location.query.pk}`,
        query: this.props.location.query
      })
      return;
    }

    api.init(props);

    api.loadPages()
    .done((returnedData) => {
      console.log("returned? " , returnedData);
      let pages = returnedData;

      this.setState({
        pages: pages
      }, () => {

      })
    })
    .fail(() => {
      console.error("Error getting pages");
    })
    .always(() => {
      $("#app-pre-loader").hide();
      this.checkAddButtonPulse();
    })

  }

  getQueryParams = () => {
    if (this.props.router.location) {
      return this.props.router.location.query
    }
  }

  isDebugMode = () => {
    let isDebug = false;
    if (this.props.router.location.query && this.props.router.location.query.debug === "true") {
      isDebug = true;
    }
    return isDebug;
  }

  setLoaderHidden = (isHidden) => {
    // TODO: Ensure this always gets called to "true" when APIs resolve, even when failing
    if (isHidden) {
      $("#app-pre-loader").hide();
      $("#root").show();
    } else {
      $("#app-pre-loader").show();
      $("#root").show();
    }

  }

  // If we load and there are no pages built yet, pulse add button after delay
  checkAddButtonPulse = () => {

    // Only perform if we don't have pages.
    // Also note there will be an error if user navigates from page before timeout finishes,
    // but should be mostly harmless (famous last words)

    // Only perform timeouts if we didn't load any pages
    if (this.state.pages.length > 0) {
      return;
    }

    let delayBeforePulse = 2500;
    let delayBetweenPulses = 200;
    let numOfPulses = 8;

    setTimeout(() => {
      for (let i = 0; i < numOfPulses; i++) {
        setTimeout(() => {
          let image;
          if (i % 2 === 0) {
            image = addImgActive
          } else {
            image = addImg
          }

          setTimeout(() => {
            this.setState({
              addImg: image
            })
          }, i * delayBetweenPulses)

        })
      }
    }, delayBeforePulse)
  }

  addEnter = () => {
    this.setState({
      addImg: addImgActive
    })
  }

  addLeave = () => {
    this.setState({
      addImg: addImg
    })
  }

  editImageEnter = () => {
    this.setState({
      editImage: editActiveImage
    })
  }

  editImageLeave = () => {
    this.setState({
      editImage: editImage
    })
  }

  cloneImageEnter = () => {
    this.setState({
      cloneImage: cloneActiveImage
    })
  }

  cloneImageLeave = () => {
    this.setState({
      cloneImage: cloneImage
    })
  }

  deleteImageEnter = () => {
    this.setState({
      deleteImage: deleteActiveImage
    })
  }

  deleteImageLeave = () => {
    this.setState({
      deleteImage: deleteImage
    })
  }

  addClick = () => {
    // TODO: Create page then go to route
    api.createNewPage()
    .done((returnedData) => {
      let id = returnedData.pk;
      this.props.router.push(`/builder/${id}?pk=${id}`)
    })
    .fail((e) => {
      console.error("Error creating new site " , e)
    })

  }

  existingPageClick = (pk) => {
    // Note that this will cancel out the normal link we set up because when we change the parent
    // hash it's going to do a hard refresh (yuck)
    if (!this.state.isDebugMode && window.parent && window.parent.updateUrlHash) {
      window.parent.updateUrlHash(pk);
    }

  }

  // TODO: Actually implement function, do hovers for icons
  cloneClick = (e, index) => {
    e.preventDefault();

    this.setActiveDialog({
      description: "Are you sure you want to clone this page?",
      confirmText: "Yes",
      cancelText: "No",
      confirm: () => {
        this.cloneBlock(index)
      }
    })
  }

  cloneBlock = (index) => {

    // Copy data from the page we clicked
    let page = $.extend(true, {}, this.state.pages[index]);
    let pageBlocks = page.page_blocks;

// pageBlocks which has returneddata accesses the title on cloneClick and changes it to '{title of the page to be cloned} - copy'
  pageBlocks.header.title = `${pageBlocks.header.title} - copy`;


    api.createNewPage()

    .done((returnedData) => {
      let pk = returnedData.pk;

      // Pass in data to save page so it doesn't try to use default `getAppState` functions
      api.savePage(pageBlocks, pk)
      .done((returnedData) => {

        // Refresh page data after it's updated
        api.loadPages()
        .done((returnedData) => {
          let pages = returnedData;
          this.setState({
            pages: pages
          })
        })
        .fail(() => {
          console.error("Error getting pages");
        })
      })
      .fail((e) => {
        console.error("Error updated the site we just created" , e)
      })

    })
    .fail((e) => {
      console.error("Error creating new site " , e)
    })
  }

  deleteClick = (e, index) => {
    e.preventDefault();

    this.setActiveDialog({
      description: "Are you sure you want to delete this page? Deletion cannot be undone, so please be careful. If you think you may want to use this page again at some point, please consider unpublishing it!",
      confirmText: "Yes, delete it now!",
      cancelText: "Cancel",
      confirm: () => {
        this.deletePage(index)
      }
    })
  }

  deletePage = (index) => {

    // Get the page id
    let page = $.extend(true, {}, this.state.pages[index]);

    api.deletePage(page.id)
    .done((returnedData) => {

        // Refresh page data after page is deleted
        api.loadPages()
        .done((returnedData) => {
          let pages = returnedData;

          this.setState({
            pages: pages
          })

          alerts.show({
            message: "You successfully deleted the page! It will no longer be accessible to candidates or members of your team. So long, page!",
            status: "success"
          })
        })
        .fail(() => {
          console.error("Error getting pages");
        })

    })
    .fail((e) => {
      console.error("Error deleting page " , e)

      alerts.show({
        message: "Uh oh! There was an error deleting your page. Please try deleting this page again. Sorry for the inconvenience!",
        status: "error"
      })
    })
  }

  switchClick = (index) => {

    // Get the page and if it's published
    let pages = this.state.pages;
    let page = $.extend(true, {}, pages[index]);

    if (this.isEmpty(page.published_page_blocks)) {

      // Check if they have any page blocks to publish

      if (this.isEmpty(page.page_blocks)) {
        this.setState({
          pages: pages
        });
        alerts.show({
          message: "Hold your horses — you haven't added anything to the page yet. Fill it with content, choose your URL and try again!",
          status: "warning"
        })
        return;
      }

      api.publishPage(page.id,page.page_blocks,false)
      .done((returnedData) => {

        // Set the published time to the page
        page.last_published_at = moment().format('YYYY-MM-DD HH:mm:ss');
        page.published_page_blocks = page.page_blocks;

        pages[index] = page;

        this.setState({
          pages: pages
        });

        alerts.show({
          message: "Awesome — you successfully republished the page! It’s now visible for everyone to see, including candidates!",
          status: "success"
        })

      })
      .fail((e) => {
        console.error("Error publishing page " , e)

        alerts.show({
          message: "Whoopsie! There was an error republishing your your page. Please refresh your browser, and try again. Sorry for the inconvenience!",
          status: "error"
        })
      })

    } else {

      // Set the copy for unpublishing

      api.unpublishPage(page.id,false)
      .done((returnedData) => {

        page.published_page_blocks = {};

        pages[index] = page;

        this.setState({
          pages: pages
        });

        alerts.show({
          message: "You successfully unpublished the page! Candidates will no longer see it when they try to visit its URL until you republish the page.",
          status: "success"
        })

      })
      .fail((e) => {
        console.error("Error unpublishing page " , e)

        alerts.show({
          message: "Uh oh! There was an error unpublishing your your page. Please refresh your browser, and try again. Sorry for the inconvenience!",
          status: "error"
        })
      })
    }

  }

  // Mimicking functionality in App.js just for the confirmation popup
  getActiveDialog = () => {
    return this.state.dialog;
  }

  setActiveDialog = (options) => {
    this.setState({
      dialog: options
    })
  }

  getMainImage = (page) => {
    // Loop through this list of blocks looking to see if background type is an image, and display
    // based on first one hit

    let blockList = [
      "tagline",
      "mission",
      "goals",
      "team",
      "signUp",
      "bottomImage",
      "location",
      "header"
    ]

    let image;
    let pageData = page.page_blocks;
    $(blockList).each((key, value) => {

      let pageBlock = pageData[value];

      if (pageBlock && pageBlock.backgroundType === "image" && pageBlock.background) {
        image = pageBlock.background;
        return false;
      }

      // For header, check logo instead of background type.
      // Shouldn't make it here if another block before it had a background defined
      if (pageBlock && pageBlock.id === "header") {
        if (pageBlock.logo) {
          image = pageBlock.logo;
          return false;
        }
      }

    })

    return image;
  }

  isEmpty = (obj) => {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // If it isn't an object at this point
    // it is empty, but it can't be anything *but* empty
    // Is it empty?  Depends on your application.
    if (typeof obj !== "object") return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
  }

  render() {

    // Sort the blocks to put most recently saved first
    let pages = this.state.pages;
    pages.sort((a,b) => {
      return moment(b.last_updated).valueOf() - moment(a.last_updated).valueOf();
    });

    let builtBlocks = pages.map((value, key) => {

      let image;

      // TODO: Build out smart way to calculate images based on first available one
      if (value.page_blocks) {
        image  = this.getMainImage(value);
      }
      let imageStyle;
      let imageContainerStyle;
      if (!image) {
        image = placeholder;
        imageStyle = {
          "minWidth": "60%"
        }
        imageContainerStyle = {
          "background": "#d0d0d0"
        }
      }

      let blockKey = `block-${key}`;

      let title = value.page_blocks.header ? value.page_blocks.header.title : "Untitled";
      let savedTime = `Last saved on ${moment(value.last_updated).format('MMM Do, YYYY')}`;

      // Link to builder page, and add query param if it's defined
      let linkUrl = `/builder/${value.pk}`;

      let enabled = !this.isEmpty(value.published_page_blocks) ? true : false;

      let unpublishedEditsStyle = moment(value.last_updated).unix() > moment(value.last_published_at).unix() ? {display: 'inline-block'} : {display: 'none'};

      // Make the title a link to the published page if has been published
      if (enabled) {
        let pagesUrl = "http://talent.gethappie.me/pages/";
        if (localStorage.getItem('url_base')) {
          pagesUrl = localStorage.getItem('url_base') + 'pages/';
        }
        let publishedUrl = pagesUrl + value.page_route;
        let linkStyle = {
          color: 'inherit'
        };
        title = <a href={publishedUrl} target="_blank" style={linkStyle}>{title}</a>
      }

      return <div className="built-page-block" key={blockKey}>
            <Link to={{pathname: linkUrl, query: this.props.location.query}} onClick={() => {this.existingPageClick(value.pk)}}>
              <div className="built-page-image-container" style={imageContainerStyle}>
                <img alt="Built Page Preview" src={image} style={imageStyle} className="preview-image"></img>
                <div className="vertical-align-container edit-button-container">
                  <div className="vertical-aligner">
                    <img alt="Edit Page" src={this.state.editImage} className="edit-image" onMouseEnter={this.editImageEnter} onMouseLeave={this.editImageLeave}></img>
                    <img alt="Clone page" src={this.state.cloneImage} className="edit-image" onMouseEnter={this.cloneImageEnter} onMouseLeave={this.cloneImageLeave} onClick={(e) => this.cloneClick(e, key)}></img>
                    <img alt="Delete Page" src={this.state.deleteImage} className="edit-image" onMouseEnter={this.deleteImageEnter} onMouseLeave={this.deleteImageLeave} onClick={(e) => this.deleteClick(e, key)}></img>
                  </div>
                </div>
              </div>
            </Link>
            <div className="built-page-description">
              <span className="unpublished-edits"><b style={unpublishedEditsStyle}>UNPUBLISHED EDITS</b></span>
              <h2 className="built-page-title"><b>{title}</b></h2>
              <Switch
                switchClick={this.switchClick}
                pageKey={key}
                on={enabled}
                title=""
                style={{display: 'inline-block'}}
              />
              <span className="published-time">{savedTime}</span>
            </div>
          </div>
    })

    // Need to mimick what we have in App.js just for dialog
    let dialogProps = {
      getActiveDialog: this.getActiveDialog,
      setActiveDialog: this.setActiveDialog,
      getAppMode: () => {return "production"}
    }

    let noPagesCreatedMessage = "You haven't created a page yet. Get started by clicking the plus sign <b>[+]</b> above. Let's make magic happen!";

    let addBlockClasses = classnames({
      "built-page-block add-block": true,
      "no-pages": true
    })

    return (

      <div id="pagebuilder-app" className="app page-selection">
        <DocumentMeta title="Happie Page Builder" />

        <div className="built-page-container">

          <div className={addBlockClasses}>
            <div className="vertical-align-container">
              <div className="vertical-aligner">
                <img src={this.state.addImg} onMouseEnter={this.addEnter} onMouseLeave={this.addLeave} onClick={this.addClick} role="button" alt="Add Page"></img>
              </div>
            </div>
          </div>

          {builtBlocks}

          <h1 className="no-pages-created-message" style={this.state.pages.length < 1 ? {display: "block"} : {display: "none"}} dangerouslySetInnerHTML={{"__html": noPagesCreatedMessage}}></h1>

          <img style={this.state.pages.length < 1 ? {display: "block", margin: "0 auto"} : {display: "none"}} src={createImage} alt="create new page decoration"></img>

        </div>

        <Dialog {...dialogProps} />

      </div>
    );
  }
}

export default PageSelection;
