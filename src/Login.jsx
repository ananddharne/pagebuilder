import React, { Component } from 'react';
import $ from 'jquery';
import AuthService from './lib/AuthService'

class Login extends Component {

  componentWillMount() {

    // If they are hitting the login page and have tokens, log them out to remove them
    if (localStorage.getItem('hap_token') || localStorage.getItem('id_token')) {
      this.props.router.push('/logout');
    } else {
      // Intialize auth in here so it doesn't try to authenticate with the happie server before
      const auth = new AuthService('veSrTVYt0NtxuuORdhOZ41IlrMF0LPeE', 'happie.auth0.com', this.props.router);
      // Try not to show the pre-loader flashing on and off
      // A little hacky, but better for UI
      setTimeout(() => {
        if (!auth.loggedIn()) {
          $("#app-pre-loader").hide();
          auth.login();
        }
      },500);
    }

  }

  render() {

    return (

      <div id="pagebuilder-app" className="app login">

      </div>
    );
  }
}

export default Login;
