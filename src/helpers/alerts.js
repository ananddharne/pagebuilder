/// Wrapper for jquery library http://www.jqueryscript.net/other/jQuery-Plugin-To-Create-Animated-Bootstrap-Alerts-notify.html
import $ from 'jquery';
import 'notify';
const alerts = {
  show: function(options) {
    $.notify(options);
  }
}

export default alerts;
