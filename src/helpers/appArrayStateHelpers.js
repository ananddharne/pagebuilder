/// Helper function for setting app state for arrays
/*
Params:
comp: the "this" object from the calling component
baseKey: the base key to update, such as "name"
arrayKey: the name of the array to update, such as "teamMembers"
value: the value to set in the assigned key

Assumes the component has props for getAppState, setAppState, modelId, and index
*/
const appArrayStateHelpers = {
  set: function(comp, baseKey, arrayKey, value) {

    let array = comp.props.getAppState(comp.props.modelId, arrayKey) || [];

    array[comp.props.index][baseKey] = value;
    comp.props.setAppState(comp.props.modelId, arrayKey, array);
  },

  get: function(comp, baseKey, arrayKey) {
    let array = comp.props.getAppState(comp.props.modelId, arrayKey);
    let object = array[comp.props.index];

    // If object is defined, return the appropriate key.  Otherwise return empty string.
    return object ? object[baseKey] : "";
  }
}

export default appArrayStateHelpers;
