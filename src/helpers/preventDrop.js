/// Prevent default behavior for dragging/dropping images. Normally if you dragged-dropped onto our images
// upload and missed, the browser would load the image instead of our app
const preventDrop = {
  init: function() {
    window.addEventListener("dragover",function(e){
      e = e || event;
      e.preventDefault();
    },false);
    window.addEventListener("drop",function(e){
      e = e || event;
      e.preventDefault();
    },false);
  }
}

export default preventDrop;
