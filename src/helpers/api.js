/// Helper file to handle API requests with server
// DEVELOPER NOTE: make sure all POST/PUT request are stringified
import $ from 'jquery';

const api = {

  activeRequests: 0,
  hasInitialized: false,

  init: function(options) {

    // Always set up these when we init, since App component adds things that might not be included if
    // PageSelect component has initialized
    this.appFunctions = options;
    this.getPageId = options.getPageId;

    // If we've already initialized, don't need to set up rest of config stuff below
    if (this.hasInitialized) {
      return;
    }

    // TODO: Put this in config file
    this.basePath = "/api/v1";

    // Override with inserted data if available
    if (window.happie_page_data && window.happie_page_data.api_base) {
      this.basePath = window.happie_page_data.api_base
    }

    // Get and set happie token
    this.authToken = localStorage.getItem('hap_token');

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
          if (!window.happie_page_data) {
            let token = this.authToken;
            xhr.setRequestHeader("Authorization", `Token ${token}`);
          }
        }.bind(this),
        error: function(xhr, settings, error) {
          // If we ever get an unauthorized error, send them to the logout page to clear any cookies so they can login
          if (xhr.status===401) {
            // Clear any tokens
            localStorage.removeItem('id_token');
            localStorage.removeItem('hap_token');
            // Check if we're in an iframe to either reload on outreach-client or re-direct to the logout page on pagebuilder
            let inIframe = false;
            try {
              inIframe = window.self !== window.top;
            } catch (e) {
              inIframe = true;
            }
            if (inIframe) {
              parent.location.reload();
            } else {
              window.location.assign('/#/logout');
            }
          }
        }
    });

    this.hasInitialized = true;
  },

  // Src: http://stackoverflow.com/questions/19491336/get-url-parameter-jquery-or-how-to-get-query-string-values-in-js
  getUrlParameter: function(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
  },

  // We can have multiple requests going at once, so only hide when we go down to 0 active requests
  beginLoading: function() {
    this.activeRequests++;
    this.appFunctions.setLoaderHidden(false);
  },

  finishLoading: function() {
    this.activeRequests--;

    if (this.activeRequests === 0) {
      this.appFunctions.setLoaderHidden(true);
    }

  },

  loadPages: function() {
    if (this.appFunctions.isDebugMode()) {

      let promise = $.Deferred();

      setTimeout(() => {

      promise.resolve([
        {
          last_published_at: "2016-12-01T21:45:22.678493Z",
          page_blocks: {

              "tagline": {
                backgroundType:"image",
                id: "tagline"
              },
              "header": {
                title: "My title",
                id: "header"
              },
              "mission": {
                backgroundType:"image",
                id: "tagline"
              }

          },
          pk: 1
        },
        {
          page_blocks: {

              "tagline": {
                background: "https://upload.wikimedia.org/wikipedia/en/5/5f/Original_Doge_meme.jpg",
                backgroundType:"video",
                id: "tagline"
              },
              "mission": {
                background: "https://pbs.twimg.com/media/Cbg6oKqUkAI2PAB.jpg",
                backgroundType:"image",
                id: "tagline"
              }

          },
          pk: 7,
          "last_published_at": null
        },
        {
          page_blocks: {
              "tagline": {
                background: "http://cdn3.sbnation.com/assets/3786371/DOGE-12.jpg",
                backgroundType:"image",
                id: "tagline"
              }
          },
          pk: 11,
          "last_published_at": null
        }

       ]);

     }, 10)


       // promise.resolve([])

      return promise;
    }

    this.beginLoading()

    // Note that local development requires you to run the local JSON server with `npm run api`
    let promise = $.ajax({
      method: "GET",
      url: `${this.basePath}/page/`
    })

    promise.always(() => {
      this.finishLoading();
    })

    return promise;
  },

  createNewPage: function() {
    if (this.appFunctions.isDebugMode()) {
      let promise = $.Deferred();

      this.beginLoading()

      setTimeout(() => {

        promise.resolve(
          {
            page_blocks: [
              {"tagline": {background: "https://pbs.twimg.com/media/Cbg6oKqUkAI2PAB.jpg"}
            },
          ], pk: 7},
         );

         this.finishLoading()


      }, 10)

      return promise;

    }

    this.beginLoading()

    // Note that local development requires you to run the local JSON server with `npm run api`
    let promise = $.ajax({
      method: "POST",
      url: `${this.basePath}/page/`
    })

    promise.always(() => {
      this.finishLoading();
    })

    return promise;
  },

  loadPage: function() {

    if (this.appFunctions.isDebugMode()) {
      let promise = $.Deferred();
      this.beginLoading();

      setTimeout(() => {
        this.finishLoading();
        promise.resolve({
          last_published_at: "2016-12-01T21:45:22.678493Z",
          company: "mycompany",
          page_route: "mycompanyurl",
          page_blocks: {

              "tagline": {
                backgroundType:"image",
                id: "tagline",
                description: "desc tagline"
              },
              "header": {
                title: "My title",
                id: "header"
              },
              "mission": {
                backgroundType:"image",
                id: "tagline"
              }

          },
          pk: 1
        });
      }, 2000)

      return promise;
    }

    this.beginLoading()

    // Note that local development requires you to run the local JSON server with `npm run api`
    let promise = $.ajax({
      url: `${this.basePath}/page/${this.getPageId()}/`,
      dataType: "json",
      type: "GET"
    })

    promise.always(() => {
      this.finishLoading();
    })

    return promise;
  },

  savePage: function(pageStateArg, pageIdArg) {

    if (this.appFunctions.isDebugMode()) {
      this.beginLoading();

      let promise = $.Deferred();

      setTimeout(() => {
        promise.resolve({});
        this.finishLoading();
      }, 100)

      return promise;
    }

    this.beginLoading();

    // Can either pass in an optional argument to save the state, or (most of the time),
    // use the function in App.js to get the app state to save
    let appState = pageStateArg || $.extend(true, {}, this.appFunctions.getAppState("app"));

    // Remove jobs info since that's saved separately
    delete appState.jobs.jobs;

    let pageId = pageIdArg || this.getPageId();

    let promise = $.ajax({
      method: "PUT",
      contentType: "application/json",
      url: `${this.basePath}/page/${pageId}/`,
      data: JSON.stringify({
        "page_blocks": appState,
        "title": appState.header.title
      })
    });

    promise.always(() => {
      this.finishLoading();
    })

    return promise;
  },

  savePublishedRoute: function() {

    if (this.appFunctions.isDebugMode()) {
      this.beginLoading();

      let promise = $.Deferred();

      setTimeout(() => {
        promise.resolve({});
        this.finishLoading();
      }, 100)

      return promise;
    }

    this.beginLoading();

    let promise = $.ajax({
      method: "POST",
      contentType: "application/json",
      url: `${this.basePath}/page/${this.getPageId()}/route/`,
      data: JSON.stringify({
        "page_route": this.appFunctions.getAppState("_metadata", "page_route")
      })
    });

    promise.always(() => {
      this.finishLoading();
    })

    return promise;
  },

  publishPage: function(pageId=this.getPageId(), pageBlocks=this.appFunctions.getAppState("app"), showLoader=true) {

    if (this.appFunctions.isDebugMode()) {
      let promise = $.Deferred();
      promise.resolve({});
      return promise;
    }

    // Don't want the screen flashing if they are hitting the toggle switch
    if(showLoader){
      this.beginLoading();
    }

    let promise = $.ajax({
      method: "POST",
      contentType: "application/json",
      url: `${this.basePath}/page/${pageId}/publish/`,
      data: JSON.stringify({
        "page_blocks": pageBlocks
      })
    });

    if (showLoader) {
      promise.always(() => {
        this.finishLoading();
      })
    }

    return promise;
  },

  unpublishPage: function(pageId=this.getPageId(), showLoader=true) {

    if (this.appFunctions.isDebugMode()) {
      let promise = $.Deferred();
      promise.resolve({});
      return promise;
    }

    // Don't want the screen flashing if they are hitting the toggle switch
    if(showLoader){
      this.beginLoading();
    }

    let promise = $.ajax({
      method: "DELETE",
      contentType: "application/json",
      url: `${this.basePath}/page/${pageId}/publish/`,
    });

    if (showLoader) {
      promise.always(() => {
        this.finishLoading();
      })
    }

    return promise;
  },

  deletePage: function(pageId=this.getPageId()) {

    if (this.appFunctions.isDebugMode()) {
      let promise = $.Deferred();
      promise.resolve({});
      return promise;
    }

    this.beginLoading();

    let promise = $.ajax({
      method: "DELETE",
      contentType: "application/json",
      url: `${this.basePath}/page/${pageId}/`
    });

    promise.always(() => {
      this.finishLoading();
    })

    return promise;
  },

  saveImage: function(files) {

    if (this.appFunctions.isDebugMode()) {
      let promise = $.Deferred();
      promise.resolve({url: files[0].preview});
      return promise;
    }


    // src: https://abandon.ie/notebook/simple-file-uploads-using-jquery-ajax
    var data = new FormData();

    // Note that we should never actually have multiple files, but keep this just in case we want to support
    // multi file upload in the future
    $.each(files, function(key, value)
    {
      data.append(key, value);
    });

    this.beginLoading();

    // TODO: If this fails, try stringifying
    let promise = $.ajax({
        url: `${this.basePath}/images/`,
        type: 'POST',
        data: data,
        cache: false,
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
    })
    .always((e) => {
      this.finishLoading();
    })

    return promise;

  },

  loadJobs: function(id) {

    if (this.appFunctions.isDebugMode()) {
      let promise = $.Deferred();
      this.beginLoading();

      setTimeout(() => {
        promise.resolve([{"pk":707,"company":127,"title":"Untitled","description":"<h2><b>Enter your job description here.</b></h2>","latitude":null,"longitude":null,"lander_comp_ote":null,"place_id":null,"locations":[],"hiring_manager":null},{"pk":706,"company":127,"title":"Untitled","description":"<h2><b>Enter your job description here.</b></h2>","latitude":null,"longitude":null,"lander_comp_ote":null,"place_id":null,"locations":[],"hiring_manager":null},{"pk":705,"company":127,"title":"Untitled","description":"<h2><b>Enter your job description here.</b></h2>","latitude":null,"longitude":null,"lander_comp_ote":null,"place_id":null,"locations":[],"hiring_manager":{"pk":1,"company":127,"first_name":"manager name","last_name":"last name","email":"test@email.com","title":null,"linkedin":""}},{"pk":704,"company":127,"title":"my req","description":"req description","latitude":"4.000000","longitude":"-456.123000","lander_comp_ote":null,"place_id":null,"locations":[],"hiring_manager":null}]);

        this.finishLoading();
      }, 200)

      return promise;
    }

    this.beginLoading();

    let promise = $.ajax({
      method: "GET",
      contentType: "application/json",
      url: `${this.basePath}/page/${this.getPageId()}/reqs/`
    })

    promise.always(() => {
      this.finishLoading();
    })

    return promise;
  },

  createJob: function() {

    if (this.appFunctions.isDebugMode()) {
      let promise = $.Deferred();
      promise.resolve({hiring_manager: {}});
      return promise;
    }

    this.beginLoading();

    let promise = $.ajax({
      method: "POST",
      contentType: "application/json",
      url: `${this.basePath}/page/${this.getPageId()}/reqs/`,
      data: JSON.stringify({
        title: "Untitled",
        description: "<h2><b>Enter your job description here.</b></h2>",
        latitude: null,
        longitude: null,
        compensation: null
      })
    })

    promise.always(() => {
      this.finishLoading();
    })

    return promise;
  },

  saveJob: function(job) {

    if (this.appFunctions.isDebugMode()) {
      let promise = $.Deferred();
      console.log("trying to save job: " , job);
      promise.resolve({});
      return promise;
    }

    this.beginLoading();

    let promise = $.ajax({
      method: "PATCH",
      contentType: "application/json",
      url: `${this.basePath}/page/${this.getPageId()}/reqs/${job.pk}/`,
      data: JSON.stringify(job)
    })

    promise.always(() => {
      this.finishLoading();
    })

    return promise;
  },

  deleteJob: function(id) {

    if (this.appFunctions.isDebugMode()) {
      let promise = $.Deferred();
      promise.resolve({});
      return promise;
    }

    this.beginLoading();

    let promise = $.ajax({
      method: "DELETE",
      contentType: "application/json",
      url: `${this.basePath}/page/${this.getPageId()}/reqs/${id}/`
    })

    promise.always(() => {
      this.finishLoading();
    })

    return promise;
  },

  reorderJobs: function(jobs) {

    if (this.appFunctions.isDebugMode()) {
      let promise = $.Deferred();
      promise.resolve([]);
      return promise;
    }

    this.beginLoading();

    let promise = $.ajax({
      method: "PATCH",
      contentType: "application/json",
      url: `${this.basePath}/page/${this.getPageId()}/reqs/reorder/`,
      data: JSON.stringify(jobs)
    })

    promise.always(() => {
      this.finishLoading();
    })

    return promise;
  },

  createHiringManager(manager, job) {
    if (this.appFunctions.isDebugMode()) {
      let promise = $.Deferred();
      let pk = Math.floor(Math.random() * 999 + 1);
      promise.resolve({
        first_name: "New",
        last_name: "Person",
        pk: pk,
        email: `new.person${pk}@test.com`,
        title: "new title"});

      return promise;
    }

    this.beginLoading();

    let managerToSave = $.extend({}, manager);
    managerToSave.req_id = job.pk;

    let promise = $.ajax({
      method: "POST",
      contentType: "application/json",
      url: `${this.basePath}/users/`,
      data: JSON.stringify(managerToSave)
    })

    promise.always(() => {
      this.finishLoading();
    })

    return promise;
  },

  updateHiringManager(hiringManager) {
    if (this.appFunctions.isDebugMode()) {
      let promise = $.Deferred();
      promise.resolve({});
      return promise;
    }

    // Make copy of hiring manager, then take out the crap we don't want to send the serve
    let manager = $.extend({}, hiringManager);
    let id = manager.id;

    let validAttr = {
      first_name: true,
      last_name: true,
      profile_photo_url: true,
      linkedin_url: true,
      title: true
    }

    $.each(manager, (key, value) => {
      if (!validAttr[key]) {
        delete manager[key]
      }
    })

    this.beginLoading();

    let promise = $.ajax({
      method: "PATCH",
      contentType: "application/json",
      url: `${this.basePath}/users/${id}/`,
      data: JSON.stringify(manager)
    })

    promise.always(() => {
      this.finishLoading();
    })

    return promise;
  },

  attachHiringManagerToReq(hiringManager, job) {
    if (this.appFunctions.isDebugMode()) {
      let promise = $.Deferred();
      promise.resolve({});
      return promise;
    }

    this.beginLoading();

    let promise = $.ajax({
      method: "PATCH",
      contentType: "application/json",
      url: `${this.basePath}/page/${this.getPageId()}/reqs/${job.pk}/`,
      data: JSON.stringify({
        hiring_manager: hiringManager
      })
    })

    promise.always(() => {
      this.finishLoading();
    })

    return promise;
  },

  loadHiringManagers(job) {
    if (this.appFunctions.isDebugMode()) {
      let promise = $.Deferred();

      this.beginLoading();

      setTimeout(() => {
        promise.resolve([
          {first_name: "Mary", last_name: "Sue", pk: 1, "linkedin_url": "http://www.google.com", email:"test1@test.com", title: "Great title", provisional: true},
          {first_name: "Bob", last_name: "Joe", pk: 2, "linkedin_url": "http://www.google.com", email:"test2@test.com", provisional: false},
          {first_name: "Test", last_name: "4", pk: 3, "linkedin_url": "http://www.google.com", email:"test3@test.com", title: "Awesome Title", provisional: true}
        ]);

        this.finishLoading();
      }, 1)

      return promise;
    }

    this.beginLoading(job);

    let promise = $.ajax({
      method: "GET",
      contentType: "application/json",
      url: `${this.basePath}/users/`
    })

    promise.always(() => {
      this.finishLoading();
    })

    return promise;
  },

  // Signup List
  loadSignups() {
    if (this.appFunctions.isDebugMode()) {
      let promise = $.Deferred();

      this.beginLoading();

      setTimeout(() => {
        promise.resolve([{
          "first_name": "",
          "last_name": "",
          "email": "test@test.com",
          "phone_number": "50864214142",

          "linkedin_url": "https://www.google.com"
        }, {
          "first_name": "super",
          "last_name": "",
          "email": "super@man.com",
          "phone_number": "",
          "linkedin_url": "https://www.yahoo.com"
        }, {
          "first_name": "super",
          "last_name": "man",
          "email": "",
          "phone_number": "8675309",
          "linkedin_url": "https://www.yahoo.com"
        }, {
          "first_name": "super",
          "last_name": "man",
          "email": "super@man.com",
          "phone_number": "8675309",
          "linkedin_url": ""
        }, {
          "first_name": "super",
          "last_name": "man",
          "email": "super@man.com",
          "phone_number": "8675309",
          "linkedin_url": "https://www.yahoo.com"
        }, {
          "first_name": "super",
          "last_name": "man",
          "email": "super@man.com",
          "phone_number": "8675309",
          "linkedin_url": "https://www.yahoo.com"
        }, {
          "first_name": "super",
          "last_name": "man",
          "email": "super@man.com",
          "phone_number": "8675309",
          "linkedin_url": "https://www.yahoo.com"
        }]);

        this.finishLoading();
      }, 10)

      return promise;
    }

    this.beginLoading();

    let promise = $.ajax({
      method: "GET",
      contentType: "application/json",
      url: `${this.basePath}/page/${this.getPageId()}/signups/`
    })

    promise.always(() => {
      this.finishLoading();
    })

    return promise;
  },

  addToSignups(user) {
    if (this.appFunctions.isDebugMode()) {
      let promise = $.Deferred();

      this.beginLoading();

      setTimeout(() => {
        promise.resolve({});

        this.finishLoading();
      }, 10)

      return promise;
    }

    this.beginLoading();


    let promise = $.ajax({
      method: "POST",
      contentType: "application/json",
      url: `${this.basePath}/page/${this.getPageId()}/signups/`,
      data: JSON.stringify(user)
    })

    promise.always(() => {
      this.finishLoading();
    })

    return promise;
  },

  applyCandidate(requestObject) {
    if (this.appFunctions.isDebugMode()) {
      let promise = $.Deferred();

      this.beginLoading();

      setTimeout(() => {
        promise.resolve({});

        this.finishLoading();
      }, 10)

      return promise;
    }

    this.beginLoading();


    let promise = $.ajax({
      method: "POST",
      contentType: "application/json",
      url: `${this.basePath}/page/apply/`,
      data: JSON.stringify(requestObject)
    })

    promise.always(() => {
      this.finishLoading();
    })

    return promise;
  },

  uploadResume(candidateId,files) {
    if (this.appFunctions.isDebugMode()) {
      let promise = $.Deferred();

      this.beginLoading();

      setTimeout(() => {
        promise.resolve({});

        this.finishLoading();
      }, 10)

      return promise;
    }

    this.beginLoading();

    // Create a formdata object and add the files
    var data = new FormData();
    $.each(files, function(key, value){
      data.append(key, value);
    });

    let promise = $.ajax({
      method: "POST",
      processData: false,
      contentType: false,
      url: `${this.basePath}/candidates/${candidateId}/resume/?session_id=${window.happie_page_data._metadata.session_id}`,
      data: data
    })

    promise.always(() => {
      this.finishLoading();
    })

    return promise;
  },

  getAllJobs() {
    if (this.appFunctions.isDebugMode()) {
      let promise = $.Deferred();

      this.beginLoading();

      setTimeout(() => {
        promise.resolve({});

        this.finishLoading();
      }, 10)

      return promise;
    }

    this.beginLoading();

    let promise = $.ajax({
      method: "GET",
      contentType: "application/json",
      url: `${this.basePath}/reqs/`,
    })

    promise.always(() => {
      this.finishLoading();
    })

    return promise;
  },

  addJobToPage(id) {
    if (this.appFunctions.isDebugMode()) {
      let promise = $.Deferred();

      this.beginLoading();

      setTimeout(() => {
        promise.resolve({});

        this.finishLoading();
      }, 10)

      return promise;
    }

    this.beginLoading();

    let promise = $.ajax({
      method: "POST",
      contentType: "application/json",
      url: `${this.basePath}/page/${this.getPageId()}/reqs/`,
      data: JSON.stringify({
        req_id: id
      })
    })

    promise.always(() => {
      this.finishLoading();
    })

    return promise;
  },

  // NOTE: this is only used if pusher fails to connect or hasn't connected yet
  reportEvent(data) {

    let promise = $.ajax({
      method: "POST",
      contentType: "application/json",
      url: `${this.basePath}/page/${this.getPageId()}/event/`,
      data: JSON.stringify(data)
    })

    return promise;
  }

}

export default api;
