// src: http://stackoverflow.com/questions/11300906/check-if-a-string-starts-with-http-using-javascript
const addProtocol = (url) => {
  if (!/^(f|ht)tps?:\/\//i.test(url)) {
      url = "https://" + url;
    }

    // If you keep hitting backspace, string will become this, so blank it out
    if (url === "https://https:/") {
      url = "https://";
    }

  return url;
}

export default addProtocol;
