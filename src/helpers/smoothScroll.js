import $ from 'jquery';

// TODO: revist to potentially improve performance if we did something in react instead of jQuery
const smoothScroll = function(selector, mode) {
  let offset = $(selector).offset().top;
  let containerOffset = $(".blocks-container").scrollTop();

  let offsetAmount = offset + containerOffset;

  // Add extra space for header in preview/prod
  if (mode !== "edit") {
    let headerHeight = $(".header-block").outerHeight();
    offsetAmount -= headerHeight;
  }

  $(".blocks-container").animate({
    scrollTop: offsetAmount
  }, 1000);
}

export default smoothScroll;
