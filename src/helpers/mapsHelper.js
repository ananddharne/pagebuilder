
import $ from "jquery";
const mapsHelper = {
  loaded: false,
  loadPromise: {},
  geocoder: null,

  load: function() {
    // TODO: Find a maps library for react that actually works, right now for the display map
    // we're just using the base JS library from google and asynchronously loading it but adding a
    // <script> tag in the head of the document

    this.loadPromise = $.Deferred();

    // TODO: This should only get called if google map script resolves,
    // but we should implement a failure case somehow
    window.gmapCallback = () => {

      this.geocoder = new window.google.maps.Geocoder();

      this.loadPromise.resolve();
    }

    // If google maps is already loaded, just resolve promise
    if (window.google && window.google.maps) {
      window.gmapCallback();
      return;
    }

    // If not loaded yet, load in new script
     var s = document.createElement("script");
     s.type = "text/javascript";
     s.src  = "https://maps.google.com/maps/api/js?callback=gmapCallback&key=AIzaSyC_EetH9JpwSXo8IN3kllzv-vK8kjvp7VE&libraries=places";

     $("head").append(s);
  },

  getLoadPromise: function() {
    return this.loadPromise;
  }
}

export default mapsHelper;
