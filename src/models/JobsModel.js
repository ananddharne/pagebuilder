const JobsModel = {
  id: "jobs",
  jobs: [],
  "title": "",
  "location": "",
  "category": "",
  "compensation": "",
  "description": "",
  "enabled": true,
  "hiringManagerImage": "",
  "hiringManagerFullName": "",
  "hiringManagerLinkedIn": ""
}

export default JobsModel;
