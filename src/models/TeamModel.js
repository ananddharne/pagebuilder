const TeamModel = {
  id: "team",
  teamMembers: [{}],
  background: "",
  backgroundType: "",
  opacity: "",
  opacityColor: "",
  zoom: "",
  parallax: false,
}

export default TeamModel;
