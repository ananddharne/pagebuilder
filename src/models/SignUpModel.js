const SignUpModel = {
  id: "signUp",
  background: "",
  backgroundType: "",
  opacity: "",
  opacityColor: "",
  zoom: "",
  parallax: false,
  header: "",
  headerDescription: "",
  textColor: "",
  signUpFields: [
    {
      "id": "first_name",
      "title": "First Name",
      "placeholder": "Enter your first name",
      "required": false,
      "value": "",
      "enabled": true,
      "readOnly": false
    },
    {
      "id": "last_name",
      "title": "Last Name",
      "placeholder": "Enter your last name",
      "required": false,
      "value": "",
      "enabled": true,
      "readOnly": false
    },
    {
      "id": "email",
      "title": "Email",
      "placeholder": "Enter your email address",
      "required": true,
      "value": "",
      "enabled": true,
      "readOnly": true
    },
    {
      "id": "phone_number",
      "title": "Phone",
      "placeholder": "Enter your phone number",
      "required": false,
      "value": "",
      "enabled": true
    },
    {
      "id": "linkedin_url",
      "title": "LinkedIn",
      "placeholder": "Enter your public LinkedIn profile link",
      "required": false,
      "value": "",
      "enabled": true
    }
  ]
}

export default SignUpModel;
