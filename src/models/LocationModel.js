const LocationModel = {
  id: "location",
  background: "",
  backgroundType: "",
  opacity: "",
  opacityColor: "",
  zoom: "",
  parallax: false,
  header: "",
  addressLine1: "",
  addressLine2: "",
  image: "",
  textColor: ""
}

export default LocationModel;
