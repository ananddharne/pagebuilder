const BottomImageModel = {
  id: "bottomImage",
  background: "",
  backgroundType: "",
  opacity: "",
  opacityColor: "",
  zoom: "",
  parallax: false
}

export default BottomImageModel;
