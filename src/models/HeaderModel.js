const HeaderModel = {
  id: "header",
  logo: "",
  title: "",
  keywords: "",
  description: "",
  buttonColor: "",
  buttonTextColor: ""
}

export default HeaderModel;
