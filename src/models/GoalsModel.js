const GoalsModel = {
  id: "goals",
  background: "",
  backgroundType: "",
  opacity: "",
  opacityColor: "",
  zoom: "",
  parallax: false,
  header: "",
  description: "",
  textColor: ""
}

export default GoalsModel;
