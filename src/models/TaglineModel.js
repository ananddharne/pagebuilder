const TaglineModel = {
  id: "tagline",
  background: "",
  backgroundType: "",
  opacity: "",
  opacityColor: "",
  zoom: "",
  parallax: false,
  header: "",
  description: "",
  textColor: ""
}

export default TaglineModel;
