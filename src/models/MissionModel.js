const MissionModel = {
  id: "mission",
  background: "",
  backgroundType: "",
  opacity: "",
  opacityColor: "",
  zoom: "",
  parallax: false,
  header: "",
  headerDescription: "",
  textColor: ""
}

export default MissionModel;
