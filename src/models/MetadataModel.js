const MetadataModel = {
  id: "_metadata",
  company: "",
  last_published_at: "",
  page_route: ""
}

export default MetadataModel;
