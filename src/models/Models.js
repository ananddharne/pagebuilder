/// Aggregate all the model files into one
/// Note that each model defines its own ID, although it should just match the file name
import MetadataModel from "MetadataModel"
import HeaderModel from "HeaderModel"
import TaglineModel from "TaglineModel"
import MissionModel from "MissionModel"
import GoalsModel from "GoalsModel"
import FooterModel from "FooterModel"
import TeamModel from "TeamModel"
import SignUpModel from "SignUpModel"
import BottomImageModel from "BottomImageModel"
import LocationModel from "LocationModel"
import MapModel from "MapModel"
import JobsModel from "JobsModel"
import ApplicationModel from "ApplicationModel"

const Models = {
  [MetadataModel.id]: MetadataModel, // id is _metadata, and should not be included in requests to/from server
  [HeaderModel.id]: HeaderModel,
  [TaglineModel.id]: TaglineModel,
  [MissionModel.id]: MissionModel,
  [GoalsModel.id]: GoalsModel,
  [TeamModel.id]: TeamModel,
  [SignUpModel.id]: SignUpModel,
  [BottomImageModel.id]: BottomImageModel,
  [LocationModel.id]: LocationModel,
  [MapModel.id]: MapModel,
  [JobsModel.id]: JobsModel,
  [ApplicationModel.id]: ApplicationModel,
  [FooterModel.id]: FooterModel
}

export default Models;
