const ApplicationModel = {
  id: "application",
  background: "",
  backgroundType: "",
  opacity: "",
  opacityColor: "",
  zoom: "",
  parallax: false,
  header: "",
  headerDescription: "",
  textColor: "",
  applicationFields: [
    {
      "id": "full_name",
      "title": "Your name?",
      "placeholder": "Enter your name...",
      "required": false,
      "value": "",
      "enabled": true,
      "readOnly": false,
      "page": 1
    },
    {
      "id": "email",
      "title": "Your email address?",
      "placeholder": "Enter your email address...",
      "required": true,
      "value": "",
      "enabled": true,
      "readOnly": true,
      "page": 1
    },
    {
      "id": "phone_number",
      "title": "Your mobile phone number?",
      "placeholder": "Enter your mobile phone number #...",
      "required": false,
      "value": "",
      "enabled": true,
      "page": 1
    },
    {
      "id": "title",
      "title": "Your most recent job title?",
      "placeholder": "Enter your most recent job title...",
      "required": false,
      "value": "",
      "enabled": true,
      "readOnly": true,
      "page": 2
    },
    {
      "id": "company_name",
      "title": "Your most recent employer?",
      "placeholder": "Enter your most recent employer...",
      "required": false,
      "value": "",
      "enabled": true,
      "page": 2
    },
    {
      "id": "linkedin_url",
      "title": "Your LinkedIn profile?",
      "placeholder": "Enter your public LinkedIn profile URL...",
      "required": true,
      "value": "",
      "enabled": true,
      "page": 3
    }
  ]
}

export default ApplicationModel;
