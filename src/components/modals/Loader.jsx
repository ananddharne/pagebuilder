import React, { Component } from 'react';
import ReactModal from "react-modal";
import classnames from "classnames";
import xOut from "x-out.svg";

class Dialog extends Component {

  componentWillMount() {

  }

  openModal = () => {

  }

  confirmClick = () => {
    if (this.props.confirm) {
      this.props.confirm();
    }
    this.closeModal();
  }

  cancelClick = () => {
    if (this.props.cancel) {
      this.props.cancel();
    }
    this.closeModal();
  }

  closeModal = () => {
    this.props.setActiveModal("");
  }

  render() {

    // If jobs block is not active, don't render anything
    if (this.props.getActiveModal().split(":")[0] !== "dialog") {
      return <span></span>
    }

    let overlayClassNames = classnames({
      "hp-modal-overlay": true,
      "with-controls-padding": this.props.getAppMode() === "edit"
    })


    return (
      <ReactModal
        className="hp-modal custom-dialog-modal"
        overlayClassName={overlayClassNames}
        onRequestClose={this.closeModal}
        isOpen={true}
      >
      <div className="modal-contents">
        <img src={xOut} alt="Close Modal" onClick={this.closeModal} className="close-out"></img>
        <div className="vertical-align-container dialog-content">
          <div className="vertical-aligner">
            <p>It looks like you've added at least one new hiring manager since you last saved your page. Would you like to notify them?</p>
            <div className="button-container">
              <button className="btn confirm" onClick={this.confirmClick}>Yes, notify them!</button>
              <button className="btn cancel" onClick={this.cancelClick}>Not now, thanks</button>
            </div>
          </div>
        </div>
      </div>
      </ReactModal>
    );
  }
}

export default Dialog;
