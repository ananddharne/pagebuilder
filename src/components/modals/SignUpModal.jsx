import React, { Component } from 'react';
import ReactModal from "react-modal";
import placeholder from "image-placeholder.svg";
import classnames from "classnames";
import xOut from "x-out.svg";
import Models from "Models";
import alerts from "alerts";
import $ from "jquery";
import api from "api";
import addProtocol from "addProtocol";

let hideOnNextUpdate = false;

class SignUpModal extends Component {

  componentWillMount() {
    this.setState({
      fieldErrors: []
    })
  }

  // Only update component if it's actually visible
  shouldComponentUpdate = () => {
    let active = this.props.getActiveModal() === "signUp";
    if (active) {
      hideOnNextUpdate = true;
      return true;
    } else if (hideOnNextUpdate) {
      hideOnNextUpdate = false;
      return true;
    } else {
      return false;
    }
  }

  openModal = () => {

  }

  closeModal = () => {
    this.props.setActiveModal("");
    this.props.setActiveBlock("signUp");
  }

  onSubmit = (e) => {
    // Disable default form functionality
    e.preventDefault();
  }

  inputChange = (e, index, id) => {
    let value = e.target.value;

    let fields = this.props.getAppState(this.props.modelId, "signUpFields").slice() || [];

    if (id === "linkedin_url") {
      value = addProtocol(value);
    }

    fields[index].value = value;

    this.props.setAppState(this.props.modelId, "signUpFields", fields);
  }

  dropdownClick = (element) => {
    this.props.setAppState(this.props.modelId, "signUpFields")

    let value = element.type;

    let fields = this.props.getAppState(this.props.modelId, "signUpFields").slice() || [];

    // TODO: Ensure select job is always first element in list
    fields[0].value = value;

    this.props.setAppState(this.props.modelId, "signUpFields", fields);

    // Set internal state so dropdown updates
    // TODO: Don't save this
    this.props.setAppState("signUp", "selectedJob", element.type);

    // Remove errors if they exist
    let errors = this.state.fieldErrors;
    errors[0] = null;
    this.setState({
      fieldErrors: errors
    })
  }

  inputBlur = (e, index) => {

    // The main purpose of this is to clear errors when you have them and fix them,
    // but not sure the best way to not just show errors on blur before submitting

    let value = e.target.value;
    let fields = this.props.getAppState(this.props.modelId, "signUpFields").slice() || [];
    let field = fields[index];

    let errors = this.state.fieldErrors;
    if (field.required && !value) {
      errors[index] = true;
      this.setState({
        fieldErrors: errors
      })
    } else {
      errors[index] = false;
      this.setState({
        fieldErrors: errors
      })
    }

  }

  keepInTouchClick = (e) => {
    // TODO: Actually API calls for user

    // Show errors on inputs that have them
    let fields = this.props.getAppState(this.props.modelId, "signUpFields").slice() || [];
    let errors = this.state.fieldErrors;
    let hasErrors = false;

    for (let i = 0; i < fields.length; i++) {
      let field = fields[i];
      if (field.required && !field.value) {
        errors[i] = true;
        hasErrors = true;
      } else {
        errors[i] = null;
      }
    }

    this.setState({
      fieldErrors: errors
    })

    if (hasErrors) {
      alerts.show({
        status: "error",
        message: "Please review the errors in the sign up form and try again."
      })
    } else {
      this.submitSignup();
    }

  }

  submitSignup = () => {

    let fields = this.props.getAppState(this.props.modelId, "signUpFields").slice();

    let requestObject = {};
    let jobId;
    // Build request object
    $.each(fields, (index, value) => {
      // Add value from array to object, unless it's the `selectedJob`, in which case send it separately
      requestObject[value.id] = value.value
    })

    if (this.props.getAppMode() === "edit") {
      alerts.show({
        status: "success",
        message: "This is just a preview, and the info you submitted won't be added to your signups list. Form entries on the live form, however, will be added to your signups list."
      })
      return;
    }

    // Not in edit mode, make actual API call
    console.log("sending request object " , requestObject);
    api.addToSignups(requestObject, jobId)
    .then(() => {
      this.props.reportEvent('signup');
      alerts.show({
        status: "success",
        message: "Thank you for signing up!"
      })
      this.closeModal();
    })
    .fail(() => {

    })

  }


  render() {

    let active = this.props.getActiveModal() === "signUp";

    let modalClass = classnames({
      "hp-modal": true,
      "dialog-dismissed": !active
    })

    let overlayClassNames = classnames({
      "hp-modal-overlay": true,
      "with-controls-padding": this.props.getAppMode() === "edit",
      "dialog-dismissed": !active
    })

    let modalAttr = {};
    if (!active) {
      modalAttr = {"aria-hidden": true};
    }

    let buttonColor = this.props.getAppState(Models.header.id, "buttonColor") || "#29c0d9";
    let buttonTextColor = this.props.getAppState(Models.header.id, "buttonTextColor") || "#FFFFFF";

    let buttonStyle = {
      color: buttonTextColor,
      backgroundColor: buttonColor,
    }

    let logo = this.props.getAppState("_metadata", "organization_logo_url") || this.props.getAppState("_metadata", "company_logo_url") || this.props.getAppState("header", "logo") || placeholder;

    let logoStyle = {
      border: "2px solid " + buttonColor,
      backgroundImage: "url('" + logo + "')"
    }

    // Don't show placeholder on preview
    if (logo===placeholder && this.props.getAppMode() !== "edit") {
      logoStyle.display = "none";
    }

    let fields = this.props.getAppState("signUp", "signUpFields");

    fields = fields.map((value, index) => {

      let labelStyle = {
        display: value.enabled ? "block" : "none"
      }
      let inputStyle = {
        border: this.state.fieldErrors[index] && value.required ? "1px solid #d0021b" : "1px solid #e0e7ee"
      }
      let errorStyle = {
        display: this.state.fieldErrors[index] && value.required ? "block" : "none"
      }

      if (value.hidden) {
        return false;
      }

      return <label key={value.id} style={labelStyle}>
        <span>{value.title}{value.required ? "*" : ""}</span>
        <input className="custom-input"
          style={inputStyle}
          placeholder={value.placeholder}
          onChange={(e) => {this.inputChange(e, index, value.id)}}
          onBlur={(e) => {this.inputBlur(e, index)}}
          value={value.value}></input>
        <span className="error-field" style={errorStyle}>This field is required. Please fill it in!</span>
      </label>
    })

    // Add job select
    /*
    let elementProps = {...this.props};
    let dropdownElements = [];

    let jobs = (this.props.getAppState("jobs", "jobs") || []).slice();

    dropdownElements = jobs.map((value, key) => {
      return {
        title: value.title,
        type: value.pk
      }
    })

    fields.unshift(<Dropdown
      key="signUp"
      modelKey="selectedJob"
      defaultTitle="Select Most Relevant Job"
      elements={dropdownElements}
      onClick={this.dropdownClick}
      hasErrors={this.state.fieldErrors[0]}
      {...elementProps}
      />)
      */


    return (
      <ReactModal
        contentLabel="sign-up-modal"
        className={modalClass}
        overlayClassName={overlayClassNames}
        onRequestClose={this.closeModal}
        isOpen={true}
      >
      <div className="modal-contents sign-up-modal" {...modalAttr} >
        <div className="close-out-container">
          <img src={xOut} alt="Close Modal" onClick={this.closeModal} className="close-out"></img>
        </div>
        <div className="logo-image-container vertical-align-container" style={logoStyle}></div>
        <form onSubmit={this.onSubmit}>
          <div>
            {fields}
          </div>
          <button className="btn btn-default" style={buttonStyle} onClick={this.keepInTouchClick} type="button">KEEP IN TOUCH</button>
        </form>

      </div>
      </ReactModal>
    );
  }
}

export default SignUpModal;
