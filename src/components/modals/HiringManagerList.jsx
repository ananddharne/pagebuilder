import React, { Component } from 'react';
import ReactModal from "react-modal";
import placeholder from "image-placeholder.svg";
import classnames from "classnames";
import api from "api";
import alerts from "alerts";
import appArrayStateHelpers from "appArrayStateHelpers";
import xOut from "x-out.svg";
import addProtocol from "addProtocol";

import addImg from "team/add-team-member.svg";
import addImgActive from "team/add-team-member-active.svg";

import $ from "jquery";

import CheckBox from "controls/elements/CheckBox"
// Dropzone not really used, just convenient functions to separate files after selecting
import Dropzone from "react-dropzone"

class HiringManagerList extends Component {

  componentWillMount() {

    this.setState({
      managers: [],
      managerToUpdate: -1, // pk of the manager we need to update when uploading images
      managersToSave: {},
      addImg: addImg,
      addRowImg: addImg,
      managerToCreate: {
        first_name: "",
        last_name: "",
        linkedin_url: "",
        title: "",
        email: "",
        profile_photo_url: null
      },
      activeRow: -1,
      createRowVisible: false
    })

  }

  loadHiringManagers = () => {

    // For now, don't load new managers if we already have loaded once
    if (this.state.managers.length > 0) {
      return;
    }

    api.loadHiringManagers()
    .done((response) => {
      this.setState({
        managers: response
      })
    })
    .fail((response) => {
      console.error("error loading managers")
    })
  }

  closeModal = () => {
    // Loop through managersToSave and save them all
    let promiseArray = [];
    $.each(this.state.managersToSave, (index, value) => {
      let promise = api.updateHiringManager(value);
      promiseArray.push(promise)
    })

    $.when.apply($, promiseArray)
    .done(() => {

    })
    .fail((e) => {
      console.error("failed to save managers " , e);
    })
    .always(() => {
      // Reset unsaved managers
      this.setState({
        managersToSave: {}
      })

      this.props.closeFunction();
    })

  }

  inputClick = (index) => {
    this.setState({
      activeRow: index
    })
  }

  inputChange = (pk, keyToUpdate, e) => {

    let inputValue = e.target.value;
    if (keyToUpdate === "linkedin_url") {
      inputValue = addProtocol(inputValue);
    }

    // Special pk "create" means our hiring manager to create, not actually updating an existing one
    if (pk === "create") {
      let newManager = this.state.managerToCreate;
      newManager[keyToUpdate] = inputValue;
      this.setState({
        managerToCreate: newManager
      })

      return;
    }

    // Find which item we're updating by pk then change the value
    let newManagers = this.state.managers.slice();
    let managerToSave;

    $.each(newManagers, (index, value) => {
      if (value.id === pk) {
        managerToSave = newManagers[index];

        managerToSave[keyToUpdate] = inputValue;
      }
    })

    let managersToSave = this.state.managersToSave;
    managersToSave[managerToSave.pk] = managerToSave;

    // Save to app state for job modal to know, then save to local state
    appArrayStateHelpers.set(this, "hiring_manager", "jobs", managerToSave);

    this.setState({
      managers: newManagers,
      managersToSave: managersToSave
    })
  }

  checkboxChange = (pk, e) => {

    let jobs = this.props.getAppState("jobs", "jobs")
    let jobToLink = jobs[this.props.index];

    if (e.target.checked === true) {
      // Find which manager we're setting/unsetting
      let managerToSet;
      $.each(this.state.managers, (index, value) => {
        if (value.id === pk) {
          managerToSet = value;
        }
      })
      this.attachManagerToJob(managerToSet, jobToLink)

      // Show dialog to confirm (and do actual linking after)
      this.props.setActiveDialog({
        description: `You selected ${managerToSet.first_name} ${managerToSet.last_name} as the featured team member for this job. Would you like to notify them via email?`,
        confirmText: "Yes, notify them!",
        cancelText: "Not now, thanks",
        confirm: () => {
          this.sendEmailToManager(managerToSet, jobToLink)
        }
      });

    } else {
      this.attachManagerToJob('', jobToLink)
    }
  }

  sendEmailToManager = (managerToSet, jobToLink) => {
    // TODO: Make API call
  }

  attachManagerToJob = (managerToSet, jobToLink) => {
    appArrayStateHelpers.set(this, "hiring_manager", "jobs", managerToSet);

    // Do actual link
    api.attachHiringManagerToReq(managerToSet, jobToLink)
    .done((response) => {
      console.log("Successfully linked to req:", response);
      if (response.hiring_manager !== null) {
        alerts.show({
          status: "success",
          message: `Great! ${managerToSet.first_name} ${managerToSet.last_name} is now the featured team member for your ${jobToLink.title} role, and will also receive notifications about applicants for this role going forward.`
        })
      } else {
        alerts.show({
          status: "success",
          message: `Great! You've unlinked the featured team member from your ${jobToLink.title} role, they will no longer receive notifications about applicants for this role going forward.`
        })
      }

    })
    .fail(() => {
      console.error("Failed to link to req")
    })
  }

  addManager = () => {
    let newManagers = this.state.managers.slice();

    // Get currently selected job
    let jobs = this.props.getAppState("jobs", "jobs")
    let job = jobs[this.props.index];

    api.createHiringManager(this.state.managerToCreate, job)
    .done((response) => {
      // Push new hiring manager and reset the "create" manager row
      newManagers.push(response);

      this.setState({
        managers: newManagers,
        managerToCreate: {
          first_name: "",
          last_name: "",
          linkedin_url: "",
          title: "",
          email: "",
          profile_photo_url: null
        }
      })

      // Set new manager to be the selected one for req
      let jobs = this.props.getAppState("jobs", "jobs")
      let jobToLink = jobs[this.props.index];

      this.attachManagerToJob(response, jobToLink);

      // Hide "create new" row
      this.setState({
        createRowVisible: false
      })

    })
    .fail((response) => {
      console.error("Failed to create hiring manager " , response);
      alerts.show({
        status: "error",
        message: "Uh oh! There was an error when you tried to create your hiring manager. Please check to make sure you entered a valid email address and try again."
      })
    })

  }

  addEnter = () => {
    this.setState({
      addImg: addImgActive
    })
  }

  addLeave = () => {
    this.setState({
      addImg: addImg
    })
  }

  addRowEnter = () => {
    this.setState({
      addRowImg: addImgActive
    })
  }

  addRowLeave = () => {
    this.setState({
      addRowImg: addImg
    })
  }

  imageUploadClick = (pk) => {
    // Need to set state here because passing pk in onDrop() wasn't working
    this.setState({
      managerToUpdate: pk
    })
    this.refs.dropzone.open();
  }

  onDrop = (files) => {

    api.saveImage(files)
    .done((data) => {

      // Show success message
      alerts.show({
        status: "success",
        message: "Successfully uploaded image!"
      })

      let url = data.url;

      // Special case for creating new manager
      if (this.state.managerToUpdate === "create") {
        let newManager = this.state.managerToCreate;
        newManager.profile_photo_url = url;
        this.setState({
          managerToCreate: newManager
        })
        return;
      }

      let managerToUpdate;

      let newManagers = this.state.managers.slice();
      $.each(newManagers, (index, value) => {
        if (this.state.managerToUpdate === value.id) {
          managerToUpdate = newManagers[index]
          managerToUpdate.profile_photo_url = url;
        }
      })

      let managersToSave = this.state.managersToSave;
      managersToSave[managerToUpdate.pk] = managerToUpdate;

      this.setState({
        managers: newManagers,
        managersToSave: managersToSave
      })

    })
    .fail((e) => {
      console.error("Error uploading image:" , e);
      alerts.show({
        status: "error",
        message: "Uh oh! There was an error when you tried to upload your image. Please make sure the image file doesn't look funky, and try again!"
      })
    })

  }

  rowClick = (disabled) => {
    // If it's a disabled row, show a warning
    if (disabled) {
      alerts.show({
        status: "warning",
        message: "For security reasons, team members with existing accounts are the only ones who can edit their accounts. Let them know that if you think they should update their profiles!"
      })
    }
  }

  createRowKeyPress = (e) => {
    // "Submit" new manager if we press return while editing that row
    if (e.which === 13) {
      this.addManager();
    }
  }

  addManagerButtonClick = () => {
    this.setState({
      createRowVisible: true
    })
  }

  render() {

    let active = this.props.open;

    let modalClass = classnames({
      "hp-modal manager-list-modal": true,
      "dialog-dismissed": !active
    })
    let overlayClassNames = classnames({
      "hp-modal-overlay": true,
      "with-controls-padding edit-mode": this.props.getAppMode() === "edit",
      "dialog-dismissed": !active
    })

    let modalAttr = {};
    if (!active) {
      modalAttr = {"aria-hidden": true};
    }

    // Get currently selected job
    let jobs = this.props.getAppState("jobs", "jobs")
    let job = jobs[this.props.index];

    let dottedBorder = {
      border: "1px dotted #979797",
      padding: "2px"
    }

    let managerList = this.state.managers.map((value, key) => {

      let dropImage = value.profile_photo_url || placeholder;
      let dropImageStyle = value.profile_photo_url ? {} : dottedBorder
      let rowClass = classnames({
        "hiring-manager-row": true,
        "active": key === this.state.activeRow
      })

      // TODO: Re-enable when provisional flag is sent
      let disabled = false;

      return <div key={value.id} className={rowClass} onClick={() => this.rowClick(disabled)}>

        <div className="column-1">
          <CheckBox onChange={(e) => {this.checkboxChange(value.id, e)}} checked={job && job.hiring_manager && value.email === job.hiring_manager.email} onClick={() => this.inputClick(key)} />
        </div>
        <div className="column-2">
          <Dropzone
            id={`manager-dropzone-${value.id}`}
            className="image-dropzone"
            disableClick={true}
            multiple={false}
            ref="dropzone"
            onDrop={(files) => {this.onDrop(files)}}>
          </Dropzone>
          <img alt="Upload" src={dropImage} style={dropImageStyle} className="upload-image" role="button"
            onClick={()=>{
              if (!disabled) { this.imageUploadClick(value.id)} }
            }></img>
        </div>
        <div className="column-3">
          <input value={value.first_name} spellCheck="false" onChange={(e) => {this.inputChange(value.id, "first_name", e)}} onClick={() => this.inputClick(key)} disabled={disabled}></input>
        </div>
        <div className="column-4">
          <input value={value.last_name} spellCheck="false" onChange={(e) => {this.inputChange(value.id, "last_name", e)}} onClick={() => this.inputClick(key)} disabled={disabled}></input>
        </div>
        <div className="column-5">
          <input value={value.title || ""} spellCheck="false" onChange={(e) => {this.inputChange(value.id, "title", e)}} onClick={() => this.inputClick(key)} disabled={disabled}></input>
        </div>
        <div className="column-6">
          <input value={value.email} spellCheck="false" onChange={(e) => {this.inputChange(value.id, "email", key)}} onClick={() => this.inputClick(key)} disabled={true}></input>
        </div>
        <div className="column-7">
          <input value={value.linkedin_url || ""} spellCheck="false" onChange={(e) => {this.inputChange(value.id, "linkedin_url", e)}} onClick={() => this.inputClick(key)} disabled={disabled}></input>
        </div>
      </div>
    })

    // Push create row
    let managerDropImage = this.state.managerToCreate.profile_photo_url || placeholder;
    let managerDropImageStyle = this.state.managerToCreate.profile_photo_url ? {} : dottedBorder;
    let createRowKey = this.state.managers.length;
    let createRowStyle = this.state.createRowVisible === true ? {display: "block"} : {display: "none"};
    let rowClass = classnames({
      "hiring-manager-row": true,
      "active": this.state.activeRow === createRowKey
    })
    let createRow = <div key="create-row" className={rowClass} onKeyPress={this.createRowKeyPress} style={createRowStyle}>

      <div className="column-1">
        <img alt="Create Manager" src={this.state.addImg} onMouseEnter={this.addEnter} onMouseLeave={this.addLeave} role="button" onClick={this.addManager} className="create-manager-button"></img>
      </div>
      <div className="column-2">
        <Dropzone
          id={`manager-dropzone-create`}
          className="image-dropzone"
          disableClick={true}
          multiple={false}
          ref="dropzone"
          onDrop={(files) => {this.onDrop(files)}}>
        </Dropzone>
        <img alt="Upload" src={managerDropImage} style={managerDropImageStyle} className="upload-image" role="button" onClick={()=>{this.imageUploadClick("create")}}></img>

      </div>
      <div className="column-3">
        <input value={this.state.managerToCreate.first_name} placeholder="First Name" spellCheck="false" onChange={(e) => {this.inputChange("create", "first_name", e)}} onClick={() => this.inputClick(createRowKey)}></input>
      </div>
      <div className="column-4">
        <input value={this.state.managerToCreate.last_name} placeholder="Last Name" spellCheck="false" onChange={(e) => {this.inputChange("create", "last_name", e)}} onClick={() => this.inputClick(createRowKey)}></input>
      </div>
      <div className="column-5">
        <input value={this.state.managerToCreate.title} placeholder="Professional Title" spellCheck="false" onChange={(e) => {this.inputChange("create", "title", e)}} onClick={() => this.inputClick(createRowKey)}></input>
      </div>
      <div className="column-6">
        <input value={this.state.managerToCreate.email} placeholder="email@gethappie.me" spellCheck="false" onChange={(e) => {this.inputChange("create", "email", e)}} onClick={() => this.inputClick(createRowKey)}></input>
      </div>
      <div className="column-7">
        <input value={this.state.managerToCreate.linkedin_url} placeholder="https://linkedin.com/yourprofile" spellCheck="false" onChange={(e) => {this.inputChange("create", "linkedin_url", e)}} onClick={() => this.inputClick(createRowKey)}></input>
      </div>
    </div>;

    // Add create row and button to beginning of array
    let createButtonRow = <div className="add-manager-button-container" key="add-manager-button">
      <img alt="Add Manager" src={this.state.addRowImg} onMouseEnter={this.addRowEnter} onMouseLeave={this.addRowLeave} role="button" onClick={this.addManagerButtonClick}></img>
    </div>

    managerList.unshift(createRow);
    managerList.unshift(createButtonRow)

    return (
      <ReactModal
        contentLabel="manager-list-modal"
        className={modalClass}
        overlayClassName={overlayClassNames}
        onRequestClose={this.closeModal}
        isOpen={true}
      >
      <div className="modal-contents" {...modalAttr} >
        <div className="info-container">
          <img src={xOut}
            alt="Close Modal"
            role="button"
            onClick={this.closeModal}
            className="close-out"
            ></img>

          <div className="hiring-manager-list-container">
            <div className="hiring-manager-row header-row">
              <div className="column-1"></div>
              <div className="column-2">Photo</div>
              <div className="column-3">First Name</div>
              <div className="column-4">Last Name</div>
              <div className="column-5">Title</div>
              <div className="column-6">Email</div>
              <div className="column-7">LinkedIn</div>
            </div>
            {managerList}
          </div>
        </div>
      </div>
      </ReactModal>
    );
  }
}

export default HiringManagerList;
