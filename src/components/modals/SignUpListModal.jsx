import React, { Component } from 'react';
import ReactModal from "react-modal";
import classnames from "classnames";
import xOut from "x-out.svg";
import csvImg from "jobs/excel.svg";
import $ from "jquery";
import json2csv from "json2csv";

import Tooltip from "../controls/elements/Tooltip";

let hideOnNextUpdate = false;

class SignUpListModal extends Component {

  componentWillMount() {

  }

  // Only update component if it's actually visible
  shouldComponentUpdate = () => {
    let active = this.props.getActiveModal() === "signUpList";
    if (active) {
      hideOnNextUpdate = true;
      return true;
    } else if (hideOnNextUpdate) {
      hideOnNextUpdate = false;
      return true;
    } else {
      return false;
    }
  }

  openModal = () => {
    console.log("in open");
  }

  closeModal = () => {
    this.props.setActiveModal("");
    this.props.setActiveBlock("signUp");
  }

  downloadClick = () => {

    // Show all fields even if they aren't enabled, could be improved to exclude
    let fields = [
      {
        label: "First Name",
        value: "first_name"
      },
      {
        label: "Last Name",
        value: "last_name"
      },
      {
        label: "Email",
        value: "email"
      },
      {
        label: "Phone",
        value: "phone_number"
      },
      {
        label: "LinkedIn Profile",
        value: "linkedin_url"
      }
    ]

    let signups = this.props.getAppState("_metadata", "signups") || [];
    signups = signups.map((value, index) => {
      let emailField = value.email_addresses || [{address: null}];
      let email = emailField[0].address;

      let phoneField = value.phone_numbers || [{number: null}];
      let phone_number = phoneField[0].number;

      value.email = email;
      value.phone_number = phone_number;
      return value;
    })

    let result = json2csv({
      data: signups,
      fields: fields
    })

    // Create a hidden <a> link and click it
    // src: http://stackoverflow.com/questions/14964035/how-to-export-javascript-array-info-to-csv-on-client-side
    let csv = "data:text/csv;charset=utf-8," + result;
    let encodedUri = encodeURI(csv);
    let link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "Happie-Signups.csv");
    document.body.appendChild(link); // Required for FF

    link.click();

    document.body.removeChild(link);
  }


  render() {

    let active = this.props.getActiveModal() === "signUpList";

    let modalClass = classnames({
      "hp-modal sign-up-list-modal": true,
      "dialog-dismissed": !active
    })

    let overlayClassNames = classnames({
      "hp-modal-overlay": true,
      "with-controls-padding": this.props.getAppMode() === "edit",
      "dialog-dismissed": !active
    })

    let modalAttr = {};
    if (!active) {
      modalAttr = {"aria-hidden": true};
    }

    // Table headers
    let fields = this.props.getAppState("signUp", "signUpFields");
    // Convert array of fields to object so we can easily hide/show columns
    let fieldMap = {};
    $(fields).each((key, value) => {
      fieldMap[value.id] = value.enabled
    })

    fields = fields.map((value, index) => {

      let rowStyle = {
        display: value.enabled ? "table-cell" : "none"
      }

      let displayTitle = value.title;
      if (value.id === "first_name" || value.id === "last_name") {
        displayTitle = "Name";
      }

      // Hide last name if first name was defined
      if (value.id === "last_name" && fieldMap.first_name) {
        rowStyle.display = "none";
      }

      let rowKey = `row-${index}`;

      // TODO: First name / Last name merge

      return <th key={rowKey} style={rowStyle}>{displayTitle}</th>
    })



    // Table data to display
    let signups = this.props.getAppState("_metadata", "signups") || [];
    let data = signups.map((value, key) => {
      let rowKey = `row-${key}`;

      // Emails and phone numbers come back in array, so extract values
      // TODO: Get value based on type, right now we're just getting the first one
      let emailField = value.email_addresses || [{address: null}];
      let email = emailField[0].address;

      let phoneField = value.phone_numbers || [{number: null}];
      let phone_number = phoneField[0].number;

      // TODO: Kind of hard to read, could be cleaned up with factory function perhaps
      return <tr key={rowKey}>
        <td style={fieldMap.first_name || fieldMap.last_name ? {display: "table-cell"} : {display: "none"}}>
          {value.first_name || value.last_name ? `${value.first_name} ${value.last_name}` : "-"}
        </td>
        <td style={fieldMap.email ? {display: "table-cell"} : {display: "none"}}>
          {email ? <a target="_blank" href={`mailto:${email}`}>{email}</a> : "-"}
        </td>
        <td style={fieldMap.phone_number ? {display: "table-cell"} : {display: "none"}}>
          {phone_number || "-"}
        </td>
        <td style={fieldMap.linkedin_url ? {display: "table-cell"} : {display: "none"}}>
          {value.linkedin_url ? <a target="_blank" href={value.linkedin_url}>Profile Link</a>  : "-"}
        </td>
      </tr>
    })

    let table = <table>
      <thead>
        <tr>
          {fields}
        </tr>
      </thead>
      <tbody>
        {data}
      </tbody>
    </table>;

    let tooltipId = this.props.modelId + "-custom-tooltip";
    let tooltipMessage = this.props.tooltip || "Your Community Signups are candidates who aren't ready to make a move yet but are interested in staying in touch. Keep them engaged by nurturing them with regular updates. You can also view them in the 'My Candidates' section of Happie.";


    return (
      <ReactModal
        contentLabel="sign-up-list-modal"
        className={modalClass}
        overlayClassName={overlayClassNames}
        onRequestClose={this.closeModal}
        isOpen={true}
      >
      <div className="modal-contents sign-up-list" {...modalAttr} >
        <img src={xOut} alt="Close Modal" onClick={this.closeModal} className="close-out"></img>


        <h1>Community Signups <Tooltip {...this.props} tooltip={tooltipMessage} id={tooltipId} /></h1>
        <button className="btn" onClick={this.downloadClick}><img src={csvImg} alt="download csv"></img>Download as Excel file</button>
        {table}

      </div>
      </ReactModal>
    );
  }
}

export default SignUpListModal;
