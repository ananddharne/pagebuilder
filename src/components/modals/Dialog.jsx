import React, { Component } from 'react';
import ReactModal from "react-modal";
import classnames from "classnames";
import xOut from "x-out.svg";

let hideOnNextUpdate = false;

class Dialog extends Component {

  componentWillMount() {

  }

  // Only update component if it's actually visible
  shouldComponentUpdate = () => {
    if (this.props.getActiveDialog()) {
      hideOnNextUpdate = true;
      return true;
    } else if (hideOnNextUpdate) {
      hideOnNextUpdate = false;
      return true;
    } else {
      return false;
    }
  }

  openModal = () => {

  }

  confirmClick = () => {
    if (this.props.getActiveDialog().confirm) {
      this.props.getActiveDialog().confirm();
    }
    this.closeModal();
  }

  cancelClick = () => {
    if (this.props.getActiveDialog().cancel) {
      this.props.getActiveDialog().cancel();
    }
    this.closeModal();
  }

  closeModal = () => {
    this.props.setActiveDialog("");
  }

  render() {

    // If active dialog is not defined, return empty object.
    let dialogOptions = this.props.getActiveDialog() || {};
    let active = this.props.getActiveDialog();

    let modalClass = classnames({
      "hp-modal custom-dialog-modal": true,
      "dialog-dismissed": !active
    })

    let overlayClassNames = classnames({
      "hp-modal-overlay custom-dialog-modal-overlay": true,
      "with-controls-padding": this.props.getAppMode() === "edit",
      "dialog-dismissed": !active
    })

    // TODO: Apply this to actual react modal, not just contents
    let modalAttr = {};
    if (!active) {
      modalAttr = {"aria-hidden": true};
    }



    return (
      <ReactModal
        contentLabel="dialog"
        className={modalClass}
        overlayClassName={overlayClassNames}
        onRequestClose={this.closeModal}
        aria-hidden={true}
        isOpen={true}
      >
      <div className="modal-contents" {...modalAttr}>
        <img src={xOut} alt="Close Modal" role="button" onClick={this.closeModal} className="close-out"></img>
        <div className="vertical-align-container dialog-content">
          <div className="vertical-aligner">
            <p>{dialogOptions.description}</p>
            <div className="button-container">
              <button className="btn confirm" onClick={this.confirmClick}>{dialogOptions.confirmText}</button>
              <button className="btn cancel" onClick={this.cancelClick}>{dialogOptions.cancelText}</button>
            </div>
          </div>
        </div>
      </div>
      </ReactModal>
    );
  }
}

export default Dialog;
