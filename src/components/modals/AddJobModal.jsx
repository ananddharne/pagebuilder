import React, { Component } from 'react';
import ReactModal from "react-modal";
import classnames from "classnames";
import xOut from "x-out.svg";
import searchImg from "search.svg";
import addNew from "add-new-icon.svg";
import briefcase from "briefcase-active.svg";
import api from "api";
import alerts from "alerts";
import $ from "jquery";

let hideOnNextUpdate = false;

class AddJobModal extends Component {

  componentWillMount() {
    this.setState({
      allJobs: [],
      search: ""
    });

    // Only get all jobs if we are in edit mode
    if (this.props.getAppMode() === "edit") {
      api.getAllJobs()
      .done((data) => {
        this.setState({
          allJobs: data
        });
      })
      .fail((e) => {
        console.error("Error getting all jobs:" , e);
        alerts.show({
          status: "error",
          message: "Uh oh! There was an error getting all jobs! Refresh the page and try again!"
        })
      })
    }
    
  }

  // Only update component if it's actually visible
  shouldComponentUpdate = () => {
    let active = this.props.getActiveModal().split(":")[0] === "addJob";
    if (active) {
      hideOnNextUpdate = true;
      return true;
    } else if (hideOnNextUpdate) {
      hideOnNextUpdate = false;
      return true;
    } else {
      return false;
    }
  }

  closeModal = () => {
    this.props.setActiveModal("");
    this.props.setActiveBlock("goals");
  }

  searchChange = (e) => {
    this.setState({
      search: e.target.value
    });
  }

  addJob = (id) => {
    // Make copy of array with slice() before modifying and saving
    let jobs = (this.props.getAppState(this.props.modelId, "jobs") || []).slice();

    api.addJobToPage(id)
    .done((data) => {
      // Push the job into the array
      jobs.push(data);
      this.props.setAppState(this.props.modelId, "jobs" , jobs);
      alerts.show({
        status: "success",
        message: "Woohoo! You successfully added your job to this page! Be sure to save and publish the changes you made so that candidates can see the job."
      })
    })
    .fail((e) => {
      alerts.show({
        status: "error",
        message: "Whoopsie! For some reason, there was an error adding your job to your page. Please refresh the window, and try again. Sorry for the inconvenience!"
      })
    });
  }

  createNewJob = () => {
    // Make copy of array with slice() before modifying and saving
    let jobs = (this.props.getAppState(this.props.modelId, "jobs") || []).slice();

    // Just for debug mode
    if (this.props.isDebugMode()) {
      let lastId = 0;
      if (jobs.length > 0) {
        lastId = jobs[jobs.length - 1].id;
      }

      jobs.push({
        title: "",
        enabled: true,
        id: lastId + 1
      })

      this.props.setAppState(this.props.modelId, "jobs" , jobs);
      this.props.setActiveModal(`jobs:${jobs.length - 1}`)
      this.props.setActiveBlock(`jobs`);

      return;
    }

    // Actual API call
    api.createJob()
    .done((data) => {
      jobs.push(data);
      this.props.setAppState(this.props.modelId, "jobs" , jobs);

      this.props.setActiveModal(`jobs:${jobs.length - 1}`)
      this.props.setActiveBlock(`jobs`);

    })
    .fail((e) => {
      console.error("Error creating job:" , e);
      alerts.show({
        status: "error",
        message: "Uh oh! There was an error when you tried to create your job. Please try again!"
      })
    })
  }

  render() {

    // If jobs block is not active, don't render anything
    let active = this.props.getActiveModal().split(":")[0] === "addJob";

    let modalClass = classnames({
      "hp-modal": true,
      "dialog-dismissed": !active
    })

    let overlayClassNames = classnames({
      "hp-modal-overlay": true,
      "with-controls-padding": this.props.getAppMode() === "edit",
      "dialog-dismissed": !active
    })

    let modalAttr = {};
    if (!active) {
      modalAttr = {"aria-hidden": true};
    }

    // All jobs
    let allJobs = this.state.allJobs;

    // Already attached jobs
    let attachedJobs = this.props.getAppState(this.props.modelId, "jobs") || [];

    let jobsToAdd = allJobs.map((value, index) => {
      // Filter out jobs that are already attached to this page
      let attached = false;
      $(attachedJobs).each((k,v) => {
        if (v.id === value.id) {
          attached = true;
          return false;
        }
      });
      // Filter using the search
      if (value.title.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1 && !attached) {
        let key = value.title + ":" + index;
        let title = value.title || "Untitled"
        return <li key={key} role="button" onClick={() => this.addJob(value.id)}>
          <span className="row-contents">
            <span>{title}</span>
          </span>
        </li>
      } else {
        return false;
      }
    })

    return (
      <ReactModal
        contentLabel="add-job-modal"
        className={modalClass}
        overlayClassName={overlayClassNames}
        onRequestClose={this.closeModal}
        isOpen={true}
      >
      <div className="modal-contents add-job-modal" {...modalAttr}>
        <img role="button" src={xOut} alt="Close Modal" onClick={this.closeModal} className="close-out"></img>
        <h2>Select an existing job or create a new one to add to your page.</h2>
        <div className="input-container">
          <img src={searchImg} alt="Search" />
          <input type="text" placeholder="Search by title..." value={this.state.search} onChange={this.searchChange} />
        </div>
        <button onClick={this.createNewJob}>
          <img src={addNew} alt="Add a new job" />
          ADD A NEW JOB
        </button>
        <div className="tab-container">
          <span className="tab">
            <img src={briefcase} alt="All jobs" />
            All jobs
          </span>
        </div>
        <ul>
          {jobsToAdd}
        </ul>
      </div>
      </ReactModal>
    );
  }
}

export default AddJobModal;
