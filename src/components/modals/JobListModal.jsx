import React, { Component } from 'react';
import ReactModal from "react-modal";
import placeholder from "image-placeholder.svg";
import classnames from "classnames";
import xOut from "x-out.svg";

let hideOnNextUpdate = false;

class JobListModal extends Component {

  componentWillMount() {

  }

  // Only update component if it's actually visible
  shouldComponentUpdate = () => {
    let active = this.props.getActiveModal().split(":")[0] === "jobList";
    if (active) {
      hideOnNextUpdate = true;
      return true;
    } else if (hideOnNextUpdate) {
      hideOnNextUpdate = false;
      return true;
    } else {
      return false;
    }
  }

  openModal = () => {

  }

  linkClick = (index) => {
    let modal = `jobs:${index}`;
    this.props.setActiveModal(modal)
    this.props.setActiveBlock("jobs");
  }

  closeModal = () => {
    this.props.setActiveModal("");
    this.props.setActiveBlock("goals");
  }

  render() {

    // If jobs block is not active, don't render anything
    let active = this.props.getActiveModal().split(":")[0] === "jobList";

    let modalClass = classnames({
      "hp-modal": true,
      "dialog-dismissed": !active
    })

    let overlayClassNames = classnames({
      "hp-modal-overlay": true,
      "with-controls-padding": this.props.getAppMode() === "edit",
      "dialog-dismissed": !active
    })

    let modalAttr = {};
    if (!active) {
      modalAttr = {"aria-hidden": true};
    }

    let buttonColor = this.props.getAppState("header", "buttonColor") || "#29c0d9";

    let logo = this.props.getAppState("_metadata", "organization_logo_url") || this.props.getAppState("_metadata", "company_logo_url") || this.props.getAppState("header", "logo") || placeholder;

    let logoStyle = {
      border: "2px solid " + buttonColor,
      backgroundImage: "url('" + logo + "')"
    }
    if (logo===placeholder && this.props.getAppMode() !== "edit") {
      logoStyle.display = "none";
    }
    let linkStyle = {
      color: buttonColor
    }

    let jobs = this.props.getAppState(this.props.modelId, "jobs") || [];

    let placeholderText = <span></span>;
    if (this.props.getAppMode() === "edit" && jobs.length < 1) {
      placeholderText = <p style={{"textAlign": "center"}}><b>Click the 'add' button on the right to begin creating your new job!</b></p>
    }

    jobs = jobs.map((value, index) => {

      let key = value.title + ":" + index;
      let title = value.title || "Untitled"
      return <a key={key} style={linkStyle} onClick={() => {
        // Set the current job so it will apply to the correct job
        this.props.setCurrentJob(value);
        this.linkClick(index);
        this.props.reportEvent('job_viewed',[value.id]);
      }}><b>{title}</b></a>
    })


    return (
      <ReactModal
        contentLabel="job-list-modal"
        className={modalClass}
        overlayClassName={overlayClassNames}
        onRequestClose={this.closeModal}
        isOpen={true}
      >
      <div className="modal-contents jobs-list-modal" {...modalAttr}>
        <img src={xOut} alt="Close Modal" onClick={this.closeModal} className="close-out"></img>
        <div className="logo-image-container vertical-align-container" style={logoStyle}></div>
        {placeholderText}
        {jobs}
      </div>
      </ReactModal>
    );
  }
}

export default JobListModal;
