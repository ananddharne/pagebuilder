import React, { Component } from 'react';
import ReactModal from "react-modal";
import classnames from "classnames";
import xOut from "x-out.svg";
import help1 from "help/video-help-1.png";
import help2 from "help/video-help-2.png";

class VideoUploadHelpModal extends Component {

  componentWillMount() {

  }

  openModal = () => {

  }

  closeModal = () => {
    this.props.setActiveModal("");
    this.props.setActiveBlock("signUp");
  }


  render() {

    let active = this.props.getActiveModal() === "video";

    let modalClass = classnames({
      "hp-modal video-help-modal": true,
      "dialog-dismissed": !active
    })

    let overlayClassNames = classnames({
      "hp-modal-overlay custom-dialog-modal-overlay": true,
      "with-controls-padding": this.props.getAppMode() === "edit",
      "dialog-dismissed": !active
    })

    let modalAttr = {};
    if (!active) {
      modalAttr = {"aria-hidden": true};
    }



    return (
      <ReactModal
        contentLabel="video-upload-help-modal"
        className={modalClass}
        overlayClassName={overlayClassNames}
        onRequestClose={this.closeModal}
        isOpen={true}
      >
      <div className="modal-contents" {...modalAttr}>
        <img src={xOut} alt="Close Modal" role="button" onClick={this.closeModal} className="close-out"></img>
        <div>
          <img src={help1} alt="help-window-1"></img>
          <img src={help2} alt="help-window-2"></img>
        </div>
      </div>
      </ReactModal>
    );
  }
}

export default VideoUploadHelpModal;
