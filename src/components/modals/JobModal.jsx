import React, { Component } from 'react';
import ReactModal from "react-modal";
import placeholder from "image-placeholder.svg";
import moneyImg from "jobs/money.svg";
import pinImg from "jobs/pin.svg";
import linkedInImg from 'team/linkedIn.svg';
import classnames from "classnames";
import api from "api";
import alerts from "alerts";
import appArrayStateHelpers from "appArrayStateHelpers";
import SingleLineEditor from "blocks/elements/SingleLineEditor";
import xOut from "x-out.svg";
import appliedImg from "applied.svg";
import upArrow from "up-arrow.svg";
import downArrow from "down-arrow.svg";
import GoogleMap from "../blocks/elements/GoogleMap";
import AddressPicker from "../controls/elements/AddressPicker";

import editImg from "jobs/edit-manager.svg";
import editImgActive from "jobs/edit-manager-active.svg";

import HiringManagerList from "./HiringManagerList";

import createRichButtonsPlugin from "draft-js-richbuttons-plugin";
import addProtocol from "addProtocol";

import $ from "jquery";
import Froala from 'blocks/elements/Froala';
import Dropdown from 'controls/elements/Dropdown';

let hideOnNextUpdate = false;

class JobModal extends Component {

  componentWillMount() {
    // Need to instantiate new plugin for each instance of editor
    let richButtonsPlugin = createRichButtonsPlugin();
    this.setState({
      plugin: richButtonsPlugin,
      collapseArrow: downArrow,
      collapsed: false,
      managerListOpen: false,
      editImg: editImg,
      jobFunctions: [{"title":"Select a Job Function","type":""},{"title":"Accounting","type":"Accounting"},{"title":"Administrative","type":"Administrative"},{"title":"Arts and Design","type":"Arts and Design"},{"title":"Business Development","type":"Business Development"},{"title":"Community & Social Services","type":"Community & Social Services"},{"title":"Consulting","type":"Consulting"},{"title":"Education","type":"Education"},{"title":"Engineering","type":"Engineering"},{"title":"Entrepreneurship","type":"Entrepreneurship"},{"title":"Finance","type":"Finance"},{"title":"Healthcare Services","type":"Healthcare Services"},{"title":"Human Resources","type":"Human Resources"},{"title":"Information Technology","type":"Information Technology"},{"title":"Legal","type":"Legal"},{"title":"Marketing","type":"Marketing"},{"title":"Media & Communications","type":"Media & Communications"},{"title":"Military & Protective Services","type":"Military & Protective Services"},{"title":"Operations","type":"Operations"},{"title":"Product Management","type":"Product Management"},{"title":"Program & Product Management","type":"Program & Product Management"},{"title":"Purchasing","type":"Purchasing"},{"title":"Quality Assurance","type":"Quality Assurance"},{"title":"Real Estate","type":"Real Estate"},{"title":"Research","type":"Research"},{"title":"Sales","type":"Sales"},{"title":"Support","type":"Support"}]
    })
  }

  // Only update component if it's actually visible
  shouldComponentUpdate = () => {
    let active = this.props.getActiveModal().split(":")[0] === "jobs";
    if (active) {
      hideOnNextUpdate = true;
      return true;
    } else if (hideOnNextUpdate) {
      hideOnNextUpdate = false;
      return true;
    } else {
      return false;
    }
  }

  openModal = () => {

  }

  closeModal = () => {
    if (this.props.jobPage) {
      return;
    }
    this.props.setActiveModal("");
    this.props.setActiveBlock("goals");
  }

  textChange = (key, text) => {

    let previousText = appArrayStateHelpers.get(this, key, "jobs");

    if (previousText !== text) {
      appArrayStateHelpers.set(this, key, "jobs", text);
      appArrayStateHelpers.set(this, "hasChanges", "jobs", true);
    }


  }

  switchClick = () => {

    // TODO: Confirm backend actually implements

    let enabled = appArrayStateHelpers.get(this, "enabled", "jobs");

    if (enabled) {
      appArrayStateHelpers.set(this, "enabled", "jobs", false);
    } else {
      appArrayStateHelpers.set(this, "enabled", "jobs", true);
    }

    // Mark that job has changes
    appArrayStateHelpers.set(this, "hasChanges", "jobs", true);


  }



  messageCollapseClick = () => {
    if (this.state.collapsed) {
      this.setState({
        collapsed: false,
        collapseArrow: upArrow
      })
    } else {
      this.setState({
        collapsed: true,
        collapseArrow: downArrow
      })
    }

  }

  editorFocus = () => {
    this.setState({
      collapsed: false,
      collapseArrow: upArrow
    })
  }

  actionClick = (url) => {
    if (url) {
      window.open(addProtocol(url), "_blank");
    } else {
      // If the candidate is set then just apply the candidate and don't show the form
      let candidate = this.props.getCandidate();
      let metadata = this.props.getAppState("_metadata");
      if (candidate || metadata.candidate_id > 0) {
        let job = this.props.getCurrentJob();
        let requestObject;

        // If the candidate_id is set, then we don't need to pass the candidate object
        if (metadata.candidate_id > 0) {
          requestObject = {
            session_id: metadata.session_id,
            req_id: job.id
          };
        } else {
          requestObject = {
            session_id: metadata.session_id,
            req_id: job.id,
            candidate: candidate
          };
        }

        api.applyCandidate(requestObject)
        .done(() => {
          this.props.setJobApplied(job.id,true);
          alerts.show({
            status: "success",
            message: `You successfully applied for the ${job.title} role at ${metadata.company}!`
          })
        })
        .fail(() => {
          alerts.show({
            status: "error",
            message: "Whoopsie! There was an error submitting your application. Please refresh the page, and try again. Sorry!"
          })
        });
      } else {
        this.props.setActiveModal("application");
      }
    }
  }

  deleteJobClick = () => {
    this.props.setActiveDialog({
      description: "Are you sure you want to delete this job?",
      confirmText: "Yes",
      cancelText: "No",
      confirm: this.deleteJob
    });
  }

  deleteJob = () => {

    // Debug mode just change current state
    if (this.props.isDebugMode()) {
      this.deleteJobFromCurrentState();
      return;
    }

    // Make copy of array with slice() before modifying and saving
    let jobs = (this.props.getAppState(this.props.modelId, "jobs") || []).slice();

    // Make API call to delete job
    let job = jobs[this.props.index];
    let id = job.pk;

    api.deleteJob(id)
    .done((data) => {

      // Job is deleted from DB, now update our current state
      this.deleteJobFromCurrentState();

    })
    .fail((e) => {
      console.error("Error deleting job:" , e);
      alerts.show({
        status: "error",
        message: "Uh oh! There was an error when you tried to delete your job. Please try again!"
      })
    })

  }

  deleteJobFromCurrentState = () => {
    let jobs = (this.props.getAppState(this.props.modelId, "jobs") || []).slice();
    jobs.splice(this.props.index, 1);
    this.props.setAppState(this.props.modelId, "jobs" , jobs);

    // New active job should be the same index, but check if it's a valid element
    // If it was the last element, the second to last should be defined.  If not, it was the the only element
    // so just close the popup
    let newJob = this.props.getAppState(this.props.modelId, "jobs")[this.props.index];
    if (!newJob) {
      let prevJob = this.props.getAppState(this.props.modelId, "jobs")[this.props.index - 1];
      if (prevJob) {
        this.props.setActiveModal(`jobs:${this.props.index - 1}`)
      } else {
        this.props.setActiveModal("");
      }
    }
  }

  /* Hiring manager functions */

  hiringManagerClick = () => {

    // Don't do anything unless we're in edit mode
    if (this.props.getAppMode() !== "edit") {
      return;
    }

    // Had issues getting modals to animate without loading them upfront, so hiring manager list is actually
    // loaded into dom but we might not necessarily want to call the API, which is why we do it manually here.
    // If there are bugs loading the hiring managers this might be a candidate for refactor.

    this.refs.hiringManagerList.loadHiringManagers();
    this.setState({
      managerListOpen: true
    })
  }

  closeManagerList = () => {
    this.setState({
      managerListOpen: false
    })
  }

  editEnter = () => {
    this.setState({
      editImg: editImgActive
    })
  }

  editLeave = () => {
    this.setState({
      editImg: editImg
    })
  }

  categoryChange = (element) => {
    appArrayStateHelpers.set(this, "category", "jobs", element.type);
    appArrayStateHelpers.set(this, "hasChanges", "jobs", true);
  }

  render() {

    let jobs = this.props.getAppState(this.props.modelId, "jobs");
    let job = jobs[this.props.index] || {};

    // If our id isn't defined, it means our dialog shouldn't be active
    let active = this.props.getActiveModal().split(":")[0] === "jobs";

    // For overlay and body, add a "dialog-dismissed" class if our id is not defined, which means
    // we got an invalid id when trying to open the modal, so it just won't display
    let modalClass = classnames({
      "hp-modal": true,
      "dialog-dismissed": !active,
      "job-page-modal": this.props.jobPage
    })
    let overlayClassNames = classnames({
      "hp-modal-overlay": true,
      "with-controls-padding edit-mode": this.props.getAppMode() === "edit",
      "dialog-dismissed": !active,
      "job-page-modal-overlay": this.props.jobPage
    })

    let modalBodyClass = classnames({
      "modal-body": true,
      "collapsed": this.state.collapsed
    })

    let modalAttr = {};
    if (!active) {
      modalAttr = {"aria-hidden": true};
    }

    let logo = this.props.getAppState("_metadata", "organization_logo_url") || this.props.getAppState("_metadata", "company_logo_url") || this.props.getAppState("header", "logo") || placeholder;

    let actionUrl = job.actionUrl;
    let title = job.title;
    let category = job.category || "Select a Job Function";
    let compensation = job.compensation;
    let description = job.description || "<p>What excited us at Rocket Solutions? Innovation. Small businesses. Etc.</p><h2><strong>RESPONSIBILITIES</strong></h2><ul><li>Qualify and develop inbound and outbound sales leads and respond to product inquiries</li><li>Set appointments: demos for sales representatives to close</li></ul>";
    let company = this.props.getAppState("_metadata", "company");

    let hiringManager = job.hiring_manager || {};

    let hasHiringManager = $.isEmptyObject(hiringManager) && this.props.getAppMode() !== 'edit'
      ? { display: 'none' }
      : { display: 'block' }

    let hiringManagerImgStyle = {};
    if (hiringManager && !hiringManager.profile_photo_url && this.props.getAppMode() !== "edit") {
      hiringManagerImgStyle = {
        display: "none"
      }
    }
    if (hiringManager && !hiringManager.profile_photo_url) {
      hiringManagerImgStyle = {
        border: "1px dotted #979797",
        padding: "3px"
      }
    }

    let buttonColor = this.props.getAppState("header", "buttonColor") || "#29c0d9";

    let logoStyle = {
      border: "2px solid " + buttonColor,
      backgroundImage: "url('" + logo + "')"
    }
    let actionButtonStyle = {
      backgroundColor: buttonColor
    }

    if (logo===placeholder && this.props.getAppMode() !== "edit") {
      logoStyle.display = "none";
    }

    let location = appArrayStateHelpers.get(this, "location", "jobs") || null;


    let hasLocation = { display: 'block' }


    // Needed because we can't predict if location wil be an object or null, and if it's null the .place_id attribute will error out.
    if (this.props.getAppMode() !== 'edit') {
      if (location == null) {
        hasLocation.display = 'none'
      } else if (location.place_id == null) {
        hasLocation.display = 'none'
      }
    }

    let applyButton = <button className="btn apply" style={actionButtonStyle} onClick={() => {
      this.actionClick(actionUrl);
      this.props.reportEvent('interested_clicked',[job.id]);
    }}>I'M INTERESTED</button>

    if (job.applied) {
      actionButtonStyle = {
        backgroundColor: "#d4d4d5"
      };
      applyButton = <button className="btn apply" style={actionButtonStyle} disabled={true}>
        <img src={appliedImg} alt="You Applied" className="you-applied"></img>
        YOU APPLIED
      </button>
    }

    if (this.props.getAppMode() === "edit") {
      applyButton = <Dropdown
        key="jobs"
        modelKey="category"
        defaultTitle={category}
        elements={this.state.jobFunctions}
        onClick={this.categoryChange}
        {...this.props}
      />
    }

    return (
      <ReactModal
        contentLabel="job-modal"
        className={modalClass}
        overlayClassName={overlayClassNames}
        onRequestClose={this.closeModal}
        isOpen={true}
      >
      <div className="modal-contents jobs-modal" {...modalAttr} >
        <div className="info-container dotted-border">
          <img src={xOut}
            alt="Close Modal"
            role="button"
            onClick={this.closeModal}
            className="close-out"
            style={this.props.jobPage ? {display: "none"} : {display: "block"}}
            ></img>
          <div className="modal-header">
            <div className="logo-image-container vertical-align-container" style={logoStyle}></div>
            <div className="header-info-container">
              <div className="company-title">
                <span><b>{company}</b></span>
              </div>
              <div>
                <h1>
                  <SingleLineEditor
                    mode={this.props.getAppMode()}
                    text={title}
                    placeholder="Untitled"
                    modelKey="title"
                    onChange={this.textChange}
                  />
                </h1>
              </div>
              <div className="info-items">
                <span className="info-item-container" style={hasLocation}>
                  <img src={pinImg} alt="pin"></img>
                  <AddressPicker
                    modelKey="location:jobs"
                    autoResize={true}
                    {...this.props}
                    index={this.props.index}
                  />
                </span>
                <span className="info-item-container compensation-container">
                <img src={moneyImg} alt="money" className="money-img"></img>
                  <SingleLineEditor
                    mode={this.props.getAppMode()}
                    text={compensation}
                    placeholder="$ "
                    modelKey="compensation"
                    onChange={this.textChange}
                  />
                </span>
              </div>
              {applyButton}
            </div>
          </div>
          <div className={modalBodyClass} id="job-description">
            <Froala
              text={description}
              mode={this.props.getAppMode()}
              modelKey="description"
              modelId={this.modelId}
              onChange={this.textChange}
              type="modal"
              onFocus={this.editorFocus}
              check={this.props.getAppState("_metadata", "company")}
              jobId={job.id}
              {...this.props}
            />
          </div>
        </div>
        {/* Footer with hiring manager */}
        <div className="hp-modal-footer dotted-border" style={hasHiringManager} onMouseEnter={this.editEnter}
        onMouseLeave={this.editLeave} onClick={this.hiringManagerClick}>
          <img src={this.state.editImg}
            className="add-hiring-manager-button"
            alt="add hiring manager"
            role="button"
            style={this.props.getAppMode() === "edit" ? {display: "block"} : {display: "none"}}
            >
          </img>
          <div className="hp-modal-footer-container">
            <img src={hiringManager.profile_photo_url || placeholder} alt="hiring manager profile" style={hiringManagerImgStyle} className="hiring-manager-image"></img>
            <div className="hp-footer-contents">
              <p style={!hiringManager.email && this.props.getAppMode() === "edit" ? {display: "block"} : {display: "none"}}>Select and/or edit this job's featured team member, who will receive notifications about this job's applicants.</p>
              <h1>
                {hiringManager.first_name} {hiringManager.last_name}
              </h1>
              <p>
                {hiringManager.title}
              </p>
              <a href={hiringManager.linkedin_url}
                target="_blank"
                style={hiringManager.linkedin_url ? {display: "block"} : {display: "none"}}>
                <img src={linkedInImg} className="linked-in-button" alt="linked in button" role="button"></img>
              </a>
            </div>
          </div>
        </div>
        {/* Map widget */}
        <div className="google-map-container" style={hasLocation}>
          <GoogleMap id="jobs-map" location={location} mode={this.props.getAppMode()} {...this.props} />
        </div>
      </div>
      <HiringManagerList open={this.state.managerListOpen} closeFunction={this.closeManagerList} ref="hiringManagerList" {...this.props} />
      </ReactModal>
    );
  }
}

export default JobModal;
