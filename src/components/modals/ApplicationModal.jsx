import React, { Component } from 'react';
import ReactModal from "react-modal";
import placeholder from "image-placeholder.svg";
import classnames from "classnames";
import xOut from "x-out.svg";
import backArrow from "back-arrow.svg";
import Models from "Models";
import alerts from "alerts";
import $ from "jquery";
import api from "api";
import addProtocol from "addProtocol";
import Dropzone from "react-dropzone";

let hideOnNextUpdate = false;

class ApplicationModal extends Component {

  componentWillMount() {
    this.setState({
      fieldErrors: [],
      page: 1,
      resumeUploaded: false,
      files: []
    })
  }

  // Only update component if it's actually visible
  shouldComponentUpdate = () => {
    let active = this.props.getActiveModal() === "application";
    if (active) {
      hideOnNextUpdate = true;
      return true;
    } else if (hideOnNextUpdate) {
      hideOnNextUpdate = false;
      return true;
    } else {
      return false;
    }
  }

  openModal = () => {

  }

  closeModal = () => {
    // If the application wasn't completed then there will be no candidate, so set the form to the beginning
    if (!this.props.getCandidate()) {
      this.setState({
        page: 1
      });
    }

    // If they did complete it, then we won't need this form again because they will be able to apply with the candidate_id

    // If it's a individual job page, go back to the job
    if (this.props.jobPage) {
      this.props.setActiveModal("jobs:0");
    } else {
      // TODO: this might need to go back to the active job modal
      this.props.setActiveModal("");
      this.props.setActiveBlock("goals");
    }
  }

  onSubmit = (e) => {
    // Disable default form functionality
    e.preventDefault();
  }

  inputChange = (e, index, id) => {
    let value = e.target.value;

    let fields = this.props.getAppState(this.props.modelId, "applicationFields").slice() || [];

    if (id === "linkedin_url") {
      value = addProtocol(value);
    }

    fields[index].value = value;

    this.props.setAppState(this.props.modelId, "applicationFields", fields);
  }

  inputBlur = (e, index) => {

    // The main purpose of this is to clear errors when you have them and fix them,
    // but not sure the best way to not just show errors on blur before submitting

    let value = e.target.value;
    let fields = this.props.getAppState(this.props.modelId, "applicationFields").slice() || [];
    let field = fields[index];

    let errors = this.state.fieldErrors;
    if (field.required && !value) {
      errors[index] = true;
      this.setState({
        fieldErrors: errors
      })
    } else {
      errors[index] = false;
      this.setState({
        fieldErrors: errors
      })
    }

  }

  nextPage = (e) => {
    // Show errors on inputs that have them
    let fields = this.props.getAppState(this.props.modelId, "applicationFields").slice() || [];
    let errors = this.state.fieldErrors;
    let hasErrors = false;

    for (let i = 0; i < fields.length; i++) {
      let field = fields[i];
      if (field.required && !field.value && field.page === this.state.page) {
        errors[i] = true;
        hasErrors = true;
      } else {
        errors[i] = null;
      }
    }

    this.setState({
      fieldErrors: errors
    })

    if (hasErrors) {
      alerts.show({
        status: "error",
        message: "Please review the errors in the application form and try again."
      })
    } else {
      this.submitApplication(true);
    }

  }

  previousPage = (e) => {
    let previousPage = this.state.page - 1;
    this.setState({
      page: previousPage
    });
  }

  resumeUpload = (acceptedFiles, rejectedFiles) => {
    if (this.props.getAppMode() === "edit") {
      if (acceptedFiles.length) {

        // Remove error text
        let errors = this.state.fieldErrors;
        $(errors).each(function(index,value){
          errors[index] = false;
        });

        this.setState({
          resumeUploaded: true,
          fieldErrors: errors,
          files: acceptedFiles
        });

        alerts.show({
          status: "success",
          message: "This is just a preview, and the file you uploaded wasn't saved."
        })
      } else {
        alerts.show({
          status: "error",
          message: "This is just a preview, but the file type you tried to upload would have failed."
        })
      }
      return;
    }

    api.uploadResume(this.state.candidateId, acceptedFiles)
    .done(() => {

      // Remove error text
      let errors = this.state.fieldErrors;
      $(errors).each(function(index,value){
        errors[index] = false;
      });

      this.setState({
        resumeUploaded: true,
        fieldErrors: errors,
        files: acceptedFiles
      });

      alerts.show({
        status: "success",
        message: "Awesome — you successfully uploaded your resume!"
      })
    })
    .fail(() => {
      alerts.show({
        status: "error",
        message: "Uh oh! There was an error uploading your resume. Please make certain it’s a PDF or Word document, and try again."
      })
    });
  }

  finishApplicationClick = (e) => {

    // Show errors on inputs that have them
    let fields = this.props.getAppState(this.props.modelId, "applicationFields").slice() || [];
    let errors = this.state.fieldErrors;
    let hasErrors = false;

    for (let i = 0; i < fields.length; i++) {
      let field = fields[i];
      if (field.required && !field.value) {
        // If a resume has been uploaded, then we don't need to require the linkedin url anymore
        if (field.id === "linkedin_url") {
          if (!this.state.resumeUploaded) {
            errors[i] = true;
            hasErrors = true;
          } else {
            errors[i] = null;
          }
        } else {
          errors[i] = true;
          hasErrors = true;
        }
      } else {
        errors[i] = null;
      }
    }

    this.setState({
      fieldErrors: errors
    })

    if (hasErrors) {
      alerts.show({
        status: "error",
        message: "You need to either upload your resume OR enter your linkedIn URL to finish your application!"
      })
    } else {
      this.submitApplication();
    }

  }

  submitApplication = (nextPageClick=false) => {

    let fields = this.props.getAppState(this.props.modelId, "applicationFields").slice();
    let req_id = this.props.getCurrentJob().id;

    let requestObject = {
      req_id: req_id,
      candidate: {}
    };

    // Build request object
    $.each(fields, (index, value) => {
      // Map the fields into a candidate object for the request
      if (value.id === 'full_name' && value.value !== "") {
        // If there is a space, split up first and last name
        let name = value.value.split(' ');
        if (name.length > 1){
          requestObject['candidate']['first_name'] = name[0];
          requestObject['candidate']['last_name'] = name[1];
        } else {
          requestObject['candidate']['first_name'] = name[0];
        }
      }
      else if (value.id === 'email') {
        requestObject['candidate']['emails'] = [{
          address: value.value
        }]
      }
      else if (value.id === 'phone_number' && value.value !== "") {
        requestObject['candidate']['phones'] = [{
          type: "mobile",
          number: value.value
        }]
      } else if (value.value !== "") {
        requestObject['candidate'][value.id] = value.value
      }
    })

    // Set the session id if there is one
    if (window.happie_page_data) {
      requestObject.session_id = window.happie_page_data._metadata.session_id;
    }

    if (this.props.getAppMode() === "edit" || this.props.getAppMode() === "preview") {
      console.log(requestObject);
      let nextPage = this.state.page + 1;
      this.setState({
        page: nextPage
      });
      if (!nextPageClick) {
        alerts.show({
          status: "success",
          message: "This is just a preview, and the application you submitted won't be added to your candidates. Form entries on the live form, however, will be added to your candidates."
        })
      }
      return;
    }

    api.applyCandidate(requestObject)
    .done((response) => {

      let candidateId = response.candidate_id;
      let nextPage = this.state.page + 1;
      this.setState({
        page: nextPage,
        candidateId: candidateId
      });

      // Set the candidate id in the metadata for reporting
      // this.props.setAppState('_metadata','candidate_id',candidateId);
      window.happie_page_data._metadata.candidate_id = candidateId;

      if (!nextPageClick) {
        // Set the candidate so they can apply with that from now on if they actually submit the application
        this.props.setJobApplied(req_id,true);
        this.props.setCandidate(requestObject.candidate);
      }

    })
    .fail((e) => {
      console.error('error trying to apply candidate ', e);
    })

  }


  render() {

    let active = this.props.getActiveModal() === "application";

    let modalClass = classnames({
      "hp-modal": true,
      "dialog-dismissed": !active,
      "application-modal-container": true,
      "job-page-modal": this.props.jobPage
    })

    let overlayClassNames = classnames({
      "hp-modal-overlay": true,
      "with-controls-padding": this.props.getAppMode() === "edit",
      "dialog-dismissed": !active,
      "application-modal-overlay": true,
      "job-page-modal-overlay": this.props.jobPage
    })

    let modalAttr = {};
    if (!active) {
      modalAttr = {"aria-hidden": true};
    }

    let buttonColor = this.props.getAppState(Models.header.id, "buttonColor") || "#29c0d9";
    let buttonTextColor = this.props.getAppState(Models.header.id, "buttonTextColor") || "#FFFFFF";

    let buttonStyle = {
      color: buttonTextColor,
      backgroundColor: buttonColor,
    }

    let logo = this.props.getAppState("_metadata", "organization_logo_url") || this.props.getAppState("_metadata", "company_logo_url") || this.props.getAppState("header", "logo") || placeholder;

    let logoStyle = {
      border: "2px solid " + buttonColor,
      backgroundImage: "url('" + logo + "')"
    }

    // Don't show placeholder on preview
    if (logo===placeholder && this.props.getAppMode() !== "edit") {
      logoStyle.display = "none";
    }

    let fields = this.props.getAppState("application", "applicationFields");

    fields = fields.map((value, index) => {

      let labelStyle = {
        display: value.enabled ? "block" : "none"
      }
      let inputStyle = {
        border: this.state.fieldErrors[index] && value.required ? "1px solid #d0021b" : "1px solid #e0e7ee"
      }
      let errorStyle = {
        display: this.state.fieldErrors[index] && value.required ? "block" : "none"
      }

      if (value.hidden || value.page !== this.state.page) {
        return false;
      }

      return <label key={value.id} style={labelStyle}>
        <span>{value.title}</span>
        <input className="custom-input"
          style={inputStyle}
          placeholder={value.placeholder}
          onChange={(e) => {this.inputChange(e, index, value.id)}}
          onBlur={(e) => {this.inputBlur(e, index)}}
          value={value.value}></input>
        <span className="error-field" style={errorStyle}>This field is required. Please fill it in!</span>
      </label>
    });
    if (this.state.page === 4) {
      fields = <div>
        <h1>And that’s it — you’re done!</h1>
        <h2>We received your application. Thanks for applying!</h2>
      </div>
    }

    let button = <button className="btn btn-default" style={buttonStyle} onClick={this.finishApplicationClick} type="button">FINISH APPLICATION</button>
    if (this.state.page < 3) {
      button = <button className="btn btn-default" style={buttonStyle} onClick={this.nextPage} type="button">NEXT</button>
    }
    if (this.state.page === 4) {
      button = false;
    }

    let backButton = false;
    if (this.state.page > 1 && this.state.page < 4) {
      backButton = <img src={backArrow} alt="Go Back" onClick={this.previousPage} className="previous-page"></img>
    }

    // TODO: react dropzone needs to be manually designed for uploading, success and failure situations
    // https://github.com/okonet/react-dropzone

    let dropzone = false;
    if (this.state.page === 3) {
      dropzone = <div>
        <span className="upload-label">Upload your resume</span>
        <Dropzone className="dropzone" onDrop={this.resumeUpload} multiple={false} accept={".doc, .docx, .pdf"}>
          <span className="drag-text">Drag a file here</span>
          <span className="or-text">or</span>
          <span className="select-file">Select file</span>
          {this.state.files.length > 0 ? <div>
          <span className="drag-text">Successfully Uploaded:</span>
          {this.state.files.map((file, index) => <span key={index} className="drag-text">{file.name}</span> )}
          </div> : null}
        </Dropzone>
        <span className="file-type-text">PDF or Word file only please</span>
      </div>
    }

    return (
      <ReactModal
        contentLabel="application-modal"
        className={modalClass}
        overlayClassName={overlayClassNames}
        onRequestClose={this.closeModal}
        isOpen={true}
      >
      <div className="modal-contents application-modal" {...modalAttr} >
        <div className="close-out-container">
          {backButton}
          <img src={xOut} alt="Close Modal" onClick={this.closeModal} className="close-out"></img>
        </div>
        <div className="logo-image-container vertical-align-container" style={logoStyle}></div>
        <form onSubmit={this.onSubmit}>
          <div>
            {dropzone}
            {fields}
          </div>
          {button}
        </form>

      </div>
      </ReactModal>
    );
  }
}

export default ApplicationModal;
