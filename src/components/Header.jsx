import React, { Component } from 'react';

import editImg from 'header/edit.svg';
import linkImg from 'header/link.svg';
import homeImg from "header/white-house.svg";
import undoDisabledImg from 'header/undo.svg';
import redoDisabledImg from 'header/redo.svg';
import undoActiveImg from 'header/undo-active.svg';
import redoActiveImg from 'header/redo-active.svg';
import saveImg from 'header/save.svg';
import previewImg from 'header/preview.svg';
import cloudImg from 'header/cloud.svg';
import alerts from 'alerts';
import api from 'api';
import $ from 'jquery';
import SingleLineEditor from "blocks/elements/SingleLineEditor"

class Header extends Component {

  componentWillMount = () => {
    this.setState({
      shouldSaveRouteOnBlur: false
    })
  }

  saveClick = (e, hideSuccessMessage) => {

    this.saveJobs()
    .done(() => {
      this.savePage(hideSuccessMessage)
    })
    .fail(() => {
      console.error("Error saving jobs");
    })


  }

  saveJobs = (hideSuccessMessage) => {
    // Loop over jobs, see which ones need to be changed, save them, then mark them as not having unsaved changes
    let jobs = this.props.getAppState("jobs", "jobs");
    let jobsPromise = $.Deferred();
    let promiseArray = [];

    $.each(jobs, (key, value) => {
      if (value.hasChanges) {

        // Convert location object into strings
        let formattedValue = $.extend(true, {}, value);

        if (formattedValue.location) {
          // Database is expecting lat/lng to only have 6 decimals, but google maps returns 7
          // TODO: Ensure there aren't any bugs around rounding/converting to/from strings
          formattedValue.latitude = parseFloat(formattedValue.location.lat.toFixed(6));
          formattedValue.longitude = parseFloat(formattedValue.location.lng.toFixed(6));
          formattedValue.place_id = formattedValue.location.place_id;
        }

        delete formattedValue.location;

        // Also delete hiring manager if it's null
        if (!formattedValue.hiring_manager) {
          delete formattedValue.hiring_manager
        }

        // Convert compensation to key expected (lander_comp_ote)
        formattedValue.lander_comp_ote = formattedValue.compensation;
        delete formattedValue.compensation;

        // TODO: See if we need to do this here as well, or if input blur is enough
        /*
        formattedValue.hiring_manager = {
          email: formattedValue.hiringManagerEmail || ""
        }
        delete formattedValue.hiringManagerEmail;
        */

        let promise = api.saveJob(formattedValue);
        promiseArray.push(promise);
      }
    })

    $.when.apply($, promiseArray)
    .done(() => {
      // Set jobs to not having changes
      $.each(jobs, (key, value) => {
        value.hasChanges = false;
      })
      this.props.setAppState("jobs", "jobs", jobs);

      console.log("Jobs saved successfully: " , jobs);
      /*
      TODO: Show confirmation when jobs saved?
      alerts.show({
        message: "Your jobs were saved successfully.",
        status: "success"
      })
      */
    })
    .fail((e) => {
      console.error("failed to save jobs " , e);
      alerts.show({
        message: "Oops!  There was an error trying to save one of your jobs. Please make sure all the information is filled in, including the hiring manager's email, and try again.",
        status: "error"
      })
    })
    .always(() => {
      jobsPromise.resolve();
    })

    return jobsPromise;

  }

  savePage = (hideSuccessMessage) => {
    api.savePage()
    .done(() => {

      if (!hideSuccessMessage) {
        alerts.show({
          message: "Nice job - you successfully saved your page!",
          status: "success"
        })
      }

      // We debounce our input saving at 500ms (right now), so those could potentially fire after we save
      // since "onChange" is triggered on blur, so set up this timeout to set unsaved changes to false
      setTimeout(() => {
        this.props.setUnsavedChanges(false);
      }, 501)

    })
    .fail(() => {
      alerts.show({
        message: "Whoopsie! There was an error when you tried to save your page. Please try saving your page again - you don't want to lose your work!",
        status: "error"
      })
    })
  }

  publishClick = () => {

    this.props.setActiveDialog({
      description: "Are you sure you want to publish your page?  We'll also save any unsaved edits before publishing it.",
      confirmText: "Yes, publish it!",
      cancelText: "Not yet, thanks",
      confirm: this.publishPage
    });
  }

  publishPage = () => {

    // Call function to save page, but don't show normal banner saying we saved (will still show error message)
    // TODO: Could refactor into separate function to separate from actual event listener
    this.saveClick(null, true);

    // Should be able to save and publish at the same time, so asynchronously start publishing page
    // If we find saves taking longer than publishes we might need to make it synchronous
    api.publishPage()
    .done((data) => {

      alerts.show({
        message: `Woohoo! You just published your page, and it's visible to the world. Now get out there and share it!`,
        status: "success"
      })
      this.props.setUnsavedChanges(false);
    })
    .fail((data) => {
      alerts.show({
        message: "Whoopsie! There was an error when you tried to publish your page. Please try again shortly.",
        status: "error"
      })
    })
  }

  savePublishedRoute = () => {

    // Should be able to save and publish at the same time, so asynchronously start publishing page
    // If we find saves taking longer than publishes we might need to make it synchronous
    api.savePublishedRoute()
    .done((data) => {

      let pagesUrl = "https://talent.gethappie.me/pages/";
      if (localStorage.getItem('url_base')) {
        pagesUrl = localStorage.getItem('url_base') + 'pages/';
      }

      let url = pagesUrl + this.props.getAppState("_metadata", "page_route");

      alerts.show({
        message: `You can now view your page at <a target='_blank' href='${url}'>${url}</a>. Remember to publish your changes if you want to see them live!`,
        status: "success"
      })

      this.setState({
        shouldSaveRouteOnBlur: false
      })
    })
    .fail((data) => {

      // Multiple checks just in case server returns something unexpected
      if (data && data.page_route && data.page_route[0] === "The URL already exist.") {
        alerts.show({
          message: "Whoopsie! It looks like that page route is already in use. Please try a different one.",
          status: "error"
        })
      } else {
        alerts.show({
          message: "Whoopsie! There was an error when you tried to change your page route. Remember to only use letters and numbers and to pick a domain unique to your company.",
          status: "error"
        })
      }

    })
  }

  previewClick = () => {
    this.props.setAppMode("preview");
    // Trigger a resize event to get blocks to adjust to new dimensions (after animation)
    setTimeout(() => {
      $(window).trigger('resize');
    }, 1000)

  }

  homeClick = () => {
    this.props.router.push({
      pathname: `/`,
      query: {
        debug: this.props.router.location.query.debug
      }
    })
  }

  editPageRouteClick = () => {
    $(".page-subdomain-url").find("div").get(0).focus();
  }

  pageRouteBlur = () => {
    // Wrap in timeout to accomodate debouncing

    setTimeout(() => {
      if (this.state.shouldSaveRouteOnBlur) {
        this.savePublishedRoute();
      }
    }, 501)
  }

  textChange = (key, text) => {
    // Remove special chars
    // Note that this only happens after input is un-focused
    let newText = text.replace(/[^a-zA-Z0-9-_]/g, '');

    // Note that usually we use modelKey, but the header changes _metadata items like page_route
    this.props.setAppState("_metadata", key, newText)

    // Flag if we should save on blur when there are changes
    if (key === "page_route") {
      this.setState({
        shouldSaveRouteOnBlur: true
      })
    } else {
      this.setState({
        shouldSaveRouteOnBlur: false
      })
    }
  }

  undoClick = () => {
    this.props.undo();
  }

  redoClick = () => {
    this.props.redo();
  }

  render() {

    let page_route = this.props.getAppState("_metadata", "page_route");

    let mode = this.props.getAppMode();

    // Animation when sliding into preview
    let headerStyle = {};
    let headerAttr = {};
    if (mode !== "edit") {
      headerStyle = {
        top: "-63px"
      }
      headerAttr = {
        "aria-hidden": true
      }
    }

    let appStateHistory = this.props.getAppStateHistory();

    let undoButtonAttr = {
      src: undoActiveImg,
      onClick: this.undoClick
    }
    if (appStateHistory.index < 1) {
      undoButtonAttr = {
        src: undoDisabledImg,
        disabled: "true"
      }
    }

    let redoButtonAttr = {
      src: redoActiveImg,
      onClick: this.redoClick
    }

    if (appStateHistory.index === appStateHistory.stateHistory.length - 1) {
      redoButtonAttr = {
        src: redoDisabledImg,
        disabled: "true"
      }
    }

    let pagesUrl = "https://talent.gethappie.me/pages/";
    if (localStorage.getItem('url_base')) {
      pagesUrl = localStorage.getItem('url_base') + 'pages/';
    }

    let publishedUrl = pagesUrl + page_route;

    return (
      <nav className="header navbar navbar-fixed-top" style={headerStyle} {...headerAttr}>
        <div className="container">
          <img className="page-selection-image" alt="home" role="button" src={homeImg} onClick={this.homeClick}></img>
          <a href={publishedUrl} target="_blank">
            <img className="link-image" src={linkImg} alt="link" role="button"></img>
          </a>
          <span className="page-base-url">{pagesUrl}</span>
          <span className="page-subdomain-url">
          <SingleLineEditor
            ref="editor"
            modelKey="page_route"
            text={page_route}
            onChange={this.textChange}
            onBlur={this.pageRouteBlur}
            placeholder="YourCustomURL"
            mode={mode}
            isUrl={true} />
          </span>
          <img className="edit-image" src={editImg} alt="edit" role="button" onClick={this.editPageRouteClick}></img>

          <div className="header-controls">
            <img {...undoButtonAttr} alt="undo" role="button" tabIndex="0" title="Undo" style={{display: "none"}}></img>
            <img {...redoButtonAttr} alt="redo" role="button" tabIndex="0" title="Redo" style={{display: "none"}}></img>
            <img src={saveImg} alt="save" role="button" tabIndex="0" title="Save" onClick={(e) => {this.saveClick(e)}}></img>
            <img className="preview-button" src={previewImg} alt="preview" role="button" tabIndex="0" title="Preview" onClick={this.previewClick}></img>
            <img src={cloudImg} alt="publish" role="button" tabIndex="0" title="Publish" onClick={this.publishClick}></img>
          </div>
        </div>
      </nav>
    );
  }
}

export default Header;
