import React, { Component } from 'react';
import Models from 'Models';
import classnames from 'classnames';
import smoothScroll from 'smoothScroll';
import JumbotronBackground from './elements/JumbotronBackground';
import $ from "jquery";
import deepEqual from "deep-equal"

import createRichButtonsPlugin from "draft-js-richbuttons-plugin";

import Froala from './elements/Froala';


class MissionBlock extends Component {

  componentWillMount() {
    this.modelId = Models.mission.id;

    // Need to instantiate new plugin for each instance of editor
    let richButtonsPlugin = createRichButtonsPlugin();
    this.setState({
      plugin: richButtonsPlugin,
      blockState: null,
      headerState: null
    })
  }

  shouldComponentUpdate() {

    // Only re-render if the model(s) we care about has updated
    let storeState = this.props.getAppState(this.modelId);
    let storeHeaderState = this.props.getAppState("header");

    if (!deepEqual(this.state.blockState, storeState) || !deepEqual(this.state.headerState, storeHeaderState)) {
      let newState = $.extend(true, {}, storeState)
      let newHeaderState = $.extend(true, {}, storeHeaderState)
      this.setState({
        blockState: newState,
        headerState: newHeaderState
      })
      return true;
    } else {
      return false;
    }
  }

  blockClick = () => {
    this.props.setActiveBlock(this.modelId);
  }

  textChange = (key, text) => {
    this.props.setAppState(this.modelId, key, text)
  }

  render() {

    var containerClass = classnames({
      'page-block mission-block jumbotron': true,
      'active': this.props.getActiveBlock() === this.modelId
    });

    let buttonColor = this.props.getAppState(Models.header.id, "buttonColor") || "#29c0d9";
    let buttonTextColor = this.props.getAppState(Models.header.id, "buttonTextColor") || "#FFFFFF";

    let buttonStyle = {
      color: buttonTextColor,
      backgroundColor: buttonColor,
    }

    let textColor = this.props.getAppState(this.modelId, "textColor") || "#6D6D6D";
    let textStyle = {
      color: textColor
    }

    // Editable text
    let description = this.props.getAppState(this.modelId, "description") || "<h1><b>This is where you talk about your team's mission.</b></h1><p>Tell candidates about what your team does, within the bigger context of your company's culture. Be inspiring!</p>";


    return (
      <section className={containerClass} onClick={this.blockClick}>
        <div className="block-content-container jumbotron-content vertical-align-container">
          <div className="vertical-aligner">
            <div className="jumbotron-message">
              <div>
                <Froala 
                  text={description} 
                  mode={this.props.getAppMode()}
                  modelKey="description"
                  modelId={this.modelId}
                  onChange={this.textChange}
                  style={textStyle}
                  type="jumbotron"
                  check={this.props.getAppState("_metadata", "company")}
                  {...this.props}
                />
              </div>
              <button
                className="btn btn-default"
                style={buttonStyle}
                onClick={() => {
                  smoothScroll(".goals-block", this.props.getAppMode());
                  this.props.reportEvent('learn_more_2_clicked');
                }}
              >LEARN MORE</button>
            </div>
          </div>
        </div>
        <JumbotronBackground modelId={this.modelId} {...this.props} selector=".mission-block"/>
      </section>
    );
  }
}

export default MissionBlock;
