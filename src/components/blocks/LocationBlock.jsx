import React, { Component } from 'react';
import Models from 'Models';
import classnames from 'classnames';
import placeholder from "location/house.svg";
import JumbotronBackground from "./elements/JumbotronBackground"
import $ from "jquery";
import deepEqual from 'deep-equal';
import mapsHelper from "mapsHelper";


class LocationBlock extends Component {

  componentWillMount() {
    this.modelId = Models.location.id;

    this.setState({
      location: null,
      gmaps: {
        address_components: []
      }
    })
  }

  blockClick = () => {
    this.props.setActiveBlock(this.modelId);
  }

  componentWillReceiveProps = (nextProps) => {

    let appStateLocation = this.props.getAppState("location", "location");

    if (!deepEqual(appStateLocation, this.state.location)) {

      // Only perform reverse geocode if maps are loaded
      mapsHelper.getLoadPromise()
      .done(() => {

        // Call gmaps api to get "gmaps" object that contains info like city, zip code, etc.
        mapsHelper.geocoder.geocode({
          "placeId": appStateLocation.place_id
        }, (results, status) => {

          this.setState({
            gmaps: results[0]
          })
        })


      })
      .fail((e) => {
        console.error("Error getting data for google maps " , e)
      })

      this.setState({
        location: appStateLocation
      })
    }
  }

  getLocationStrings = (location) => {

    if (!location || !this.state.gmaps) {
      return "";
    }

    let streetNumber = "";
    let street = "";
    let city = "";
    let state = "";
    let zipCode = "";

    // Address components that come back aren't the same for every address, so need to loop
    // through them to find the data types we're looking for
    $(this.state.gmaps.address_components).each((index, value) => {
      if (value.types[0] === "locality") {
        city = value.long_name;
      } else if (value.types[0] === "street_number") {
        streetNumber = value.long_name;
      } else if (value.types[0] === "route") {
        street = value.short_name;
      } else if (value.types[0] === "postal_code") {
        zipCode = value.short_name
      } else if (value.types[0] === "administrative_area_level_1") {
        state = value.short_name;
      }
    })

    return {
      streetNumber: streetNumber,
      street: street,
      city: city,
      state: state,
      zipCode: zipCode
    };
  }


  render() {

    var containerClass = classnames({
      'page-block location-block jumbotron': true,
      'active': this.props.getActiveBlock() === this.modelId
    });

    let textColor = this.props.getAppState(this.modelId, "textColor") || "#6D6D6D";
    let textStyle = {
      color: textColor
    }

    let headerImage = this.props.getAppState(this.modelId, "image");
    let headerImageStyle;

    // If we haven't defined an image and we're in edit mode, show default image with dotted border
    if (!headerImage && this.props.getAppMode() === "edit") {
      headerImage = placeholder;
      headerImageStyle = {
        border: "2px dotted #979797"
      }
    } else if (!headerImage && this.props.getAppMode() !== "edit") {
      headerImageStyle = {
        display: "none"
      }
    }

    // TODO: Format lines
    let location = this.getLocationStrings(this.props.getAppState("location", "location"));

    if (!location) {
      location = {
        street: "Street Address",
        city: "City",
        state: "State",
        zipCode: "ZIP"
      }
    }

    let company = this.props.getAppState("_metadata", "company") || "Our Location";

    return (
      <section className={containerClass} onClick={this.blockClick} >
        <div className="block-content-container jumbotron-content vertical-align-container">
          <div className="vertical-aligner">
            <div className="jumbotron-header-image-container">
              <img className="jumbotron-header-image" src={headerImage} style={headerImageStyle} alt="Location"></img>
            </div>
            <div className="jumbotron-message">
              <div>
                <h1 style={textStyle}><b>{company}</b></h1>
                <p style={textStyle}>{location.streetNumber} {location.street}</p>
                <p style={textStyle}>{location.city}, {location.state} {location.zipCode}</p>
              </div>
            </div>
          </div>
        </div>
        <JumbotronBackground modelId={this.modelId} {...this.props} selector=".location-block"/>
      </section>
    );
  }
}

export default LocationBlock;
