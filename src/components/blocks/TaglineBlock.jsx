import React, { Component } from 'react';
import Models from 'Models';
import classnames from 'classnames';
import smoothScroll from 'smoothScroll';
import JumbotronBackground from './elements/JumbotronBackground';

import createRichButtonsPlugin from "draft-js-richbuttons-plugin";
import $ from "jquery";
import deepEqual from "deep-equal"

import Froala from './elements/Froala';


class TaglineBlock extends Component {

  componentWillMount() {
    this.modelId = Models.tagline.id;

    // Need to instantiate new plugin for each instance of editor
    let richButtonsPlugin = createRichButtonsPlugin();
    this.setState({
      plugin: richButtonsPlugin,
      blockState: null,
      headerState: null
    })
  }

  shouldComponentUpdate() {

    // Only re-render if the model(s) we care about has updated
    let storeState = this.props.getAppState(this.modelId);
    let storeHeaderState = this.props.getAppState("header");

    if (!deepEqual(this.state.blockState, storeState) || !deepEqual(this.state.headerState, storeHeaderState)) {
      let newState = $.extend(true, {}, storeState)
      let newHeaderState = $.extend(true, {}, storeHeaderState)
      this.setState({
        blockState: newState,
        headerState: newHeaderState
      })
      return true;
    } else {
      return false;
    }
  }

  blockClick = () => {
    this.props.setActiveBlock(this.modelId);
  }

  textChange = (key, text) => {
    this.props.setAppState(this.modelId, key, text)
  }


  render() {

    var containerClass = classnames({
      'page-block tagline-block jumbotron': true,
      'active': this.props.getActiveBlock() === this.modelId
    });

    let textColor = this.props.getAppState(this.modelId, "textColor") || "#6D6D6D";
    let textStyle = {
      color: textColor
    }

    let buttonColor = this.props.getAppState(Models.header.id, "buttonColor") || "#29c0d9";
    let buttonTextColor = this.props.getAppState(Models.header.id, "buttonTextColor") || "#FFFFFF";

    let buttonStyle = {
      color: buttonTextColor,
      backgroundColor: buttonColor,
    }

    // Editable text
    let description = this.props.getAppState(this.modelId, "description") || "<h1><b>This is where your opportunity tagline goes. Make it count.</b></h1><p>Briefly describe what sets your opportunity apart from candidates - keep it short and sweet!</p>";


    return (
      <section className={containerClass} onClick={this.blockClick}>
        <div className="block-content-container jumbotron-content vertical-align-container">
          <div className="vertical-aligner">
            <div className="jumbotron-message">
              <div>
                <Froala 
                  text={description} 
                  mode={this.props.getAppMode()}
                  modelKey="description"
                  modelId={this.modelId}
                  onChange={this.textChange}
                  style={textStyle}
                  type="jumbotron"
                  check={this.props.getAppState("_metadata", "company")}
                  {...this.props}
                />
              </div>
              <button className="btn btn-default"
                style={buttonStyle}
                onClick={() => {
                  smoothScroll(".mission-block", this.props.getAppMode());
                  this.props.reportEvent('learn_more_1_clicked');
                }}
              >LEARN MORE</button>
            </div>
          </div>
        </div>
        <JumbotronBackground modelId={this.modelId} {...this.props} selector=".tagline-block"/>
      </section>
    );
  }
}

export default TaglineBlock;
