import React, { Component } from 'react';
import Models from 'Models';
import classnames from 'classnames';
import GoogleMap from "./elements/GoogleMap";

import $ from "jquery";
import deepEqual from "deep-equal"

class MapBlock extends Component {

  componentWillMount() {
    // Note that this ID is the same as the block above, since both should act as the same unit
    this.modelId = Models.location.id;

    this.setState({
      blockState: null,
      headerState: null
    })
  }

  shouldComponentUpdate() {

    // Only re-render if the model(s) we care about has updated
    let storeState = this.props.getAppState(this.modelId);
    let storeHeaderState = this.props.getAppState("header");

    if (!deepEqual(this.state.blockState, storeState) || !deepEqual(this.state.headerState, storeHeaderState)) {
      let newState = $.extend(true, {}, storeState)
      let newHeaderState = $.extend(true, {}, storeHeaderState)
      this.setState({
        blockState: newState,
        headerState: newHeaderState
      })
      return true;
    } else {
      return false;
    }
  }

  componentDidMount() {

  }

  blockClick = () => {
    this.props.setActiveBlock(this.modelId);
  }

  render() {

    var containerClass = classnames({
      'page-block map-block': true,
      'active': this.props.getActiveBlock() === this.modelId
    });

    let location = this.props.getAppState("location", "location") || null;

    return (
      <section className={containerClass} onClick={this.blockClick}>
        <GoogleMap id="headquarters-map" location={location} mode={this.props.getAppMode()} {...this.props} />
      </section>
    );
  }
}

export default MapBlock;
