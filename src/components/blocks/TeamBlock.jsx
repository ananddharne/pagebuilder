import React, { Component } from 'react';
import Models from 'Models';
import classnames from 'classnames';
import JumbotronBackground from './elements/JumbotronBackground';
import TeamMember from './elements/TeamMember';
import addTeamMemberImg from "team/add-team-member.svg";
import addTeamMemberImgActive from "team/add-team-member-active.svg";
import placeholder from "team/image-placeholder-speech-bubble.svg";

import createRichButtonsPlugin from "draft-js-richbuttons-plugin";

import Froala from './elements/Froala';


class TeamBlock extends Component {

  componentWillMount() {
    this.modelId = Models.team.id;

    // Need to instantiate new plugin for each instance of editor
    let richButtonsPlugin = createRichButtonsPlugin();
    this.setState({
      addTeamMemberImg: addTeamMemberImg,
      plugin: richButtonsPlugin,
      blockState: null,
      headerState: null,
      currentMode: this.props.getAppMode()
    })

  }

  /*
  Omitted because this relies too much on own state (hovers) and app state
  shouldComponentUpdate(nextProps, nextState) {

  }
  */

  blockClick = () => {
    this.props.setActiveBlock(this.modelId);
  }

  textChange = (key, text) => {
    this.props.setAppState(this.modelId, key, text);
  }

  addTeamMember = (e) => {

    // Make copy of array with slice() before modifying and saving
    let teamMembers = (this.props.getAppState(this.modelId, "teamMembers") || []).slice();
    teamMembers.push({
      name: "",
      title: "",
      image: "",
      imageName: "",
      linkedin_url: ""
    })

    this.props.setAppState(this.modelId, "teamMembers" , teamMembers);

    // Set active block to new index
    this.props.setActiveBlock("team:" + (teamMembers.length - 1));

    // Click will trigger normal block click, so don't bubble up
    e.stopPropagation();
  }

  addTeamMemberEnter = () => {
    this.setState({
      addTeamMemberImg: addTeamMemberImgActive
    })
  }

  addTeamMemberLeave = () => {
    this.setState({
      addTeamMemberImg: addTeamMemberImg
    })
  }


  render() {

    let containerClass = classnames({
      'page-block team-block jumbotron': true,
      'active': this.props.getActiveBlock() === this.modelId
    });

    let textColor = this.props.getAppState(this.modelId, "textColor") || "#6D6D6D";
    let textStyle = {
      color: textColor
    }

    // Editable text
    let description = this.props.getAppState(this.modelId, "description") || "<h1><b>Interested? Let's talk.</b></h1><p>Get in touch with us. We'd love to hear from you.</p>";

    let teamMembers = this.props.getAppState(this.modelId, "teamMembers");

    teamMembers = teamMembers.map((value, index) => {
      // This should make unique-enough key
      let key = value.name + index;
      return <TeamMember key={key} index={index} modelId={this.modelId} {...this.props} />
    })

    // Hide team member add button if not in edit mode
    let addTeamMemberContainerStyle = {
      display: this.props.getAppMode() === "edit" ? "inline-block" : "none"
    }
    // Holy es6 syntax, batman!  Make sure babel handles this correctly in older browsers
    let addTeamMemberButtonStyle = {
      background: `url('${this.state.addTeamMemberImg}')`
    }

    let teamImage = this.props.getAppState(this.modelId, "image");

    let teamImageStyle;

    // If we haven't defined an image and we're in edit mode, show default image with dotted border
    if (!teamImage && this.props.getAppMode() === "edit") {
      teamImage = placeholder;
      teamImageStyle = {
        border: "2px dotted #979797"
      }
    } else if (!teamImage && this.props.getAppMode() !== "edit") {
      teamImageStyle = {
        display: "none"
      }
    }

    return (
      <section className={containerClass} onClick={this.blockClick}>
        <div className="block-content-container jumbotron-content vertical-align-container">
          <div className="vertical-aligner">
            <div className="jumbotron-header-image-container">
              <img className="jumbotron-header-image" src={teamImage} style={teamImageStyle} alt="team member profile"></img>
            </div>
            <div className="jumbotron-message">
              <div>
                <Froala
                  text={description}
                  mode={this.props.getAppMode()}
                  modelKey="description"
                  modelId={this.modelId}
                  onChange={this.textChange}
                  style={textStyle}
                  type="jumbotron"
                  check={this.props.getAppState("_metadata", "company")}
                  {...this.props}
                />
              </div>
            </div>

            <div className="team-member-container">
              {teamMembers}
              <div className="add-team-member-container" style={addTeamMemberContainerStyle}>
                <div className="vertical-align-container">
                  <div className="vertical-aligner">
                    <button aria-label="Add Team Member" style={addTeamMemberButtonStyle} onClick={this.addTeamMember} onMouseEnter={this.addTeamMemberEnter} onMouseLeave={this.addTeamMemberLeave}></button>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <JumbotronBackground modelId={this.modelId} {...this.props} selector=".team-block"/>
      </section>
    );
  }
}

export default TeamBlock;
