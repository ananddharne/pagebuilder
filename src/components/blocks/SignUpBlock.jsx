import React, { Component } from 'react';
import Models from 'Models';
import classnames from 'classnames';
import JumbotronBackground from './elements/JumbotronBackground';
import api from "api";

import $ from "jquery";
import deepEqual from "deep-equal"

import createRichButtonsPlugin from "draft-js-richbuttons-plugin";

import Froala from './elements/Froala';

class SignUpBlock extends Component {

  componentWillMount() {
    this.modelId = Models.signUp.id;

    // Need to instantiate new plugin for each instance of editor
    let richButtonsPlugin = createRichButtonsPlugin();
    this.setState({
      plugin: richButtonsPlugin,
      loaded: false,
      blockState: null,
      headerState: null
    })
  }

  shouldComponentUpdate() {

    // Only re-render if the model(s) we care about has updated
    let storeState = this.props.getAppState(this.modelId);
    let storeHeaderState = this.props.getAppState("header");

    if (!deepEqual(this.state.blockState, storeState) || !deepEqual(this.state.headerState, storeHeaderState)) {
      let newState = $.extend(true, {}, storeState)
      let newHeaderState = $.extend(true, {}, storeHeaderState)
      this.setState({
        blockState: newState,
        headerState: newHeaderState
      })
      return true;
    } else {
      return false;
    }
  }

  blockClick = () => {
    this.props.setActiveBlock(this.modelId);
    if (!this.state.loaded) {
      this.loadData();
    }

  }

  loadData = () => {
    if (!(window.happie_page_data && window.happie_page_data.page_blocks)){
      api.loadSignups()
      .then((response) => {

        this.props.setAppState("_metadata", "signups", response)
        this.setState({
          loaded: true
        })

      })
      .fail(() => {
        console.log("failed to get signups");
      })
    }
  }

  textChange = (key, text) => {
    this.props.setAppState(this.modelId, key, text)
  }

  signUpClick = () => {
    this.props.setActiveModal("signUp")
    this.props.reportEvent('signup_clicked')
  }

  render() {

    var containerClass = classnames({
      'page-block sign-up-block jumbotron': true,
      'active': this.props.getActiveBlock() === this.modelId
    });

    let buttonColor = this.props.getAppState(Models.header.id, "buttonColor") || "#29c0d9";
    let buttonTextColor = this.props.getAppState(Models.header.id, "buttonTextColor") || "#FFFFFF";

    let buttonStyle = {
      color: buttonTextColor,
      backgroundColor: buttonColor,
    }

    let textColor = this.props.getAppState(this.modelId, "textColor") || "#6D6D6D";
    let textStyle = {
      color: textColor
    }

    // Editable text
    let description = this.props.getAppState(this.modelId, "description") || "<h1><b>This is where you encourage candidates to stay in touch, even if they're not ready to make a move today.</b></h1><p>Tell candidates why they would want to keep in touch. Try mentioning things that would add value for passive candidates, like joining a community, learning about future opportunities, and receiving ongoing updates.</p>";


    return (
      <section className={containerClass} onClick={this.blockClick}>
        <div className="block-content-container jumbotron-content vertical-align-container">
          <div className="vertical-aligner">
            <div className="jumbotron-message">
              <div>
                <Froala 
                  text={description} 
                  mode={this.props.getAppMode()}
                  modelKey="description"
                  modelId={this.modelId}
                  onChange={this.textChange}
                  style={textStyle}
                  type="jumbotron"
                  check={this.props.getAppState("_metadata", "company")}
                  {...this.props}
                />
              </div>
              <button
                className="btn btn-default "
                style={buttonStyle}
                onClick={this.signUpClick}
                >SIGN UP FOR UPDATES</button>
            </div>
          </div>
        </div>
        <JumbotronBackground modelId={this.modelId} {...this.props} selector=".sign-up-block"/>
      </section>
    );
  }
}

export default SignUpBlock;
