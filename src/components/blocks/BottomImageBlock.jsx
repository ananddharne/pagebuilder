import React, { Component } from 'react';
import Models from 'Models';
import classnames from 'classnames';
import JumbotronBackground from './elements/JumbotronBackground';


class BottomImage extends Component {

  componentWillMount() {
    this.modelId = Models.bottomImage.id;
  }

  blockClick = () => {
    this.props.setActiveBlock(this.modelId);
  }

  render() {

    var containerClass = classnames({
      'page-block bottom-image-block jumbotron': true,
      'active': this.props.getActiveBlock() === this.modelId
    });

    return (
      <section className={containerClass} onClick={this.blockClick}>
        <JumbotronBackground modelId={this.modelId} {...this.props} selector=".bottom-image-block"/>
        <div className="block-content-container jumbotron-content bottom-image vertical-align-container">
          <div className="vertical-aligner">
            {/* Intentionally left blank to keep dotted line around content */}
          </div>
        </div>
      </section>
    );
  }
}

export default BottomImage;
