import React, { Component } from 'react';
import Models from 'Models';
import classnames from 'classnames';
import smoothScroll from 'smoothScroll';

import placeholder from 'image-placeholder.svg'

class HeaderBlock extends Component {

  componentWillMount() {
    this.modelId = Models.header.id;

    this.setState({
      hoveredLink: null
    })
  }

  onTextChange = (e) => {
    this.props.setAppState(this.modelId, "title", e.target.value)
  }

  blockClick = () => {
    this.props.setActiveBlock(this.modelId);
  }

  linkClick = (e, target) => {
    // Prevent default link behavior
    e.preventDefault();
    smoothScroll(target, this.props.getAppMode());
  }

  linkMouseEnter = (index) => {
    this.setState({
      hoveredLink: index
    })
  }

  linkMouseLeave = (index) => {
    this.setState({
      hoveredLink: null
    })
  }

  render() {

    let containerClass = classnames({
      'navbar navbar-default page-block header-block': true,
      'active': this.props.getActiveBlock() === this.modelId
    });

    let buttonColor = this.props.getAppState(this.modelId, "buttonColor") || "#29c0d9";

    // Need to define styles for normal, hovered, then the "oppotunity" buttons normal and hovered styles
    let buttonStyle = {
      color: buttonColor
    }
    let hoveredButtonStyle = {
      color: "#FFFFFF",
      backgroundColor: buttonColor
    }
    let opportunityButtonStyle = {
      color: buttonColor,
      border: "2px solid " + buttonColor,
    }
    let hoveredOpportunityButtonStyle = {
      color: "#FFFFFF",
      backgroundColor: buttonColor,
      border: "2px solid " + buttonColor,
    }

    let headerImage = this.props.getAppState(this.modelId, "logo") || placeholder;

    return (
      <nav className={containerClass} onClick={this.blockClick}>
        <div className="container-fluid">
          <div className="logo-container">
            <div className="vertical-align-container">
              <div className="vertical-aligner">
                <img alt="Brand" src={headerImage} className={!this.props.getAppState(this.modelId, "logo") ? "edit-only" : ""} role="button" onClick={(e) => {this.linkClick(e, ".tagline-block")}}></img>
              </div>
            </div>
          </div>
          <ul className="nav navbar-nav navbar-right">
            <li>
              <a
                className="header-link"
                style={this.state.hoveredLink === "team" ? hoveredButtonStyle : buttonStyle}
                onClick={(e) => {
                  this.linkClick(e, ".team-block");
                  this.props.reportEvent('team_clicked');
                }}
                onMouseEnter={()=>this.linkMouseEnter("team")}
                onMouseLeave={()=>this.linkMouseLeave("team")}
              >TEAM</a>
            </li>
            <li>
              <a className="header-link"
                style={this.state.hoveredLink === "community" ? hoveredButtonStyle : buttonStyle}
                onClick={(e) => {
                  this.linkClick(e, ".sign-up-block");
                  this.props.reportEvent('community_clicked');
                }}
                onMouseEnter={()=>this.linkMouseEnter("community")}
                onMouseLeave={()=>this.linkMouseLeave("community")}
              >COMMUNITY</a>
            </li>
            <li>
              <a className="header-link"
                style={this.state.hoveredLink === "opportunity" ? hoveredOpportunityButtonStyle : opportunityButtonStyle}
                onClick={(e) => {
                  this.linkClick(e, ".goals-block");
                  this.props.reportEvent('opportunity_clicked');
                }}
                onMouseEnter={()=>this.linkMouseEnter("opportunity")}
                onMouseLeave={()=>this.linkMouseLeave("opportunity")}
              >OPPORTUNITY</a>
            </li>
          </ul>

        </div>
      </nav>
    );
  }
}

export default HeaderBlock;
