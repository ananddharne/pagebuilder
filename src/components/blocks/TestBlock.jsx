import React, { Component } from 'react';
import Models from 'Models';
import classnames from 'classnames';
import Froala from './elements/Froala';

class TestBlock extends Component {

  componentWillMount() {
    this.modelId = Models.test.id;

    // Testing
    // this.props.setAppState(this.modelId, "text", "Default text")
  }

  onTextChange = (e) => {
    this.props.setAppState(this.modelId, "text", e.target.value)
  }

  blockClick = () => {
    this.props.setActiveBlock(this.modelId);
  }

  render() {

    var containerClass = classnames({
      'page-block test-block': true,
      'active': this.props.getActiveBlock() === this.modelId
    });

    let description = 'Testing';

    return (
      <div className={containerClass} onClick={this.blockClick}>
        <h1>Test Block</h1>
        
      </div>
    );
  }
}

export default TestBlock;
