import React, { Component } from 'react';
import Models from 'Models';
import deleteImg from 'delete.svg';
import placeholder from 'image-placeholder-with-padding.svg';
import linkedIn from 'team/linkedIn.svg';
import classnames from "classnames";
import SingleLineEditor from "./SingleLineEditor";
import appArrayStateHelpers from "appArrayStateHelpers";

class TeamMember extends Component {

  componentWillMount() {
    this.modelId = Models.team.id;
  }

  blockClick = () => {
    this.props.setActiveBlock(this.modelId);
  }

  textChange = (key, text) => {
    appArrayStateHelpers.set(this, key, "teamMembers", text);
  }

  deleteTeamMemberClick = () => {
    this.props.setActiveDialog({
      description: "Are you sure you want to remove this team member?",
      confirmText: "Yes",
      cancelText: "No",
      confirm: this.deleteTeamMember
    });
  }

  deleteTeamMember = () => {
    // Make copy of array with slice() before modifying and saving
    let teamMembers = (this.props.getAppState(this.modelId, "teamMembers") || []).slice();

    // Remove item
    teamMembers.splice(this.props.index, 1);

    this.props.setAppState(this.modelId, "teamMembers" , teamMembers);

  }

  setActiveMember = (e) => {
    let block = "team:" + this.props.index;
    this.props.setActiveBlock(block);
    // Prevent normal block click from happening
    e.stopPropagation();
  }

  render() {

    // Need to get active block, get second part after ":", then convert to int
  let activeIndex = parseInt(this.props.getActiveBlock().split(":")[1], 10)

    let containerClass = classnames({
      "team-member": true,
      "active": activeIndex === this.props.index
    })

    let teamMembers = this.props.getAppState(this.props.modelId, "teamMembers");
    let teamMember = teamMembers[this.props.index];

    let person = teamMember.image || placeholder;
    // NOTE: Just using the key "name" broke the inline editor for unknown reasons
    let fullName = teamMember.fullName;
    let title = teamMember.title;
    let linkedInUrl = teamMember.linkedIn || "";

    let linkedInButton = {};

    // TODO: once the team members are pulled from users, we should have the user id to pass for reporting

    if (linkedInUrl) {
      linkedInButton = <a onClick={(e) => {this.props.reportEvent('linkedin_clicked',[fullName]);}} href={linkedInUrl} target="_blank"><img className="linked-in-button" alt="linked in" src={linkedIn} role="button"></img></a>
    } else {
      linkedInButton = null;
    }

    let profileStyle = {};
    if (this.props.getAppMode() === "edit") {
      profileStyle = {
        border: "2px dotted #979797"
      }
    } else {
      profileStyle = {
        border: "2px solid " + this.props.getAppState("header", "buttonColor")
      }
    }

    // TODO: Hide on preview/prod or not?
    let teamMemberClass = classnames({
      "team-member-image": true
    })

    return (
      <div className={containerClass} onClick={this.setActiveMember}>
        <img className="delete-team-member"
          alt="Delete team member button"
          role="button"
          src={deleteImg}
          onClick={this.deleteTeamMemberClick}
          style={this.props.getAppMode() === "edit" ? {display: "block"} : {display: "none"}}></img>
        <img className={teamMemberClass} alt="Team member profile" src={person} style={profileStyle}></img>
          <div className="name">
            <SingleLineEditor
              mode={this.props.getAppMode()}
              text={fullName}
              placeholder="Team Member Name"
              modelKey="fullName"
              onChange={this.textChange}
            />
          </div>
          <div className="title">
            <SingleLineEditor
              mode={this.props.getAppMode()}
              text={title}
              placeholder="Professional Title"
              modelKey="title"
              onChange={this.textChange}
          />
        </div>
        <div>
          {linkedInButton}
        </div>
      </div>
    );
  }
}

export default TeamMember;
