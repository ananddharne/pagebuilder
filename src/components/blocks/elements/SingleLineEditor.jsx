import React, { Component } from 'react';
import ContentEditable from "react-contenteditable";
import alerts from "alerts";
import classnames from "classnames";
import $ from "jquery";

class SingleLineEditor extends Component {

  componentWillMount = () => {
    this.setState({
      debounceTimeout: null
    })
  }

  componentWillUnmount = () => {
    clearTimeout(this.state.debounceTimeout);
  }

  onChange = (e) => {

    let value = e.target.value;
    if (this.props.isUrl) {
      value = value.replace(/&nbsp;/g, '');
      value = value.replace(/\s/g, '');
    }

    clearTimeout(this.state.debounceTimeout);

    let timeout = setTimeout(() => {
      this.performChange(value)
    }, 500)

    this.setState({
      debounceTimeout: timeout
    })
  }

  performChange = (value) => {
    this.props.onChange(this.props.modelKey, value);
  }

  onKeyDown = (e) => {
    // Show warning message if this should be a URL and we type in anything besdies letters/numbers/dashes

    // If enter is hit, blur element
    if (e.keyCode === 13) {
      e.preventDefault();

      $(this.refs.contentEditable.htmlEl).blur();
      window.getSelection().removeAllRanges();

      return;
    }

    if (this.props.isUrl) {
      let isValid = false;
      if ((e.keyCode >= 48 && e.keyCode <= 90)
      || (e.keyCode >= 16 && e.keyCode <= 18)
      || (e.keyCode >= 37 && e.keyCode <= 40)
      || e.keyCode === 8
      || e.keyCode === 9
      || e.keyCode === 20
      || e.keyCode === 91
      || e.keyCode === 189) {
        isValid = true;
      }

      if (!isValid) {
        e.preventDefault();
        alerts.show({
          status: "warning",
          message: "Please enter a URL that only contains numbers, letters, and dashes."
        })
      }

    }
  }

  render() {


    // TODO: Figure out why this doesn't work on main contentEditable element, requiring wrapping <span>
    let elementClass = classnames({
      "single-editor-placeholder-visible": !this.props.text,
      "preview-mode": this.props.mode !== "edit"
    })

    return (
      <span className={elementClass}>
        <ContentEditable
          style={{display: "inline-block"}}
          ref="contentEditable"
          disabled={this.props.mode !== "edit"}
          html={this.props.text || ""}
          onChange={this.onChange}
          onBlur={this.props.onBlur}
          onKeyDown={this.onKeyDown}
          spellCheck={false}
          data-placeholder={this.props.placeholder}
        />
    </span>

    );
  }
}

export default SingleLineEditor;
