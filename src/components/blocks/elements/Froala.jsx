import React, { Component } from 'react';

import '!style!css!font-awesome/css/font-awesome.css';
import "script-loader!../../../lib/froala/froala_editor.min.js";
import "script-loader!../../../lib/froala/plugins/paragraph_format.min.js";
import "script-loader!../../../lib/froala/plugins/lists.min.js";

let $ = window.$;

class Froala extends Component {

	componentWillMount() {

    // License Key for froala
    let activationKey = 'lJCJWECHICc2JOZWJ==';

    let h1ButtonId = 'h1'+this.props.modelId;
    let h2ButtonId = 'h2'+this.props.modelId;
		let toolbarButtons = ["bold","italic","underline",h1ButtonId,"undo","redo"];

		if(this.props.type === "modal"){
			toolbarButtons = ["bold","italic","underline",h2ButtonId,"formatUL","formatOL","undo","redo"];
		}

    let stateObject = {
      h1ButtonId: h1ButtonId,
      h2ButtonId: h2ButtonId,
      historyIndex: 0,
      froalaSelector: 'edit-'+this.props.modelId,
      options: {
        key: activationKey,
        toolbarButtons: toolbarButtons,
        toolbarButtonsMD: toolbarButtons,
        toolbarButtonsSM: toolbarButtons,
        toolbarButtonsXS: toolbarButtons,
        toolbarInline: true,
        toolbarVisibleWithoutSelection: true
      },
      mode: this.props.mode
    }

    if (this.props.jobId) {
      stateObject.jobId = this.props.jobId;
    }
 
    this.setState(stateObject);

  }

  componentWillReceiveProps(nextProps) {

    // Only ever update editors if it's in the job modal and the job changes
    if (nextProps.jobId) {
      if (nextProps.jobId !== this.state.jobId) {
        let oldText = $('#'+this.state.froalaSelector).froalaEditor('html.get');
        if (oldText !== nextProps.text) {
          $('#'+this.state.froalaSelector).froalaEditor('html.set', nextProps.text);
        }
        this.setState({
          jobId: nextProps.jobId
        })
      }
    }

    // Pretty hacky, but our company should only ever load once, so if that happens, grab
    // the text we should load from state

    let historyIndex = 0;
    let historyLength = 1;
    if (this.props.getAppStateHistory) {
      historyIndex = this.props.getAppStateHistory().index;
      historyLength = this.props.getAppStateHistory().stateHistory.length;
    }

    let loadCheck = false;
    if (nextProps.check && nextProps.check !== this.state.check) {
      loadCheck = true;
    }
    let indexCheck = false;
    if (historyIndex !== this.state.historyIndex && historyIndex !== historyLength - 1) {
      indexCheck = true;
    }

    if (loadCheck || indexCheck) {
     $('#'+this.state.froalaSelector).froalaEditor('html.set', nextProps.text);
      this.setState({
        check: nextProps.check,
        historyIndex: historyIndex,
      })
    }

  }

  componentDidMount() {
  	let _this2 = this;

  	$(function () {

  		// Create the header 1 button
      $.FroalaEditor.DefineIcon('h1', { NAME: 'header'});
      $.FroalaEditor.RegisterCommand(_this2.state.h1ButtonId, {
        title: 'Header 1',
        icon: 'h1',
        undo: true,
        focus: true,
        showOnMobile: true,
        refreshAfterCallback: true,
        callback: function () {
        	// Check what the selection is and then either call the header command or the paragraph command
          if ($(this.selection.element()).is('h1') || $(this.selection.element()).parent().is('h1') || $(this.selection.element()).parent().parent().is('h1') || $(this.selection.element()).parent().parent().parent().is('h1')){
            $('#'+_this2.state.froalaSelector).froalaEditor('paragraphFormat.apply', 'P');
          }
          else{
            $('#'+_this2.state.froalaSelector).froalaEditor('paragraphFormat.apply', 'H1');
          }
        },
        refresh: function ($btn) {
        	// Check what the selection is to show the header button selected or not
          if (($(this.selection.element()).is('h1') || $(this.selection.element()).parent().is('h1') || $(this.selection.element()).parent().parent().is('h1') || $(this.selection.element()).parent().parent().parent().is('h1')) && !$('button.fr-command[data-cmd="'+_this2.state.h1ButtonId+'"]').hasClass('fr-active')){
            $('button.fr-command[data-cmd="'+_this2.state.h1ButtonId+'"]').addClass('fr-active');
          }
          else{
            $('button.fr-command[data-cmd="'+_this2.state.h1ButtonId+'"]').removeClass('fr-active');
          }
        }
      });

      // Create the header 2 button
      $.FroalaEditor.DefineIcon('h2', { NAME: 'header'});
      $.FroalaEditor.RegisterCommand(_this2.state.h2ButtonId, {
        title: 'Header 2',
        icon: 'h2',
        undo: true,
        focus: true,
        showOnMobile: true,
        refreshAfterCallback: true,
        callback: function () {
        	// Check what the selection is and then either call the header command or the paragraph command
          if ($(this.selection.element()).is('h2') || $(this.selection.element()).parent().is('h2') || $(this.selection.element()).parent().parent().is('h2') || $(this.selection.element()).parent().parent().parent().is('h2')){
            $('#'+_this2.state.froalaSelector).froalaEditor('paragraphFormat.apply', 'P');
          }
          else{
            $('#'+_this2.state.froalaSelector).froalaEditor('paragraphFormat.apply', 'H2');
          }
        },
        refresh: function ($btn) {
        	// Check what the selection is to show the header button selected or not
          if (($(this.selection.element()).is('h2') || $(this.selection.element()).parent().is('h2') || $(this.selection.element()).parent().parent().is('h2') || $(this.selection.element()).parent().parent().parent().is('h2')) && !$('button.fr-command[data-cmd="'+_this2.state.h2ButtonId+'"]').hasClass('fr-active')){
            $('button.fr-command[data-cmd="'+_this2.state.h2ButtonId+'"]').addClass('fr-active');
          }
          else{
            $('button.fr-command[data-cmd="'+_this2.state.h2ButtonId+'"]').removeClass('fr-active');
          }
        }
      });

    	// Froala has a bug where if we dangerously set the html after updating the state, it will no longer let you edit
    	// So this is where we will input the content html once
    	$('#'+_this2.state.froalaSelector).html(_this2.props.text);

      $('#'+_this2.state.froalaSelector).froalaEditor(_this2.state.options || {});

      // Turn the edit mode on or off based on the mode
      if (_this2.state.mode === 'edit') {
        $('#'+_this2.state.froalaSelector).froalaEditor('edit.on');
      } else {
        $('#'+_this2.state.froalaSelector).froalaEditor('edit.off');
      }

      // If there is a content change, here is where we will update the state
      $('#'+_this2.state.froalaSelector).on('froalaEditor.contentChanged', function (e, editor) {
        _this2.props.onChange(_this2.props.modelKey, $('#'+_this2.state.froalaSelector).froalaEditor('html.get'));
      });

    });
  }

  modeCheck = (e) => {
    let check = this.props.getAppMode();
    if (check !== this.state.mode) {
      if (check === 'edit') {
        $('#'+this.state.froalaSelector).froalaEditor('edit.on');
      } else {
        $('#'+this.state.froalaSelector).froalaEditor('edit.off');
      }
      this.setState({
        mode: check
      });
    }
  }

	render() {

    return (
    	<div style={this.props.style} onClick={this.modeCheck}>
    		<div id={this.state.froalaSelector}></div>
    	</div>
    );
  }
}

export default Froala;