import React, { Component } from 'react';
import ReactPlayer from 'react-player';
import classnames from 'classnames';
import placeholder from 'image-placeholder-with-padding.svg';
import $ from 'jquery';


class JumbotronBackground extends Component {

  componentDidMount = () => {
    $(window).resize(this.updateDimensions);

    // Default which will get overwritten by updateDimensions
    this.setState({
      width: 0
    })
    this.updateDimensions();
  }
  componentWillUnmount = () => {
    $(window).off("resize", this.updateDimensions)
  }
  updateDimensions = () => {
    // Add 5 pixels to width just for cushion
    setTimeout(() => {
      this.setState({
        width: $(this.props.selector).width() + 5
      });
    })

  }

  getImageBackground = (modelId) => {
    let backgroundUrl = this.props.getAppState(modelId, "background") || placeholder;
    let urlString = "url('" + backgroundUrl + "') no-repeat center center";

    let backgroundStyle = {
      background: urlString || ""
    }

    // Zoom state is a value 0-100. So add that as a percentage, starting at 100%.
    let zoom = 1 + (this.props.getAppState(modelId, "zoom") || 1) / 100;
    let transformString = "scale(" + zoom + ")";

    // Slider sets default value to .01 instead of 0, so only add transform if it's not those values

    if (zoom > 1.01) {
      backgroundStyle.transformOrigin = "center center";
      backgroundStyle.transform = transformString;
    }

    let backgroundClasses = classnames({
      "background-layer image-background": true,
      "default-background edit-only": !this.props.getAppState(modelId, "background"),
      "parallax": this.props.getAppState(modelId, "parallax")
    })

    let opacityStyle = {
      backgroundColor: this.props.getAppState(modelId, "opacityColor") || "#FFFFFF",
      opacity: this.props.getAppState(modelId, "opacity") / 100
    }

    return <div>
      <div className={backgroundClasses} style={backgroundStyle}></div>
      <div className="opacity-layer" style={opacityStyle}></div>
    </div>
  }

  getVideoBackground = (modelId) => {
    let url = this.props.getAppState(modelId, "background");
    let opacityStyle = {
      backgroundColor: this.props.getAppState(modelId, "opacityColor"),
      opacity: this.props.getAppState(modelId, "opacity") / 100
    }

    // Default to 16:9 aspect ratio
    let factor = 1.78;
    let minWidth = 1114;
    let width = this.state.width
    width = width > minWidth ? width : minWidth;
    let height = width / factor;
    let minHeight = $(this.props.selector).height();

    // Use minheight if it's greater than our calculated height
    height = minHeight > height ? minHeight : height;

    return <div>
      <ReactPlayer
        url={url}
        playing={true}
        loop={true}
        controls={false}
        volume={0}
        width={width}
        height={height}
        className="background-video-container"
        style={{position: "absolute"}} />
      <div className="opacity-layer" style={opacityStyle}></div>
    </div>
  }

  getSolidBackground = (modelId) => {
    let backgroundStyle = {
      backgroundColor: this.props.getAppState(modelId, "background") || "#FFFFFF"
    }
    let opacityStyle = {
      backgroundColor: this.props.getAppState(modelId, "opacityColor") || "#FFFFFF",
      opacity: this.props.getAppState(modelId, "opacity") / 100
    }

    return <div>
      <div className="background-layer" style={backgroundStyle}></div>
      <div className="opacity-layer" style={opacityStyle}></div>
    </div>
  }

  getDefaultBackground = (modelId) => {
    let backgroundClasses = classnames({
      "background-layer image-background default-background": true,
      "edit-only": !this.props.getAppState(modelId, "background")
    })
    let urlString = "url('" + placeholder + "') no-repeat center center";
    let backgroundStyle = {
      background: urlString || ""
    }

    return <div className={backgroundClasses} style={backgroundStyle}></div>
  }

  render() {

    let modelId = this.props.modelId

    // Background
    let backgroundElement = null;

    let type = this.props.getAppState(modelId, "backgroundType");

    switch (type) {
      case "video":
        backgroundElement = this.getVideoBackground(modelId);
        break;
      case "solid":
        backgroundElement = this.getSolidBackground(modelId);
        break;
      case "image":
        backgroundElement = this.getImageBackground(modelId);
        break;
      default:
        backgroundElement = this.getDefaultBackground(modelId);
        break;
    }

    return (
      <div className="background-layer-container">
        {backgroundElement}
      </div>
    );
  }
}

export default JumbotronBackground;
