import React, { Component } from 'react';
import mapsHelper from "mapsHelper";
import placeholder from "location/map.svg";
import deepEqual from "deep-equal";

class GoogleMap extends Component {

  componentWillMount() {
    this.setState({
      map: {},
      location: {
        lat: 0,
        lng: 0
      }
    })
  }

  componentDidMount() {
    // Asynchronously loaded google maps API, so wait for it to load before rendering
    mapsHelper.getLoadPromise().done(() => {
      this.initMap();
    })
    .fail((e) => {
      console.error("Error loading google maps " , e);
    })
  }

  // Use this function to compute when location has changed and update UI
  // Note that we're using "label" to compute when we need to change
  componentWillReceiveProps(nextProps) {

    // Need to wrap whole thing in promise resolver to make sure API is loaded
    mapsHelper.getLoadPromise()
    .done(() => {

      // If location label doesn't match our state's label, update location, and then callback after state
      // is set to set new center and marker on map, assuming the map has loaded
      if (nextProps.location && !deepEqual(nextProps.location, this.state.location)) {
        this.setState({
          location: nextProps.location
        }, () => {

          if (this.state.map) {

            // Trigger a resize since we might be going from hidden to visible, then set center/marker for new location
            setTimeout(() => {
              this.initMap();
            }, 501)

          }

        });
      }

    })
    .fail((e) => {
      console.error("Error loading google maps " , e)
    })
  }

  initMap = () => {
    var map;

    var myLatLng = {lat: 0, lng: 0};

    if (this.props.location) {
      myLatLng = {lat: this.props.location.lat, lng: this.props.location.lng}
    }

    let id = this.props.id;
    map = new window.google.maps.Map(document.getElementById(id), {
      center: myLatLng,
      zoom: 14,
      zoomControl: true,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false,
      fullscreenControl: false,
      scrollwheel: false
    });

    var marker = new window.google.maps.Marker({
       position: myLatLng,
       map: map,
       title: 'test'
     });

     var infowindow = new window.google.maps.InfoWindow({
       content: this.props.getAppState("_metadata", "company") || "Our Location"
     });
     infowindow.open(map, marker)

     this.setState({
       map: map,
       marker: marker,
       infowindow: infowindow
     })
  }

  render() {

    // We send in a default prop for location, but when we update it's outside of a normal React update,
    // so we need to keep track of default prop passed in as well as internal state has been set
    let mapStyle;
    let placeholderStyle;
    let isDefaultSetting = (this.props.location && this.props.location.place_id) ? false : true;


    if (this.props.location && !isDefaultSetting) {
      mapStyle = {display: "block"};
      placeholderStyle = {display: "none", height: "200px"}
    } else {
      mapStyle = {display: "none"};
      placeholderStyle = {display: "block", "height": "200px"}
    }

    // Additional check for mode
    if (this.props.mode !== "edit") {
      placeholderStyle = {display: "none"}
    }

    return (
      <div style={{height: "100%"}}>
        <div id={this.props.id} className="google-map-component" style={mapStyle}></div>
        <img src={placeholder} alt="Map Placeholder" style={placeholderStyle}></img>
      </div>
    );
  }
}

export default GoogleMap;
