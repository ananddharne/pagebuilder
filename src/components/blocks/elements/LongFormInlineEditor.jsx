import React, { Component } from 'react';
import {EditorState} from 'draft-js';
import Editor from 'draft-js-plugins-editor';
import {stateToHTML} from 'draft-js-export-html';
import {stateFromHTML} from 'draft-js-import-html';
import $ from "jquery";
import Immutable from 'immutable';
import classnames from "classnames";

// Style object for controls
// src: https://www.npmjs.com/package/draft-js-richbuttons-plugin
const InlineFormattingButton = ({iconName, toggleInlineStyle, isActive, label, inlineStyle, onMouseDown, title }) =>
  <span onClick={toggleInlineStyle} onMouseDown={onMouseDown} role="button">
    <span
      className={`log-form-editor-button is-active-${isActive} button-${iconName}`}
    >{title}</span>
  </span>;

const BlockFormattingButton = ({iconName, toggleBlockType, isActive, title, test }) =>
  <span
      role="button"
      onClick={(e) => {
        toggleBlockType(e);
      }}>
    <span
      className={`log-form-editor-button is-active-${isActive}`}
    >{title}</span>
  </span>;

class LongFormInlineEditor extends Component {

  componentWillMount() {

    let content = this.props.text;

    // TODO: Potentially don't initialize here since we reload anyway when page is loaded, which is currently
    // checked in componentWillReceiveProps
      this.setState({
        editorState: EditorState.createWithContent(stateFromHTML(content)),
        controlsVisible: false,
        activeElement: null,
        loadedCompany: null,
        historyIndex: 0,
        debounceTimeout: null,
        blurTimeout: null
      })
    }

    // Clear debounce timeout when unmounting
    componentWillUnmount() {
      clearTimeout(this.state.debounceTimeout)
      clearTimeout(this.state.blurTimeout)

    }

    componentWillReceiveProps(nextProps) {
      // Pretty hacky, but our company should only ever load once, so if that happens, grab
      // the text we should load from state
      // Also reload on undo/redo

      let historyIndex = 0;
      let historyLength = 1;
      if (this.props.getAppStateHistory) {
        historyIndex = this.props.getAppStateHistory().index;
        historyLength = this.props.getAppStateHistory().stateHistory.length;
      }

      let loadCheck = false;
      if (nextProps.check && nextProps.check !== this.state.check) {
        loadCheck = true;
      }
      let indexCheck = false;
      if (historyIndex !== this.state.historyIndex && historyIndex !== historyLength - 1) {
        indexCheck = true;
      }

      if (loadCheck || indexCheck) {
        this.setState({
          check: nextProps.check,
          historyIndex: historyIndex,
          editorState: EditorState.createWithContent(stateFromHTML(nextProps.text))
        })
      }

    }

    onChange = (editorState) => {
      this.setState({editorState});

      // Really bad performance when timeout, so debounce the input to only save when we stop typing
      clearTimeout(this.state.debounceTimeout);

      let debounceTimeout = setTimeout(() => {
        this.performChange()
      }, 500)

      this.setState({
        debounceTimeout: debounceTimeout
      })

    }

    performChange = () => {
      this.props.onChange(this.props.modelKey, stateToHTML(this.state.editorState.getCurrentContent()));
    }

    onFocus = () => {
      // Save active element in state because of bug in onBlur -> see below
      this.setState({
        controlsVisible: true,
        activeElement: $(document.activeElement)
      })

      // Call prop onfocus as well if it's defined
      if (this.props.onFocus) {
        this.props.onFocus();
      }
    }

    onBlur = (e) => {
      // Weird bug where clicking on a block formatting button calls the "onBlur" function of the editor
      // but NOT the onFocus afterwards. This checks after a delay if our active element
      // is the editor, and if not then hide the controls.  Time might need to be adjusted if we have
      // issues on slower computers.
      let blurTimeout = setTimeout(() => {
        if (!$(document.activeElement).is(this.state.activeElement)) {
          this.setState({
            controlsVisible: false
          })
        }
      }, 200)

      this.setState({
        blurTimeout: blurTimeout
      })

    }

    render() {

      let editorClasses = classnames({
        "long-form-inline-editor": true,
        "editor-hidden": !this.state.controlsVisible || this.props.mode !== "edit"
      });

      let editorAttr;
      if (!this.state.controlsVisible || this.props.mode !== "edit") {
        editorAttr = {
          "aria-hidden": true
        }
      }

      const richButtonsPlugin = this.props.plugin;
      const {ItalicButton, BoldButton, UnderlineButton, OLButton, ULButton, H1Button, H2Button} = richButtonsPlugin;

      const plugins = [
        richButtonsPlugin
      ]

      let toolbar;
      // Different toolbars for different scenerios
      if (this.props.type === "jumbotron") {
        toolbar = <div className={editorClasses} {...editorAttr}>
          <BoldButton>
            <InlineFormattingButton iconName="bold" title="B" />
          </BoldButton>
          <ItalicButton>
            <InlineFormattingButton iconName="italic" title="I" />
          </ItalicButton>
          <UnderlineButton>
            <InlineFormattingButton iconName="underline" title="U" />
          </UnderlineButton>

          <H1Button onClick={this.blockClick}>
            <BlockFormattingButton iconName="h1" title="H1" />
          </H1Button>
        </div>
      } else {
        toolbar = <div className={editorClasses} {...editorAttr}>
          <BoldButton>
            <InlineFormattingButton iconName="bold" title="B" />
          </BoldButton>
          <ItalicButton>
            <InlineFormattingButton iconName="italic" title="I" />
          </ItalicButton>
          <UnderlineButton>
            <InlineFormattingButton iconName="underline" title="U" />
          </UnderlineButton>

          <H2Button onClick={this.blockClick}>
            <BlockFormattingButton iconName="h2" title="H2" />
          </H2Button>
          <ULButton>
            <BlockFormattingButton iconName="ul" title="•" />
          </ULButton>
          <OLButton>
            <BlockFormattingButton iconName="ol" title="1." />
          </OLButton>
        </div>
      }

      const blockRenderMap = Immutable.Map({
        'header-one': {
         element: 'h1'
        },
        'header-two': {
         element: 'h2'
        },
        'ordered-list-item': {
         element: 'li',
         wrapper: <ol />
        },
        'unordered-list-item': {
         element: 'li',
         wrapper: <ul />
        },
        'paragraph': {
         element: 'p'
        },
        'unstyled': {
          element: 'p'
        }
      });

      return (
        <div style={this.props.style}>
          {toolbar}
          <Editor
            editorState={this.state.editorState}
            onChange={this.onChange}
            onFocus={this.onFocus}
            onBlur={this.onBlur}
            plugins={plugins}
            readOnly={this.props.mode !== "edit"}
            blockRenderMap={blockRenderMap}
            placeholder="Click Below to Enter Text"
          />
      </div>
      );
    }
  }

export default LongFormInlineEditor;
