import React, { Component } from 'react';
import HeaderBlock from 'blocks/HeaderBlock';
import TaglineBlock from 'blocks/TaglineBlock';
import MissionBlock from 'blocks/MissionBlock';
import GoalsBlock from 'blocks/GoalsBlock';
import TeamBlock from 'blocks/TeamBlock';
import BottomImageBlock from 'blocks/BottomImageBlock';
import LocationBlock from 'blocks/LocationBlock';
import MapBlock from 'blocks/MapBlock';
import SignUpBlock from 'blocks/SignUpBlock';
import FooterBlock from 'blocks/FooterBlock';

class BlockContainer extends Component {

  componentWillMount() {
    this.setState({
      scrollPosition: 0
    })
  }

  render() {

    // Add scroll position to other props
    let defaultProps = {...this.props}

    return (
      <div className="blocks-container" onScroll={this.onScroll}>
        <HeaderBlock {...defaultProps} />
        <TaglineBlock {...defaultProps} />
        <MissionBlock {...defaultProps} />
        <GoalsBlock {...defaultProps} />
        <TeamBlock {...defaultProps} />
        <SignUpBlock {...defaultProps} />
        <div className="two-column-block">
          <div className="column-left">
            <LocationBlock {...defaultProps} />
            <MapBlock {...defaultProps} />
          </div>
          <div className="column-right">
            <BottomImageBlock {...defaultProps} />
          </div>
        </div>
        <FooterBlock {...defaultProps} />
      </div>
    );
  }
}

export default BlockContainer;
