import React, { Component } from 'react';
import Models from 'Models';
import happieLogo from 'happie-logo-gray.svg';
import classnames from 'classnames';

class FooterBlock extends Component {

  componentWillMount() {
    this.modelId = Models.footer.id;
  }

  render() {

    let containerClass = classnames({
      'page-block footer-block vertical-align-container': true,
      'active': this.props.getActiveBlock() === this.modelId
    });

    return (
      <footer className={containerClass}>
        <div className="vertical-aligner">
          <div>This page is powered by <a href="https://gethappie.me" target="_blank"><img src={happieLogo} alt="Happie Logo"></img></a></div>
          <div><a href="https://app.gethappie.me/privacy" target="_blank"><b>Privacy Policy</b></a></div>
        </div>
      </footer>
    );
  }
}

export default FooterBlock;
