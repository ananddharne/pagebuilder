import React, { Component } from 'react';
import HeaderControl from "controls/HeaderControl";
import TaglineControl from "controls/TaglineControl";
import MissionControl from "controls/MissionControl";
import GoalsControl from "controls/GoalsControl";
import TeamControl from "controls/TeamControl";
import SignUpControl from "controls/SignUpControl";
import BottomImageControl from "controls/BottomImageControl";
import LocationControl from "controls/LocationControl";
import FooterControl from "controls/FooterControl";
import JobsModalControl from "controls/JobsModalControl";
import Models from "Models";

import classnames from "classnames";

class ControlsContainer extends Component {
  render() {

    // List all possible control components
    // Note that `Map` block shares a control with `Location`
    let controls = [
      <HeaderControl key={Models.header.id}  {...this.props} />,
      <TaglineControl key={Models.tagline.id}  {...this.props} />,
      <MissionControl key={Models.mission.id}  {...this.props} />,
      <GoalsControl key={Models.goals.id}  {...this.props} />,
      <TeamControl key={Models.team.id}  {...this.props} />,
      <SignUpControl key={Models.signUp.id}  {...this.props} />,
      <BottomImageControl key={Models.bottomImage.id}  {...this.props} />,
      <LocationControl key={Models.location.id}  {...this.props} />,
      <JobsModalControl key={Models.jobs.id} {...this.props} />,
      <FooterControl key={Models.footer.id}  {...this.props} />
    ]

    // Filter out the currently active control
    let activeControl = controls.filter((value, key) => {
      // Get value before ":" to set as active block
      let block = this.props.getActiveBlock().split(":")[0];
      return value.key === block
    })

    let containerClass = classnames({
      "controls-container": true,
      "collapsed": this.props.getAppMode() !== "edit"
    })

    let hiddenAttr =  {};
    if (this.props.getAppMode() !== "edit") {
      hiddenAttr = {
        "aria-hidden": true
      }
    }

    return (
      <div className={containerClass} {...hiddenAttr}>
        {activeControl}
      </div>
    );
  }
}

export default ControlsContainer;
