import React, { Component } from 'react';
import Models from 'Models';
import FieldSet from './elements/FieldSet';
import JobsControl from "./elements/JobsControl"
import Input from "./elements/Input";
import ImageUpload from "./elements/ImageUpload";

class JobsModalControl extends Component {

  componentWillMount() {
    this.modelId = Models.jobs.id;

    // Possible states: null (default), "exists", "nonexistant"
    this.setState({
      emailStatus: null,
      emailButtonText: ""
    })
  }

  render() {

    let defaultProps = {
      modelId: this.modelId,

    }
    let elementProps = {...defaultProps, ...this.props};

    let index = parseInt(this.props.getActiveModal().split(":")[1], 10);

    let jobsElements = [
      <JobsControl key="jobs" {...elementProps}></JobsControl>
    ]

    let logoElements = [
      <ImageUpload key="logo" hideControls={true} {...elementProps} modelId="header" modelKey="logo"></ImageUpload>
    ]

    /*
    TODO: Remove this, left just for reference how we did hiring manager controls before

    let hiringManagerElements = [
      <Input key="fullName" modelKey="hiringManagerFullName:jobs" title="Name" placeholder="" index={index} {...elementProps} />,
      <Input key="title" modelKey="hiringManagerTitle:jobs" title="Professional Title" placeholder="" index={index} {...elementProps} />,
      <Input key="linkedInUrl" modelKey="hiringManagerLinkedIn:jobs" title="LinkedIn Profile URL" placeholder="" type="url" index={index} {...elementProps} />,
      <HiringManagerInput key="email" modelKey="hiringManagerEmail:jobs" title="Email Address" placeholder="" index={index} tooltip="The email address you enter will not be displayed publicly. We'll use it to assign a hiring manager to your open role." {...elementProps} />,
    ]
    */

    let linkElements = [
      <Input key="link" modelKey="actionUrl:jobs" title="URL" placeholder="http://example.com" index={index} {...elementProps} />
    ]

    return (
      <div>
        <FieldSet legend="Your Job(s)" elements={jobsElements}></FieldSet>
        <FieldSet legend="Your Logo" elements={logoElements}></FieldSet>
        <FieldSet legend="Custom Apply Link" elements={linkElements}></FieldSet>
      </div>
    );
  }
}

export default JobsModalControl;
