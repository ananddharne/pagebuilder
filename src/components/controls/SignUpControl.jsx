import React, { Component } from 'react';
import Models from 'Models';
import FieldSet from './elements/FieldSet';
import ImageUpload from './elements/ImageUpload';
import ColorPicker from './elements/ColorPicker';
import Dropdown from "./elements/Dropdown";
import VideoUpload from "./elements/VideoUpload";
import Slider from "./elements/Slider";
import SignUpList from "./elements/SignUpListControl";

class SignUpControl extends Component {

  componentWillMount() {
    this.modelId = Models.signUp.id;

    this.setState({
      activeDropdownElement: null
    })
  }

  render() {

    let defaultProps = {
      modelId: this.modelId
    }
    let elementProps = {...defaultProps, ...this.props};

    let dropdownElements = [
      {
        title: "Image",
        type: "image"
      },
      {
        title: "Video",
        type: "video"
      },
      {
        title: "Solid Color",
        type: "solid"
      }
    ]

    let backgroundElements = {
      "image": <ImageUpload key="image-upload" modelKey="background" tooltip="Use a JPG, PNG, or GIF under 5MB in size. Resolution of 1400 by 1200 pixels looks best." {...elementProps} />,
      "video": <VideoUpload key="video-upload" {...elementProps} />,
      "solid": <div key="solidColorDropdown">
        <ColorPicker key="background-solid" modelKey="background" {...elementProps} />
        <Slider key="opacity" modelKey="opacity" title="Opacity" {...elementProps} />
        <ColorPicker key="opacity-color-solid" modelKey="opacityColor" {...elementProps} />
      </div>
    }

    let backgroundKey = this.props.getAppState("signUp", "backgroundType");
    let backgroundElement = backgroundElements[backgroundKey];

    let imageElements = [
      <Dropdown
        key="image"
        modelKey="backgroundType"
        resetKey="background"
        defaultTitle="Select a background type"
        elements={dropdownElements}
        {...elementProps}
        />
    ]

    if (backgroundElement) {
      imageElements.push(backgroundElement);
    }

    let textColorElements = [
      <ColorPicker key="buttonColor" modelKey="textColor" {...elementProps} />
    ]

    let signUpElements = [
      <SignUpList key="signUpFields" modelKey="signUpFields" {...elementProps} />
    ]

    return (
      <div>
        <FieldSet legend="Community Signups" elements={signUpElements}></FieldSet>
        <FieldSet legend="Background" elements={imageElements}></FieldSet>
        <FieldSet legend="Text Color" elements={textColorElements}></FieldSet>
      </div>
    );
  }
}

export default SignUpControl;
