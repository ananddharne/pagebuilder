import React, { Component } from 'react';
import Models from 'Models';
import FieldSet from './elements/FieldSet';
import ImageUpload from './elements/ImageUpload';
import ColorPicker from './elements/ColorPicker';
import Dropdown from "./elements/Dropdown";
import VideoUpload from "./elements/VideoUpload";
import Slider from "./elements/Slider";
import TeamMemberControl from "./TeamMemberControl";

class TeamControl extends Component {

  componentWillMount() {
    this.modelId = Models.team.id;

    this.setState({
      activeDropdownElement: null
    })
  }

  render() {

    let defaultProps = {
      modelId: this.modelId
    }
    let elementProps = {...defaultProps, ...this.props};

    let dropdownElements = [
      {
        title: "Image",
        type: "image"
      },
      {
        title: "Video",
        type: "video"
      },
      {
        title: "Solid Color",
        type: "solid"
      }
    ]

    let backgroundElements = {
      "image": <ImageUpload key="image-upload" modelKey="background" tooltip="Use a JPG, PNG, or GIF under 5MB in size. Resolution of 1400 by 1200 pixels looks best." {...elementProps} />,
      "video": <VideoUpload key="video-upload" {...elementProps} />,
      "solid": <div key="solidColorDropdown">
        <ColorPicker key="background-solid" modelKey="background" {...elementProps} />
        <Slider key="opacity" modelKey="opacity" title="Opacity" {...elementProps} />
        <ColorPicker key="opacity-color-solid" modelKey="opacityColor" {...elementProps} />
      </div>
    }

    let backgroundKey = this.props.getAppState("team", "backgroundType");
    let backgroundElement = backgroundElements[backgroundKey];

    let imageElements = [
      <Dropdown
        key="image"
        modelKey="backgroundType"
        resetKey="background"
        defaultTitle="Select a background type"
        elements={dropdownElements}
        {...elementProps}
        />
    ]

    if (backgroundElement) {
      imageElements.push(backgroundElement);
    }

    let iconElements = [
      <ImageUpload key="icon-image-upload" modelKey="image" tooltip="Use a JPG, PNG, or GIF under 5MB in size. Resolution of 250 by 250 pixels looks best." hideControls={true} {...elementProps} />
    ]

    let textColorElements = [
      <ColorPicker key="buttonColor" modelKey="textColor" {...elementProps} />
    ]

    // Index number for selected team member (if there is one)
    let teamMemberIndex = this.props.getActiveBlock().split(":")[1];

    // If there's an index, only show team member control. Otherwise, show options for team block
    // TODO: Figure out if there's a better way to do this without adding all the extraneous <div> tags
    let controls;
    if (teamMemberIndex) {
      controls = <div>
        <TeamMemberControl index={teamMemberIndex} {...this.props}></TeamMemberControl>
      </div>;
    } else {
      controls = <div>
        <FieldSet legend="Background" elements={imageElements}></FieldSet>
        <FieldSet legend="Icon" elements={iconElements}></FieldSet>
        <FieldSet legend="Text Color" elements={textColorElements}></FieldSet>
      </div>;
    }

    return (
      <div>{controls}</div>
    );
  }
}

export default TeamControl;
