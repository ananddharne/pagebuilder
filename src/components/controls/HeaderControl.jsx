import React, { Component } from 'react';
import Models from 'Models';
import FieldSet from './elements/FieldSet';
import ImageUpload from './elements/ImageUpload';
import TextArea from './elements/TextArea';
import ColorPicker from './elements/ColorPicker';

class HeaderControl extends Component {

  componentWillMount() {
    this.modelId = Models.header.id;
  }

  render() {

    // Set default props, then merge with our existing props that are passed down from App.
    // This just saves copy/pasting props for all elements
    let defaultProps = {
      modelId: this.modelId
    }
    let elementProps = {...defaultProps, ...this.props};

    let logoElements = [
      <ImageUpload key="0" modelKey="logo" hideControls={true} tooltip="Use a JPG, PNG, or GIF under 5MB in size. Resolution of 250 by 250 pixels looks best." {...elementProps} />
    ]

    let titleTooltip = "Page Title is used for both the title of your page and how search engines display your page on results pages. The recommended length for search engine optimization is 55 characters to ensure that your title isn’t cut off, but it’s entirely up to you.";

    let descriptionTooltip = "Page Descriptions impact how search engines find your page and display it on results pages. The recommended length for search engine optimization is 155 characters to ensure that your page description isn’t cut off, but it’s entirely up to you.";

    let infoElements = [
      <TextArea key="title" modelKey="title" title="Page Title" placeholder="Enter your page title here..." {...elementProps} tooltip={titleTooltip} characterCount={55} />,
      <TextArea key="keywords" modelKey="keywords" title="Keywords" placeholder="Enter keywords about your opportunity, team, and skills required..." {...elementProps} />,
      <TextArea key="description" modelKey="description" title="Page Description" placeholder="Enter 1-2 sentences that summarize your page..." {...elementProps} tooltip={descriptionTooltip} characterCount={155} />
    ]

    let buttonColorElements = [
      <ColorPicker key="buttonColor" modelKey="buttonColor" {...elementProps} />
    ]

    let buttonTextColorElements = [
      <ColorPicker key="buttonTextColor" modelKey="buttonTextColor" {...elementProps} />
    ]


    return (
      <div>
        <FieldSet legend="Your Logo" elements={logoElements}></FieldSet>
        <FieldSet legend="Page Info" elements={infoElements}></FieldSet>
        <FieldSet legend="Button Color" elements={buttonColorElements}></FieldSet>
        <FieldSet legend="Button Text Color" elements={buttonTextColorElements}></FieldSet>
      </div>
    );
  }
}

export default HeaderControl;
