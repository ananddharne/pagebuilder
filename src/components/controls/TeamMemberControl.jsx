import React, { Component } from 'react';
import Models from 'Models';
import FieldSet from './elements/FieldSet';
import ImageUpload from './elements/ImageUpload';
import Input from "./elements/Input";

class TeamMemberControl extends Component {

  componentWillMount() {
    this.modelId = Models.team.id;

    this.setState({
      activeDropdownElement: null
    })
  }

  render() {

    let defaultProps = {
      modelId: this.modelId
    }
    let elementProps = {...defaultProps, ...this.props};


    let teamElements = [
      <ImageUpload key="image-upload" modelKey="image:teamMembers" tooltip="Use a JPG, PNG, or GIF under 5MB in size. Resolution of 150 by 150 pixels looks best." hideControls={true} {...elementProps} />,
      <Input key="fullName" modelKey="fullName:teamMembers" title="Name" placeholder="" {...elementProps} />,
      <Input key="title" modelKey="title:teamMembers" title="Professional Title" placeholder="" {...elementProps} />,
      <Input key="linkedIn" modelKey="linkedIn:teamMembers" title="LinkedIn Profile URL" type="url" placeholder="" {...elementProps} />
    ]

    // Only show team member controls if there is an active block chosen (indicated with ":" in active block)
    let teamMemberElement = {};

    if (this.props.getActiveBlock().split(":")[1]) {
      teamMemberElement = <FieldSet legend="Team Member" elements={teamElements}></FieldSet>
    } else {
      teamMemberElement = null;
    }

    return (
      <div>
        {teamMemberElement}
      </div>
    );
  }
}

export default TeamMemberControl;
