import React, { Component } from 'react';
import Models from 'Models';
import FieldSet from './elements/FieldSet';
import TextArea from './elements/TextArea';

class TestControl extends Component {

  componentWillMount() {
    this.modelId = Models.test.id;
  }

  onTextChange = (e) => {
    this.props.setAppState(this.modelId, "text", e.target.value)
  }

  render() {

    let defaultProps = {
      modelId: this.modelId
    }
    let elementProps = {...defaultProps, ...this.props};

    // Note that specifing an integer key on each element doesn't do anything but suppress the warning
    // We should be fine since app state is tracked in one store, but good to note in case old element
    // states are being carried over to new ones.
    // See http://blog.arkency.com/2014/10/react-dot-js-and-dynamic-children-why-the-keys-are-important/
    let elements = [
      <TextArea key="description" modelKey="text" title="Test Title" placeholder="Test placeholder" {...elementProps} />
    ]


    return (
      <div>
        <FieldSet legend="Your Logo" elements={elements}></FieldSet>
      </div>
    );
  }
}

export default TestControl;
