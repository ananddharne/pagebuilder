import React, { Component } from 'react';
import infoButton from 'info-tooltip.png';

class Tooltip extends Component {

  componentWillMount() {
    this.setState({
      tooltipVisible: false
    })
  }

  tooltipMouseEnter = () => {
    this.setTooltipVisible(true);
  }
  tooltipMouseLeave = () => {
    this.setTooltipVisible(false);
  }

  setTooltipVisible = (isVisible) => {
    this.setState({
      tooltipVisible: isVisible
    })
  }


  render() {

    let tooltipID = this.props.id || this.props.modelId + "-custom-tooltip";
    let tooltipMessage = this.props.tooltip || "Use a JPG, PNG, or GIF under 5MB in size. Resolution of 1400 by 1200 pixels looks best.";
    let tooltipStyle = {};
    if (this.state.tooltipVisible) {
      tooltipStyle.display = "block";
    } else {
      tooltipStyle.display = "none";
    }

    return (
      <span className="custom-tooltip-container" style={this.props.getAppMode() !== "edit" ? {display: "none"} : {}}>
        <div id={tooltipID}
          style={tooltipStyle}
          className="custom-tooltip"
          role="tooltip">
          <p>{tooltipMessage}</p>
        </div>
        <img className="info-button" src={infoButton} alt="info button" onMouseEnter={this.tooltipMouseEnter} onMouseLeave={this.tooltipMouseLeave}></img>
      </span>
    );
  }
}

export default Tooltip;
