import React, { Component } from 'react';
import addJobImgInactive from 'jobs/add-job.svg';
import addJobImgActive from 'jobs/add-job-active.svg';
import Reorder from 'react-reorder';
import JobListItem from './JobListItem';
import api from "api";
import alerts from "alerts";
import $ from "jquery";

class JobsControl extends Component {

  componentWillMount() {
    this.setState({
      addJobImg: addJobImgInactive
    });
  }

  addJobClick = () => {
    this.props.setActiveModal('addJob');
  }

  reorderComplete = (event, itemThatHasBeenMoved, itemsPreviousIndex, itemsNewIndex, reorderedArray) => {

    // Make API call to reorder reqs
    let requestObject = [];

    for (let i = 0; i < reorderedArray.length; i++) {
      requestObject.push({
        position: i + 1,
        pk: reorderedArray[i].pk
      })
    }

    api.reorderJobs(requestObject)
    .then((response) => {
      let newArray = reorderedArray.slice();
      this.props.setAppState(this.props.modelId, "jobs" , newArray, true);
    })
    .fail((response) => {
      console.log("failed to reoder array " , response);
    })


  }

  editClick = (e) => {
    // Hacky way to get index of the edit button clicked. Not using built in click method
    // for Reorder library because it makes us choose a delay between dragging and clicking which can be wonky
    let target = $(e.target);

    let parentItem = target.parents(".jobs-draggable-item");
    let index = $(".jobs-draggable-item").index(parentItem);

    // If goals active block has an index after, that means the job is selected
    let modal = "jobs:" + index;

    this.props.setActiveBlock("jobs");
    this.props.setActiveModal(modal);

  }

  removeClick = (id) => {

    // Make copy of array with slice() before modifying and saving
    let jobs = (this.props.getAppState(this.props.modelId, "jobs") || []).slice();

    api.deleteJob(id)
    .done((data) => {
      // Remove the job from the array
      let removeIndex;
      $(jobs).each((index,value) => {
        if (value.id === id) {
          removeIndex = index;
          return false;
        }
      });
      jobs.splice(removeIndex,1);

      this.props.setAppState(this.props.modelId, "jobs" , jobs);

      alerts.show({
        status: "success",
        message: "Woohoo! You successfully removed this job from your page! Be sure to save and publish the changes you made so that candidates won't see the job."
      })
    })
    .fail((e) => {
      alerts.show({
        status: "error",
        message: "Whoopsie! For some reason, there was an error removing this job from your page. Please refresh the window, and try again. Sorry for the inconvenience!"
      })
    });

  }

  addEnter = () => {
    this.setState({
      addJobImg: addJobImgActive
    });
  }

  addLeave = () => {
    this.setState({
      addJobImg: addJobImgInactive
    });
  }

  render() {

    let jobs = this.props.getAppState(this.props.modelId, "jobs").slice(0);

    // Add unique key for draggable list
    jobs = jobs.map((value, index) => {
      value.uniqueTitle = value.title + ":" + index;
      return value;
    })

    // Reorder component is kinda funky, so need to send in shared props like this
    let sharedProps = {
      "editClick": this.editClick,
      "removeClick": this.removeClick,
      ...this.props
    }

    return (
      <label>
        {this.props.title}
        <div className="add-new-job-container" onMouseEnter={this.addEnter} onMouseLeave={this.addLeave} onClick={this.addJobClick}>
          <img role="button" alt="add job" src={this.state.addJobImg} onClick={this.addJobClick}></img>
          <span role="button"><b>Add a Job</b></span>
        </div>
        <ul className="jobs-list">
          <Reorder
            itemKey="uniqueTitle"
            lock="horizontal"
            list={jobs}
            listClass="jobs-draggable-list"
            itemClass="jobs-draggable-item"
            template={JobListItem}
            callback={this.reorderComplete}
            itemClicked={this.itemClicked}
            sharedProps={sharedProps}
            >
          </Reorder>
        </ul>
      </label>
    );
  }
}

export default JobsControl;
