import React, { Component } from 'react';

class Dropdown extends Component {

  componentWillMount() {
    this.setState({
      activeTitle: null
    })
  }

  dropdownClick = (element) => {
    // Type is for internal state, title is human-readable dropdown label that is set when dropown is clicked
    this.props.setAppState(this.props.modelId, this.props.modelKey, element.type);

    // If we have a reset key, set it to blank on selecting the dropdown (in case type changes)
    if (this.props.resetKey) {
      this.props.setAppState(this.props.modelId, this.props.resetKey, "");
    }
  }

  getTitle = () => {
    let dropdownType = this.props.getAppState(this.props.modelId, this.props.modelKey);
    let title = this.props.defaultTitle;

    // Loop through elements and return title (if there's a type selected)
    if (dropdownType) {
      for (let i = 0; i < this.props.elements.length; i++) {
        if (this.props.elements[i].type === dropdownType) {
          title = this.props.elements[i].title;
          break;
        }
      }
    }

    return title;

  }

  render() {

    // TODO: Pick a safer way to set ID's
    let id = this.props.defaultTitle.toLowerCase().replace(/\s+/g, '-') + "-dropdown";

    let elements = this.props.elements.map((element, index) => {
      let elementKey = element.title + index;
      return <li key={elementKey}>
        <button onClick={(e) => {
          // eslint-disable-next-line
          this.props.onClick ? this.props.onClick(element) : this.dropdownClick(element)
        }}>{element.title}</button>
      </li>
    })

    let errorStyle = this.props.hasErrors ? {border: "1px solid #d0021b"} : {};

    return(
      <div className="dropdown">
        <button className="btn btn-default dropdown-toggle" type="button" id={id} data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style={errorStyle}>
          {/* Dropdown title */}
          {this.getTitle()}
          <span className="caret"></span>
        </button>
        <ul className="dropdown-menu" aria-labelledby={id}>
          {elements}
        </ul>
      </div>
    );
  }
}

export default Dropdown;
