import React, { Component } from 'react';
import Input from "./Input";
import appArrayStateHelpers from 'appArrayStateHelpers';
import checkImg from "controls/check-green.svg";
import xImg from "controls/x-red.svg";
import classnames from "classnames";
import alerts from "alerts";
import api from "api";

// TODO: Maybe refactor into something more generic like "InputWithSubtext"
class HiringManagerInput extends Component {

  componentWillMount = () => {
    this.setState({
      emailStatus: null,
      emailButtonText: "",
      statusImage: null,
      statusText: ""
    })
  }

  componentWillReceiveProps = (nextProps) => {
    // Undo internal state when props index changes
    if (nextProps.index !== this.props.index) {
      this.setState({
        emailStatus: null,
        emailButtonText: "",
        statusImage: null,
        statusText: ""
      })
    }
  }

  // Functions to check emails and display corresponding messages
  onBlur = () => {
    let email = appArrayStateHelpers.get(this, "hiringManagerEmail", "jobs");
    let fullName = appArrayStateHelpers.get(this, "hiringManagerFullName", "jobs") || "";

    // Assume first name is everything in front of the space
    let firstName = fullName.split(" ")[0];
    let lastName = fullName.split(" ");
    lastName.shift();
    lastName = lastName.join(" ");


    let title = appArrayStateHelpers.get(this, "hiringManagerTitle", "jobs");
    let linkedIn = appArrayStateHelpers.get(this, "hiringManagerLinkedIn", "jobs");

    // Convert to object API can recieve
    let hiringManagerObject = {
      first_name: firstName,
      last_name: lastName,
      title: title,
      linkedin: linkedIn,
      email: email
    }

    let modalIndex = parseInt(this.props.getActiveModal().split(":")[1], 10);
    let jobs = this.props.getAppState(this.props.modelId, "jobs");
    let job = jobs[modalIndex] || {};

    //
    api.checkHiringManagerExists(email)
    .done((response) => {
      // Hiring manager already exists, attach it to req
      console.log("hiring manager exists " , response);

      this.attachHiringManager(hiringManagerObject, job);
      this.setInputStatus("exists");

    })
    .fail((response) => {
      console.log("failed to check if hiring manager exists " , response);
      if (response.responseJSON.status.toLowerCase() === "hiring manager not found.") {
        api.createHiringManager(hiringManagerObject, job)
        .done((response) => {
          console.log("Successfully created hiring manager " , response);
          this.attachHiringManager(hiringManagerObject, job);
          this.setInputStatus("nonexistant");
        })
        .fail((response) => {
            console.log("Failed creating hiring manager " , response);
            this.showHiringManagerError();
        })
      } else {
        console.log("hiring manager check failed without good error " , response);
        this.showHiringManagerError();
      }
    })

  }

  showHiringManagerError = () => {
    alerts.show({
      status: "error",
      message: "Uh-oh, there was an error saving the hiring manager to this job. Please make sure all fields are filled in and try again."
    })
  }

  attachHiringManager = (hiringManager, job) => {
    api.attachHiringManagerToReq(hiringManager, job)
    .done((response) => {
      console.log("Successfully added hiring manager to req");
    })
    .fail((response) => {
      console.log("Error attaching hiring manager to req" , response);
    })
  }

  setInputStatus = (status) => {
    if (status === "exists") {
      this.setState({
        emailStatus: "exists",
        emailButtonText: "Notify Hiring Manager",
        statusImage: checkImg,
        statusText: "A Happie user account currently exists for this email address."
      })
    } else if (status === "nonexistant") {
      this.setState({
        emailStatus: "nonexistant",
        emailButtonText: "Invite & Notify Hiring Manager",
        statusImage: xImg,
        statusText: "A Happie user account doesn't exist yet for this email address."
      })
    }
  }

  notifyClick = () => {
    // TODO: Make API request, respond with correct message
    alerts.show({
      status: "success",
      message: "Success! A notification message to the hiring manager is on its way. Feel free to send another later on if you want to give an extra nudge."
    })
  }

  // Copied from normal Input component, added api requests



  render() {

    let subtextStyle = !this.state.emailStatus ? {display: "none"} : {};

    let icon = <img alt="email status" className="custom-input-icon" style={subtextStyle} src={this.state.statusImage}></img>;

    let subtextContainerClass = classnames({
      "custom-input-subtext-container": true,
      "success": this.state.emailStatus === "exists",
      "failure": this.state.emailStatus === "nonexistant"
    })

    return (
      <div>
        <Input onBlur={this.onBlur} style={{width: "140px"}} icon={icon} {...this.props} onFocus={this.onFocus}></Input>

        <div className={subtextContainerClass} style={subtextStyle}>
          <span>{this.state.statusText}</span>
          <div>
            <button className="btn" onClick={this.notifyClick}>{this.state.emailButtonText}</button>
          </div>
        </div>
      </div>
    );
  }
}

export default HiringManagerInput;
