import React, { Component } from 'react';
import appArrayStateHelpers from 'appArrayStateHelpers';
import Tooltip from "./Tooltip";
import addProtocol from "addProtocol";

class Input extends Component {

  componentWillMount = () => {
    this.setState({
      debounceTimeout: null,
      value: this.getTextValue()
    })
  }

  componentWillUnmount = () => {
    clearTimeout(this.state.debounceTimeout);
  }

  componentWillReceiveProps = (nextProps) => {
    let value = this.getTextValue();

    if (value !== this.state.value && !this.state.debounceTimeout) {
      this.setState({
        value: value
      })
    }
  }

  onTextChange = (e) => {

    let value = e.target.value;

    // If we're using a URL, make sure https is added
    if (this.props.type === "url" && value !== "") {
      value = addProtocol(value);
    }

    let key = this.props.modelKey.split(":");
    let baseKey = key[0];
    let arrayKey = key[1];

    // If there's a colon in the key, it means we're editing an array, so modify the object specified in the 2nd part

    clearTimeout(this.state.debounceTimeout);

    let timeout;
    if (arrayKey) {
      timeout = setTimeout(() => {
        appArrayStateHelpers.set(this, baseKey, arrayKey, value);

        this.setState({
          debounceTimeout: null
        })
      }, 500)
    } else {
      // Normal case, model key has no ":" in it
      timeout = setTimeout(() => {
        this.props.setAppState(this.props.modelId, baseKey, value)

        this.setState({
          debounceTimeout: null
        })
      }, 500)
    }

    this.setState({
      debounceTimeout: timeout,
      value: value
    })

  }

  getTextValue() {

    let key = this.props.modelKey.split(":");
    let baseKey = key[0];
    let arrayKey = key[1];

    // If there's a colon in the key, it means we're using an array, so get correct object value
    if (arrayKey) {
      return appArrayStateHelpers.get(this, baseKey, arrayKey) || "";
    } else {
      // Normal case, model key has no ":" in it
      return this.props.getAppState(this.props.modelId, this.props.modelKey) || "";
    }

  }

  render() {

    let tooltip = this.props.tooltip ? <Tooltip {...this.props} tooltip={this.props.tooltip} ref="tooltipComponent" id={this.props.modelId + "-custom-tooltip"} /> : null;

    return (
      <label>
        {this.props.title}
        {tooltip}
        <input
          value={this.state.value}
          className="custom-input"
          onChange={this.props.onChange || this.onTextChange}
          placeholder={this.props.placeholder}
          onBlur={this.props.onBlur}
          style={this.props.style}
          >
        </input>
        {this.props.icon}
      </label>
    );
  }
}

export default Input;
