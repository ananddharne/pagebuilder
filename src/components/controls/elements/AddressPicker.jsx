import React, { Component } from 'react';
import appArrayStateHelpers from 'appArrayStateHelpers';
import Geosuggest from 'react-geosuggest';
import mapsHelper from "mapsHelper";
import $ from "jquery";
import alerts from "alerts";
import deepEqual from "deep-equal";

class AddressPicker extends Component {

  componentWillMount() {

    this.setState({
      mapsLoaded: false,
      location: null,
      displayedLocation: null,
      fixtures: [
        {label: 'Cambridge, MA'},
        {label: 'New York, NY'},
        {label: 'San Francisco, CA'}
      ]
    })

    // We're asynchronously loading google maps, so set state to "loaded" once it's ready
    mapsHelper.getLoadPromise()
    .done(() => {
      this.setState({
        mapsLoaded: true,
        location: new window.google.maps.LatLng(0,0)
      })
    })
    .fail((e) => {
      console.error("Error loading google maps " , e)
    })
  }

  componentWillReceiveProps = (nextProps) => {

    let key = this.props.modelKey.split(":");
    let baseKey = key[0];
    let arrayKey = key[1];

    let location;
    if (arrayKey) {

      // At this point our props have not actually updated, so make a new object and copy over
      // props to pass into our array state helper function
      let component = {
        props: {...this.props}
      }
      component.props.index = nextProps.index;

      location = appArrayStateHelpers.get(component, baseKey, arrayKey) || "";
    } else {
      location = this.props.getAppState(this.props.modelId, baseKey) || "";
    }

    if (location && !deepEqual(location, this.state.displayedLocation)) {

      mapsHelper.getLoadPromise()
      .done(() => {

        // Call gmaps api to get "gmaps" object that contains info like city, zip code, etc.
        mapsHelper.geocoder.geocode({
          "placeId": location.place_id
        }, (results, status) => {

          let cityAndState = this.getCityAndState({
            gmaps: results[0]
          });

          this.refs.geoSuggestComponent.update(cityAndState);

          this.setState({
            displayedLocation: location
          })

        })
      })
      .fail((e) => {
        console.error("Error getting data for google maps " , e)
      })


    }
  }

  onSuggestSelect = (location) => {

    let key = this.props.modelKey.split(":");
    let baseKey = key[0];
    let arrayKey = key[1];

    // Convert to simpler object
    let simpleLoc = location.location;
    simpleLoc.place_id = location.placeId;

    // If we've got a default fixture, replace the data we get from clicking on it
    if (location.isFixture) {
      if (location.label === "Cambridge, MA") {
        simpleLoc = {place_id: "ChIJX8wwy6Vw44kRh2xoiWSOOsU", lat: 42.3647365, lng: -71.1035734}
      } else if (location.label === "San Francisco, CA") {
        simpleLoc = {place_id: "ChIJIQBpAG2ahYAR_6128GcTUEo", lat: 37.7749295, lng: -122.4194155}
      } else if (location.label === "New York, NY") {
        simpleLoc = {place_id: "ChIJOwg_06VPwokRYv534QaPC8g", lat: 40.7127837, lng: -74.0059413}
      }
    }

    // If there's a colon in the key, it means we're editing an array, so modify the object specified in the 2nd part
    if (arrayKey) {
      appArrayStateHelpers.set(this, baseKey, arrayKey, simpleLoc);

      // Also set has unsaved changes if we're changing a job
      if (arrayKey === "jobs") {
        appArrayStateHelpers.set(this, "hasChanges", arrayKey, true);
      }

    } else {
      // Normal case, model key has no ":" in it
      this.props.setAppState(this.props.modelId, baseKey, simpleLoc)
    }

    // After setting real location, update input value to just show city and state
    this.refs.geoSuggestComponent.update(this.getCityAndState(location));

    // Then set our internal location to the one we just picked, which includes all gmaps metadata
    this.setState({
      location: new window.google.maps.LatLng(simpleLoc.lat, simpleLoc.lng)
    })
  }

  getCityAndState = (location) => {

    if (!location || !location.gmaps) {
      return "";
    }

    let city = "";
    let state = "";

    // Address components that come back aren't the same for every address, so need to loop
    // through them to find the data types we're looking for
    $(location.gmaps.address_components).each((index, value) => {
      if (value.types[0] === "locality") {
        city = value.long_name;
      } else if (value.types[0] === "administrative_area_level_1") {
        state = value.short_name;
      }
    })

    return `${city}, ${state}`;
  }

  onActivateSuggest = (location) => {

    let simpleLoc = location;
    // If we've got a default fixture, replace the data we get from clicking on it
    if (location.isFixture) {
      if (location.label === "Cambridge, MA") {
        simpleLoc = {place_id: "ChIJX8wwy6Vw44kRh2xoiWSOOsU", lat: 42.3647365, lng: -71.1035734}
      } else if (location.label === "San Francisco, CA") {
        simpleLoc = {place_id: "ChIJIQBpAG2ahYAR_6128GcTUEo", lat: 37.7749295, lng: -122.4194155}
      } else if (location.label === "New York, NY") {
        simpleLoc = {place_id: "ChIJOwg_06VPwokRYv534QaPC8g", lat: 40.7127837, lng: -74.0059413}
      }
    }

    this.setState({
      location: new window.google.maps.LatLng(simpleLoc.lat, simpleLoc.lng)
    })
  }

  onSuggestBlur = (e) => {
    // Blur only called if an item isn't selected from the list
    // TODO: Select first item if you don't actually select it
    let key = this.props.modelKey.split(":");
    let baseKey = key[0];
    let arrayKey = key[1];

    let location;
    if (arrayKey) {
      location = appArrayStateHelpers.get(this, baseKey, arrayKey) || "";
    } else {
      location = this.props.getAppState(this.props.modelId, baseKey) || "";
    }

    this.refs.geoSuggestComponent.update(this.getCityAndState(location));

    if (!location || !e) {
      alerts.show({
        status: "warning",
        message: "Please select an option from the location dropdown list."
      })
    }
  }

  render() {

    // Location suggest
    let geoSuggest = <span></span>;

    let key = this.props.modelKey.split(":");
    let baseKey = key[0];
    let arrayKey = key[1];

    let location;
    if (arrayKey) {
      location = appArrayStateHelpers.get(this, baseKey, arrayKey) || "";
    } else {
      location = this.props.getAppState(this.props.modelId, baseKey) || "";
    }

    location = this.getCityAndState(location);

    // Only display if google maps API is loaded
    if (this.state.mapsLoaded) {

      // Pretty hacky way to dynamically size input. Calculate a size based on num of chars,
      // but have a max limit

      let geoSuggestStyle = {};

      if (this.props.autoResize) {
        let length = location.length * 9;
        if (length > 280) {
          length = 280;
        } else if (length < 140) {
          length = 140;
        }
        geoSuggestStyle = {
          width: length + "px"
        }
      }

      let additionalAttrs = {};
      if (this.props.getAppMode() !== "edit") {
        additionalAttrs = {disabled: true}
      }

      geoSuggest = <Geosuggest
        placeholder="City, State"
        initialValue={location}
        fixtures={this.state.fixtures}
        onSuggestSelect={this.onSuggestSelect}
        location={this.state.location}
        ref="geoSuggestComponent"
        radius="20"
        autoActivateFirstSuggest={true}
        onActivateSuggest={this.onActivateSuggest}
        onBlur={this.onSuggestBlur}
        style={{"input": geoSuggestStyle}}
        types={["geocode"]}
        {...additionalAttrs}
        />
    }

    return (
      <span>
        {geoSuggest}
      </span>
    );
  }
}

export default AddressPicker;
