import React, { Component } from 'react';
import editImg from 'jobs/edit-green.svg';
import arrowImg from 'sort-arrow.svg';
import removeImg from 'x-out.svg';

class JobListItem extends Component {

  editOnMouseDown = (e) => {
    // Prevent dragging on edit button
    e.stopPropagation();
  }

  render() {
    return (
      <li>
        <img role="button" alt="edit job" src={editImg} data-edit-button={true} onMouseDown={this.editOnMouseDown} onClick={this.props.sharedProps.editClick}></img>
        <img role="button" alt="sort team members" src={arrowImg}></img>
        <span role="button">{this.props.item.title || "Untitled " }</span>
        <img className="remove-job" role="button" alt="remove job" src={removeImg} data-edit-button={true} onMouseDown={this.editOnMouseDown} onClick={() => {this.props.sharedProps.removeClick(this.props.item.id)}}></img>
      </li>
    );
  }
}

export default JobListItem;
