import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import Slider from './Slider';
import ColorPicker from './ColorPicker';
import Switch from './Switch';
import Tooltip from './Tooltip';
import appArrayStateHelpers from 'appArrayStateHelpers';
import api from "api";
import alerts from "alerts";
import deleteImg from "delete.svg";

class ImageUpload extends Component {

  componentWillMount() {
  }

  buttonClick = (e) => {
    this.refs.dropzone.open();
  }

  onDrop = (files) => {

    // Get filename before we upload, since file comes back with obfuscated name
    let fileName = files[0].name;

    api.saveImage(files)
    .done((data) => {

      let url = data.url;

      let key = this.props.modelKey.split(":");
      let baseKey = key[0];
      let arrayKey = key[1];

      // If there's a colon in the key, it means we're editing an array, so modify the object specified in the 2nd part
      if (arrayKey) {
        appArrayStateHelpers.set(this, baseKey, arrayKey, url);
        if (this.props.modelKey.indexOf('teamMembers') > -1) {
          appArrayStateHelpers.set(this, "teamMembers:imageName", arrayKey, fileName);
        }
      } else {
        // Normal case, model key has no ":" in it
        this.props.setAppState(this.props.modelId, baseKey, url)
      }

      this.props.setAppState(this.props.modelId, this.props.modelKey, url);
      // Append "ImageName" to save actual file name
      this.props.setAppState(this.props.modelId, this.props.modelKey + "ImageName", fileName);

      // Also set app state to have default properties when image is uploaded
      this.setBackgroundAfterUpload();

      alerts.show({
        status: "success",
        message: "Successfully uploaded image!"
      })

    })
    .fail((e) => {
      console.error("Error uploading image:" , e);
      alerts.show({
        status: "error",
        message: "Uh oh! There was an error when you tried to upload your image. Please make sure the image file doesn't look funky, and try again!"
      })
    })

  }

  setBackgroundAfterUpload() {

    // Set default colors for text/opacity if they aren't already set
    if (!this.props.getAppState(this.props.modelId, "opacityColor") && !this.props.hideControls) {
      this.props.setAppState(this.props.modelId, "opacityColor", "#323233");
      this.props.setAppState(this.props.modelId, "opacity", 55);
    }

    if (!this.props.getAppState(this.props.modelId, "textColor") && !this.props.hideControls) {
      this.props.setAppState(this.props.modelId, "textColor", "#F5F5F5");
    }
  }

  // Show tooltip when dragging into image upload, and hide when dragging out
  imageDragEnter = () => {
    this.refs.tooltipComponent.setTooltipVisible(true)
  }
  imageDragLeave = () => {
    this.refs.tooltipComponent.setTooltipVisible(false)
  }

  deleteImageClick = () => {
    this.props.setActiveDialog({
      description: "Are you sure you want to delete your image?",
      confirmText: "Yes",
      cancelText: "No",
      confirm: this.deleteImage
    })
  }

  deleteImage = () => {
    // TODO: Implement actual delete or just keep it like this that will `un-save` the image, but it will
    // still take up storage
    this.props.setAppState(this.props.modelId, this.props.modelKey, "");
    this.props.setAppState(this.props.modelId, this.props.modelKey + "ImageName", "");

    alerts.show({
      status: "success",
      message: "Successfully deleted image"
    })
  }

  render() {

    let dropzoneId = this.props.modelId + "-" + this.props.modelKey + "-image-upload";

    let elementProps = {modelId: this.modelId, ...this.props};

    let background = this.props.getAppState(this.props.modelId, this.props.modelKey)
    let imageControls = null;

    let key = this.props.modelKey.split(":");
    let arrayKey = key[1];

    let imageName = this.props.getAppState(this.props.modelId, this.props.modelKey + "ImageName") || null;

    if (arrayKey && this.props.modelKey.indexOf('teamMembers') > -1) {
      imageName = appArrayStateHelpers.get(this, "teamMembers:imageName", arrayKey) || null;
    }

    let parallaxOn = this.props.getAppState(this.props.modelId, "parallax") || false;

    if (background && !this.props.hideControls) {
      // Note that modelKey is defined after other props, and actually overwrites a modelKey that
      // is defined in elementProps.  Not sure if this is a valid way of overriding, or if each one should
      // individually change the `elementProps` object
      imageControls = [
        <Slider key="zoom" title="Zoom In / Zoom Out" {...elementProps} modelKey="zoom" />,
        <Switch key="parallax-switch"
          onClick={this.switchClick}
          on={parallaxOn} {...elementProps}
          modelKey="parallax"
          title="Parallax Effect" />,
        <Slider key="opacity" title="Opacity" {...elementProps} modelKey="opacity" />,
        <ColorPicker key="opacity-color-solid" {...elementProps} modelKey="opacityColor" />
      ]
    }

    let tooltipId = this.props.modelId + "-custom-tooltip";
    let tooltipMessage = this.props.tooltip || "Use a JPG, PNG, or GIF under 5MB in size. Resolution of 1400 by 1200 pixels looks best.";

    return (
      <div className="image-upload-container">
        <label htmlFor={dropzoneId}>
          Upload An Image
          <Tooltip {...this.props} tooltip={tooltipMessage} ref="tooltipComponent" id={tooltipId} />
        </label>

        <Dropzone
          id={dropzoneId}
          className="image-dropzone dotted-border"
          disableClick={true}
          multiple={false}
          ref="dropzone"
          onDrop={this.onDrop}
          aria-describedby={tooltipId}
          onDragEnter={this.imageDragEnter}
          onDragLeave={this.imageDragLeave}>
          <div>Drag a photo here</div>
          <div>or</div>
          <button onClick={this.buttonClick}>Select Image</button>
        </Dropzone>
        <div className="image-title">{imageName}
          <img src={deleteImg}
            style={!imageName ? {display: "none"} : {float: "right"}}
            role="button"
            alt="delete"
            onClick={this.deleteImageClick}></img>
        </div>
        {imageControls}
      </div>

    );
  }
}

export default ImageUpload;
