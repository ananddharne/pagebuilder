import React, { Component } from 'react';
import CP from 'react-color-picker'
import eyedropperImg from 'controls/eyedropper.svg'
import colorPickerHandle from 'controls/colorPickerHandle.svg'
import $ from 'jquery'

class ColorPicker extends Component {

  constructor(props) {
    super(props)

    this.state = {
      swatches: [
        "#70A7E6",
        "#77C185",
        "#F8D856",
        "#F3954A",
        "#F46D6F",
        "#EE8DB9",
        "#6A6FD1",
        "#6D6D6D",
        "#F5F5F5"
      ],
      "collapsed": true
    }

  }

  componentDidMount() {
    // TODO: Webpack makes it difficult to reference an image url() from the CSS, but might need
    // to take the time to figure it out if this proves buggy
    let imageUrl = "url('" + colorPickerHandle + "')"
    $(".react-color-picker__hue-drag").css("background", imageUrl);

  }

  onDrag = (color, c) => {
    this.props.setAppState(this.props.modelId, this.props.modelKey, color)
  }

  colorChange = (color) => {
    this.props.setAppState(this.props.modelId, this.props.modelKey, color);
    // Set state just to force re-render. To actually get state, use getAppState(), NOT this.state.
    this.setState({
      color: color
    })

  }

  inputChange = (e) => {
    let value = e.target.value;

    // Add # in if it's not there
    if (value.charAt(0) !== "#") {
      value = "#" + value;
    }

    // Limit to 6 codes (7 with #)
    if (value.length >= 7) {
      value = value.substring(0,7)
    }

    this.setState({
      color: value
    })
    this.props.setAppState(this.props.modelId, this.props.modelKey, value, true);
  }

  inputBlur = (e) => {
    let value = e.target.value;

    // If we leave input before putting in 6 digits (+1 for '#'), pad with 0's
    let lessDigits = 7 - value.length;

    if (lessDigits > 0) {
      for (let i = 0; i < lessDigits; i++) {
        value += "0";
      }
      this.props.setAppState(this.props.modelId, this.props.modelKey, value)
    }
  }

  inputFocus = () => {
    this.setState({
      collapsed: false
    })
  }

  inputKeyDown = (e) => {
    // Close color picker on enter key when typing in hex
    if (e.keyCode === 13) {
      this.setState({
        collapsed: true
      })
    }
  }

  render() {

    // Use #FFFFFF as a default color
    let currentColor = this.props.getAppState(this.props.modelId, this.props.modelKey);

    // Build color swatches and attach data attribute so we can reference when clicking
    let swatches = this.state.swatches.map((value) => {
      return <span key={value} className="color-swatch" style={{"backgroundColor": value}} onClick={() => this.colorChange(value)} role="button"></span>
    })

    let colorPickerStyle = {
      display: this.state.collapsed ? "none" : "block"
    }

    return (
      <div>
        <div>
          <label>Pick a Swatch
            <div className="swatch-container">{swatches}</div>
          </label>

        </div>
        <CP saturationWidth={148} saturationHeight={148} hueWidth={20} hueHeight={148} onDrag={this.onDrag} onChange={this.colorChange} style={colorPickerStyle} />
        <div className="color-hex-container">
          {/* TODO: Be able to inline edit this */}
          <img className="eyedropper" src={eyedropperImg} alt="eyedropper"></img>
          <input
            value={currentColor}
            onChange={this.inputChange}
            onBlur={this.inputBlur}
            onFocus={this.inputFocus}
            onKeyDown={this.inputKeyDown}
            placeholder="Or select a custom color"></input>
          <span className="color-swatch" style={{"backgroundColor": currentColor}}></span>
        </div>
      </div>
    );
  }
}

export default ColorPicker;
