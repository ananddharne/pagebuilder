import React, { Component } from 'react';
import RCSlider from "rc-slider";

class Slider extends Component {

  sliderChange = (value) => {
    this.props.setAppState(this.props.modelId, this.props.modelKey, value, true)

    // Set state just to force re-render. To actually get state, use getAppState(), NOT this.state.
    this.setState({
      color: value
    })
  }

  render() {

    let value = this.props.getAppState(this.props.modelId, this.props.modelKey) || 0;
    return (
      <label>
        {this.props.title}
      <RCSlider value={value} onChange={this.sliderChange}></RCSlider>
      </label>
    );
  }
}

export default Slider;
