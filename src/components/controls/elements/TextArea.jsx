import React, { Component } from 'react';
import Tooltip from "./Tooltip";
import classnames from "classnames";

class TextArea extends Component {

  componentWillMount = () => {

    // Our internal state value is not exactly the same as the overall app state, since we
    // were getting some back performance issues, so we added debouncing to input and actually saving.
    this.setState({
      debounceTimeout: null,
      value: this.props.getAppState(this.props.modelId, this.props.modelKey)
    })
  }

  componentWillUnmount = () => {
    clearTimeout(this.state.debounceTimeout);
  }

  componentWillReceiveProps = (nextProps) => {
    let value = this.props.getAppState(this.props.modelId, this.props.modelKey);

    if (value !== this.state.value && !this.state.debounceTimeout) {
      this.setState({
        value: value
      })
    }
  }

  onTextChange = (e) => {

    let value = e.target.value;

    clearTimeout(this.state.debounceTimeout);

    let timeout = setTimeout(() => {
      this.props.setAppState(this.props.modelId, this.props.modelKey, value)
      this.setState({
        debounceTimeout: null
      })
    }, 500)

    this.setState({
      debounceTimeout: timeout,
      value: value
    })
  }

  render() {

    let tooltip = this.props.tooltip ? <Tooltip {...this.props} tooltip={this.props.tooltip} ref="tooltipComponent" id={this.props.modelId + "-custom-tooltip"} /> : null;

    let characterCount;
    let value = this.props.getAppState(this.props.modelId, this.props.modelKey);

    if (this.props.characterCount) {
      let countClass = classnames({
        "character-count": true,
        "warning": value.length > this.props.characterCount
      })
      characterCount = <div className="character-count-container">
        <span className="character-count-label">Characters</span>
        <span className={countClass}>{this.props.characterCount - value.length}</span>
      </div>
    }

    return (
      <label>
        {this.props.title}
        {tooltip}
        <textarea
          value={this.state.value}
          onChange={this.onTextChange}
          placeholder={this.props.placeholder}
          >
        </textarea>
        {characterCount}
      </label>
    );
  }
}

export default TextArea;
