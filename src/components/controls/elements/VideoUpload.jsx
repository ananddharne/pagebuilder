import React, { Component } from 'react';
import ColorPicker from './ColorPicker';
import Slider from "./Slider"

class VideoUpload extends Component {

  inputChange = (e) => {
    // TODO: Maybe set up debouncing if we think people will be typing in URLs

    this.props.setAppState(this.props.modelId, "background", e.target.value);

    this.setBackgroundAfterUpload();
  }

  setBackgroundAfterUpload() {

    // Set default colors for text/opacity if they aren't already set
    if (!this.props.getAppState(this.props.modelId, "opacityColor")) {
      this.props.setAppState(this.props.modelId, "opacityColor", "#323233");
      this.props.setAppState(this.props.modelId, "opacity", 55);
    }

    if (!this.props.getAppState(this.props.modelId, "textColor")) {
      this.props.setAppState(this.props.modelId, "textColor", "#F5F5F5");
    }
  }

  helpClick = (e) => {
    e.preventDefault();
    this.props.setActiveModal("video")
  }

  render() {
    let elementProps = {modelId: this.modelId, ...this.props};

    let videoUrl = this.props.getAppState(this.props.modelId, "background") || "";

    return(
      <label>
        <div className="label-separator">Add a Video (<a onClick={this.helpClick}>Show me how!</a>)</div>
        <input className="custom-input" placeholder="Paste a YouTube or Vimeo URL" onChange={this.inputChange} value={videoUrl}></input>
        <Slider modelKey="opacity" title="Opacity" {...elementProps} />
        <ColorPicker key="opacityColor" modelKey="opacityColor" {...elementProps} />
      </label>
    );
  }
}

export default VideoUpload;
