import React, { Component } from 'react';
import ReactSwitch from 'react-toggle-switch';

class Switch extends Component {

  switchClick = (e) => {

    // If switchClick passed in as prop, call that function INSTEAD OF this one
    if (this.props.switchClick) {

      if (Number.isInteger(this.props.pageKey)) {
        this.props.switchClick(this.props.pageKey);
        return;
      }

      this.props.switchClick(e);
      return;
    }

    // Otherwise, default way to save props
    if (this.props.getAppState(this.props.modelId, this.props.modelKey)) {
      this.props.setAppState(this.props.modelId, this.props.modelKey, false)
    } else {
      this.props.setAppState(this.props.modelId, this.props.modelKey, true)
    }

  }

  render() {

    let switchLabel = null;
    if (this.props.on) {
      switchLabel = <span onClick={this.switchClick} className="switch-label on">On</span>
    } else {
      switchLabel = <span onClick={this.switchClick} className="switch-label">Off</span>
    }

    let element = {};

    // Only return wrapped in label if title is defined
    if (this.props.title) {
      element = <label>
        {this.props.title}
        <div className="switch-container">
          <ReactSwitch key="parallax-switch" onClick={this.switchClick} on={this.props.on} />
          {switchLabel}
        </div>
      </label>
    } else {
      element = <div className="switch-container">
        <ReactSwitch key="parallax-switch" onClick={this.switchClick} on={this.props.on} />
        {switchLabel}
      </div>
    }

    return (
      <span style={this.props.style}>{element}</span>

    );
  }
}

export default Switch;
