import React, { Component } from 'react';
import UpArrow from 'up-arrow.svg';
import DownArrow from 'down-arrow.svg';

class FieldSet extends Component {

  componentWillMount() {
    this.setState({
      collapsed: false
    })
  }

  toggleCollapse = () => {
    this.setState({collapsed: !this.state.collapsed})
  }

  render() {

    // Set elements to hidden instead of removing them to preserve state
    // Also change direction of arrow
    let elementsContainerStyle;
    let collapseArrow;

    if (this.state.collapsed) {
      elementsContainerStyle = {"display": "none"}
      collapseArrow = <img src={DownArrow} tabIndex="0" alt="down arrow"></img>
    } else {
      elementsContainerStyle = {"display": "block"}
      collapseArrow = <img src={UpArrow} tabIndex="0" alt="up arrow"></img>
    }

    return (
      <fieldset>
        <legend role="button" onClick={this.toggleCollapse}>{this.props.legend} {collapseArrow}</legend>
        <hr></hr>
        <div style={elementsContainerStyle}>
          {this.props.elements}
        </div>
      </fieldset>
    );
  }
}

export default FieldSet;
