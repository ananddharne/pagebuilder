import React, { Component } from 'react';
import CheckBox from "./CheckBox";

class SignUpListControl extends Component {

  componentWillMount() {

  }

  checkBoxChange(e, type, index) {
    let checked = e.target.checked;

    let fields = this.props.getAppState(this.props.modelId, "signUpFields").slice() || [];
    fields[index][type] = checked;

    // If we're unchecking the "active" field, uncheck the required field as well
    if (type === "enabled" && checked === false) {
      fields[index]["required"] = checked;
    }

    // Likewise, if we check "required", make sure "active" is also checked
    if (type === "required" && checked === true) {
      fields[index]["enabled"] = checked;
    }

    // Save changes
    this.props.setAppState(this.props.modelId, "signUpFields", fields);

  }

  previewClick = () => {
    this.props.setActiveModal("signUp");
  }

  viewSignupsClick = () => {
    this.props.setActiveModal("signUpList");
  }


  render() {

    let fields = this.props.getAppState(this.props.modelId, "signUpFields");

    fields = fields.map((value, index) => {
      return <li key={value.id}>{value.title}
        <span className="list-input-container">
        <CheckBox checked={value.enabled}
          onChange={(e) => {this.checkBoxChange(e, "enabled", index)}}
          disabled={value.readOnly} />
        <CheckBox checked={value.required}
          onChange={(e) => {this.checkBoxChange(e, "required", index)}}
          disabled={value.readOnly }/>
        </span>
      </li>
    })

    let signups = this.props.getAppState("_metadata", "signups") || [];
    let signupsCountMessage = signups.length === 1 ? `${signups.length} candidate has signed up for updates` : `${signups.length} candidates have signed up for updates`;

    return (
      <div className="sign-up-control">
        <div className="sign-up-label">
          Signup Form
        </div>
        <button className="btn" onClick={this.previewClick}>Preview Form</button>
        <div className="checkbox-label-container">
          <span>Active</span>
          <span>Require</span>
        </div>
        <ul>
          {fields}
        </ul>
        <div className="sign-up-count-container">
          <span>{signupsCountMessage}</span>
          <button className="btn" onClick={this.viewSignupsClick}>View Signups</button>
        </div>
      </div>
    );
  }
}

export default SignUpListControl;
