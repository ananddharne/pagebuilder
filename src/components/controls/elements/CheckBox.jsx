import React, { Component } from 'react';

class CheckBox extends Component {

  render() {
    return (
      <span className="control-group">
        <label className="control control--checkbox">
          <input type="checkbox"
            checked={this.props.checked}
            onChange={this.props.onChange}
            disabled={this.props.disabled} />
          <div className="control__indicator"></div>
        </label>
      </span>
    );
  }
}

export default CheckBox;
