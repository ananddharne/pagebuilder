import React, { Component } from 'react';
import Models from 'Models';
import FieldSet from './elements/FieldSet';
import ImageUpload from './elements/ImageUpload';
import VideoUpload from './elements/VideoUpload';
import ColorPicker from './elements/ColorPicker';
import Slider from './elements/Slider';
import Dropdown from './elements/Dropdown';

class BottomImageControl extends Component {

  componentWillMount() {
    this.modelId = Models.bottomImage.id;

    this.setState({
      activeDropdownElement: null
    })
  }

  render() {

    let defaultProps = {
      modelId: this.modelId
    }
    let elementProps = {...defaultProps, ...this.props};

    let dropdownElements = [
      {
        title: "Image",
        type: "image"
      },
      {
        title: "Video",
        type: "video"
      },
      {
        title: "Solid Color",
        type: "solid"
      }
    ]

    let backgroundElements = {
      "image": <ImageUpload key="image-upload" modelKey="background" tooltip="Use a JPG, PNG, or GIF under 5MB in size. Resolution of 1400 by 1200 pixels looks best." {...elementProps} />,
      "video": <VideoUpload key="video-upload" {...elementProps} />,
      "solid": <div key="solidColorDropdown">
        <ColorPicker key="background-solid" modelKey="background" {...elementProps} />
        <Slider key="opacity" modelKey="opacity" title="Opacity" {...elementProps} />
        <ColorPicker key="opacity-color-solid" modelKey="opacityColor" {...elementProps} />
      </div>
    }

    let backgroundKey = this.props.getAppState(this.modelId, "backgroundType");
    let backgroundElement = backgroundElements[backgroundKey];

    let imageElements = [
      <Dropdown
        key="image"
        modelKey="backgroundType"
        resetKey="background"
        defaultTitle="Select a background type"
        elements={dropdownElements}
        {...elementProps}
        />
    ]

    if (backgroundElement) {
      imageElements.push(backgroundElement);
    }

    return (
      <div>
        <FieldSet legend="Background" elements={imageElements}></FieldSet>
      </div>
    );
  }
}

export default BottomImageControl;
