import React, { Component } from 'react';

class Loader extends Component {

  render() {

    let ariaAttr = {};
    let style = {
      display: "table",
      visiblity: "visible",
      opacity: 0.9
    }
    if (this.props.hidden) {
      ariaAttr = {
        "aria-hidden": true
      }
      style = {
        visibility: "hidden",
        opacity: 0
      }
    }

    return (
      <div className="loader-container vertical-align-container" {...ariaAttr} style={style}>
        <div className="vertical-aligner">
          <div className="sk-folding-cube">
            <div className="sk-cube1 sk-cube"></div>
            <div className="sk-cube2 sk-cube"></div>
            <div className="sk-cube4 sk-cube"></div>
            <div className="sk-cube3 sk-cube"></div>
          </div>
        </div>
      </div>
    );
  }
}

export default Loader;
