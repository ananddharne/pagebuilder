import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory, hashHistory } from 'react-router'
import App from './App';
import PageSelection from './PageSelection';
import './styles/main.scss';
import Login from "./Login";

const requireAuth = (nextState, replace) => {
  if (!localStorage.getItem('hap_token') && !(window.happie_page_data && window.happie_page_data.page_blocks)) {
    replace({ pathname: '/login' })
  }
}

const logoutAuth = (nextState, replace) => {
  localStorage.removeItem('id_token');
  localStorage.removeItem('hap_token');
  replace({ pathname: '/login' })
}

let indexComponent;
// TODO: Switch all to browser history if that doesn't mess with django
let historyType;
if (window.happie_page_data && window.happie_page_data.page_blocks) {
  historyType = browserHistory
  indexComponent = App;
} else {
  historyType = hashHistory;
  indexComponent = PageSelection;
}

ReactDOM.render(
  <Router history={historyType}>
    <Route path="/" component={indexComponent} onEnter={requireAuth}>
    </Route>
    <Route path="/login" component={Login}>
    </Route>
    <Route path="/pages/:publishedRoute" component={indexComponent}>
    </Route>
    <Route path="/jobs/:jobId" component={indexComponent}>
    </Route>
    <Route path="/builder/:pageId" component={App} onEnter={requireAuth}>
    </Route>
    <Route path="/logout" onEnter={logoutAuth}>
    </Route>
    <Route path="*" component={Login}>
    </Route>
  </Router>,
  document.getElementById('root')
);