import Auth0Lock from 'auth0-lock'
import { isTokenExpired } from './jwtHelper'
import $ from 'jquery';

export default class AuthService {
  constructor(clientId, domain, router) {
    // Configure Auth0
    this.lock = new Auth0Lock(clientId, domain, {
      auth: {
        responseType: 'token'
      },
      closable: false,
      languageDictionary: {
        title: "Log into Pagebuilder"
      },
    })

    this.router = router;
    // Add callback for lock `authenticated` event
    this.lock.on('authenticated', this._doAuthentication.bind(this))
    // binds login functions to keep this context
    this.login = this.login.bind(this)
  }

  _doAuthentication(authResult) {
    $("#app-pre-loader").show();
    this.lock.hide();
    // Saves the user token
    this.setToken(authResult.idToken);
    // Get user_id and log user into happie servers
    this.lock.getUserInfo(authResult.accessToken, (error, profile) => {
      if (error) {
        console.log("error getting profile " , error);
      } else {
        // TODO: put api base path into env.js file to use as a global variable
        $.ajax({
          method: "POST",
          contentType: "application/json",
          url: `/api/v1/users/login/`,
          data: JSON.stringify({auth0_id: profile.user_id})
        })
        .done((response) => {
          this.setHappie(response.token);
          this.router.push('/');
        })
        .fail((error) => {
          console.log("error logging into happie: " , error);
        })
      }
    })
  }

  login() {
    // Call the show method to display the widget.
    this.lock.show();
  }

  loggedIn() {
    // Checks if there is a saved token and it's still valid
    const token = this.getToken()
    return !!token && !isTokenExpired(token)
  }

  setToken(token) {
    // Saves user token to local storage
    localStorage.setItem('id_token', token)
  }

  setHappie(token) {
    // Saves user token to local storage
    localStorage.setItem('hap_token', token)
  }

  getToken() {
    // Retrieves the user token from local storage
    return localStorage.getItem('id_token')
  }

  getHappie() {
    // Retrieves the user token from local storage
    return localStorage.getItem('hap_token')
  }

  logout() {
    // Clear user token and profile data from local storage
    localStorage.removeItem('id_token');
    localStorage.removeItem('hap_token');
  }
}
