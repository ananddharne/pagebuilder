# See ../README.md for instructions on using this Makefile.

APP_STATIC_BASE=python-package/pagebuilder_static/static
APP_STATIC_FULL=$(APP_STATIC_BASE)/pagebuilder_static
S3_BUCKET=happie-dev-shared
S3_BUCKET_WEBSITE_HOSTNAME=happie-dev-shared.s3-website-us-east-1.amazonaws.com


python-build: build
	# Verify that python is version 3.4 or higher
	# If build fails here make sure you're in the right virtualenv.
	@echo 'import sys;sys.exit(0 if sys.version_info >= (3,4) else 1)' | python -

	# strip leading zeros because python does that later
	@sh -c "date +'%Y.%m.%d.%H.%M' | sed s/\\\\.0/./g > python-package/VERSION"

	@echo Updated version number to: `cat python-package/VERSION`
	@echo Preparing python package...

	rm -rf $(APP_STATIC_BASE)
	mkdir -p $(APP_STATIC_BASE)
	cp -a build $(APP_STATIC_FULL)
	$(MAKE) python-build-sdist


python-build-sdist:
	cd python-package && python setup.py sdist
	@echo
	@echo
	@echo Build completed. Created source distribution:
	@echo python-package/dist/pagebuilder-static-`cat python-package/VERSION`.tar.gz
	@echo
	@echo To upload, run: $(MAKE) upload
	@echo To install locally, run: $(MAKE) install
	@echo


upload: python-package/dist
	cd python-package/dist && dir2pi .
	aws s3 sync --acl public-read python-package/dist s3://$(S3_BUCKET)/packages/
	@echo Python package index at: http://$(S3_BUCKET_WEBSITE_HOSTNAME)/packages/simple/
	@echo To install the latest pagebuilder-static from this repo, run:
	@echo pip install --upgrade --trusted-host $(S3_BUCKET_WEBSITE_HOSTNAME) --index http://$(S3_BUCKET_WEBSITE_HOSTNAME)/packages/simple/ pagebuilder-static


install: python-package/VERSION
	pip install --upgrade --force-reinstall python-package/dist/pagebuilder-static-`cat python-package/VERSION`.tar.gz


whatversion:
	@echo Your currently installed version of pagebuilder-static is:
	@pip freeze|grep -E '^pagebuilder-static='
	@echo
	@echo \(If no version was reported above, then \'pip freeze\' isn\'t finding one.\)


build:
	npm run build

clean:
	rm -rf $(APP_STATIC_BASE)
	cd python-package && rm -rf pagebuilder_static.egg-info build dist
	rm -rf build
