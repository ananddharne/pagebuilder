## Happie Page Builder 😄

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
 
You can find the most recent version of the Create React App guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).  

## Installing
Assuming [node](https://nodejs.org) is installed on your machine, from the root directory run `npm install` to install dependencies.  Then use one of the following scripts to run the application.


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run api`

Runs the JSON api server for local development.  Edit the [Test.json](src/api/test.json) file to add API responses to the initial call that populates the App page state.

### `npm test`

Launches the test runner in the interactive watch mode.  
See the section about [running tests](#running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!


## Python Packaging

Because pagebuilder is distributed inside of the `happieweb` Django/Python application, this repo can be compiled into a python package distribution, which is then included in `custom-requirements.txt` in that repo. The `Makefile` facilitates generating new distributions, uploading them to Happie's package index (for use during Heroku deployments), and installing them locally (for dev/testing).

You should active the virtualenv you use for `happieweb` development before running these `make` commands.


### `make` (or `make python-build`)

Creates a new source distribution, assigning it a new version string based on the current date and time (e.g. 2016.12.12.11.48). The distribution is saved in a .tar.gz file in `python-package/dist`.


### `make install`

Installs the most recently built distribution into the current local environment. Use this when you're making changes in pagebuilder and want to test them with a happieweb instance you're running on your own machine, and do not necessarily want to release that version to staging/production.


### `make upload`

Generates package index files for all packages within `python-package/dist`, and then uploads these files to the S3 bucket hosting our custom package index. That package index is used for staging and production deploys, as well as from the requirements files in happieweb.


### `make whatversion`

Shows you what version of pagebuilder-static you currently have installed in your python environment.


### `make clean`

Clears out the NPM build output as well as previous python package builds.

## Installing Python Package to local Happie instance

From the `happieweb` package (with appropriate `virtualenv` running), you can run the following command to update to the latest front-end version that was packaged in the previous steps:

#### `rm custom-requirements.txt; pip-compile --trusted-host happie-dev-shared.s3-website-us-east-1.amazonaws.com custom-requirements.in; pip install -r requirements.txt --upgrade`
